#include "benchmark/YaepParser.hpp"

#include <absl/strings/str_join.h>
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <ranges>

#include "Input.hpp"
#include "Statistics.hpp"
#include "util/Finally.hpp"

namespace benchmark {
namespace {

std::string buildDescription(Grammar const& grammar, bool buildParseTree) {
  std::string description;
  absl::flat_hash_map<Symbol, std::vector<Production const*>> productionsByLhs;
  absl::flat_hash_set<Symbol> terminals;
  for (auto const& production : grammar.productions()) {
    productionsByLhs[production.lhs()].emplace_back(&production);
    terminals.insert(production.rhs().begin(), production.rhs().end());
  }
  for (auto const& production : grammar.productions()) {
    terminals.erase(production.lhs());
  }
  description += "TERM";
  for (Symbol t : terminals) {
    if (t > 255 || t < -1) {
      throw std::logic_error(
          fmt::format("YAEP only supports terminals in range (-1) 0-255, got {}", t));
    }
    // ANTLR EOF marker.
    if (t == -1) {
      t = 0;
    }
    description += fmt::format("\n  P{} = {}", t, t);
  }
  description += ";\n";
  auto addRule = [&](Symbol lhs, std::vector<Production const*> const& productions) {
    description += fmt::format("P{}", lhs);
    for (int i = 0; i < productions.size(); ++i) {
      Production const* p = productions[i];
      description += fmt::format("\n  {} ", i == 0 ? ":" : "|");
      if (p->rhs().empty()) {
        continue;
      }
      auto rhs = std::ranges::transform_view(p->rhs(), [](Symbol t) { return t == -1 ? 0 : t; });
      description += fmt::format("P{}", absl::StrJoin(rhs, " P"));
      // Parse tree translation specified in "(...)" - simply include all rhs symbols.
      if (buildParseTree) {
        description += fmt::format(" # {} ({})", p->label().empty() ? "_" : p->label(),
                                   absl::StrJoin(std::views::iota(0ul, p->rhs().size()), " "));
      }
    }
    description += ";\n";
  };
  addRule(grammar.start(), productionsByLhs[grammar.start()]);
  productionsByLhs.erase(grammar.start());
  for (auto const& [lhs, productions] : productionsByLhs) {
    addRule(lhs, productions);
  }
  return description;
}

Count countParseTrees(yaep_tree_node* node, absl::flat_hash_map<yaep_tree_node*, Count>& visited) {
  if (auto it = visited.find(node); it != visited.end()) {
    return it->second;
  }
  auto count = [&]() -> Count {
    switch (node->type) {
      case YAEP_ERROR:
        // Should not happen.
        return 0;
      case YAEP_ALT:
        // Alternative of the translation - ambiguous grammar.
        return countParseTrees(node->val.alt.node, visited)
               + (node->val.alt.next ? countParseTrees(node->val.alt.next, visited) : 0);
      case YAEP_ANODE: {
        // Abstract node.
        Count result = 1;
        for (auto child = node->val.anode.children; *child != nullptr; ++child) {
          result *= countParseTrees(*child, visited);
        }
        return result;
      }
      case YAEP_NIL:
        // Empty node.
        return 1;
      case YAEP_TERM:
        // Translation of terminals.
        return 1;
    }
    throw std::logic_error(fmt::format("YAEP unexpected node type: {}", node->type));
  };
  return visited.emplace(node, count()).first->second;
}

Count countParseTrees(yaep_tree_node* node) {
  absl::flat_hash_map<yaep_tree_node*, Count> visited;
  return countParseTrees(node, visited);
}

}  // namespace

using relparser::Statistics;

int YaepParser::tokenIndex_{0};
std::vector<Symbol> const* YaepParser::tokens_{nullptr};

YaepParser::YaepParser(std::string const& rpGrammarFile, ParseMode parseMode)
    : parseMode_{parseMode},
      description_{buildDescription(relparser::loadRpGrammar(rpGrammarFile),
                                    parseMode != ParseMode::Recognition)} {
  if (parseMode == ParseMode::Count) {
    throw std::invalid_argument(fmt::format("Unsuported parse mode: {}", parseMode));
  }
  TIMER("YaepParser::parseGrammar");
  if (parser_.parse_grammar(0, description_.c_str())) {
    SPDLOG_ERROR("Invalid Yaep grammar: \n{}", description_);
    throw std::logic_error(fmt::format("Error ({}) creating YAEP parser: {}", parser_.error_code(),
                                       parser_.error_message()));
  }
  SPDLOG_DEBUG("Yaep grammar: \n{}", description_);
  parser_.set_lookahead_level(1);
  parser_.set_error_recovery_flag(0);
  if (parseMode_ == ParseMode::ParseForest || parseMode_ == ParseMode::ParseForestCount) {
    parser_.set_one_parse_flag(0);
  }
}

std::optional<Count> YaepParser::parse(std::vector<Symbol> const& tokens) {
  tokenIndex_ = 0;
  tokens_ = &tokens;
  yaep_tree_node* root{nullptr};
  int is_ambiguous{0};
  {
    TIMER("YaepParser::parse");
    if (parser_.parse(readToken, syntaxError, /*parse_alloc=*/nullptr,
                      /*parse_free=*/nullptr, &root, &is_ambiguous)
        || root == nullptr) {
      return std::nullopt;
    }
  }
  auto onExit = finally([root]() {
    TIMER("YaepParser::freeMemory");
    yaep::free_tree(root, nullptr, nullptr);
  });
  Count result{1};
  if (parseMode_ == ParseMode::ParseForestCount) {
    TIMER("YaepParser::countParseTrees");
    result = countParseTrees(root);
  }
  return result;
}

int YaepParser::readToken(void** attr) {
  *attr = nullptr;
  if (tokenIndex_ < tokens_->size()) {
    int t = (*tokens_)[tokenIndex_++];
    // ANTLR EOF marker.
    return t == -1 ? 0 : t;
  }
  return -1;
}

void YaepParser::syntaxError(int err_tok_num, void* err_tok_attr, int start_ignored_tok_num,
                             void* start_ignored_tok_attr, int start_recovered_tok_num,
                             void* start_recovered_tok_attr) {
  // Don't do anything - because set_error_recovery_flag is set to "0" the parsing will be
  // interrupted and error reported there.
}

}  // namespace benchmark