#ifndef RELATIONAL_PARSER_PARSER_PARSING_HPP
#define RELATIONAL_PARSER_PARSER_PARSING_HPP

#include <functional>
#include <map>
#include <optional>
#include <string>
#include <vector>

#include "benchmark/Parser.hpp"
#include "benchmark/Settings.hpp"

namespace benchmark {

struct CommandlineFlags {
  std::string parsingMode;
  std::string parser;
  std::string grammar;
  std::vector<std::string> inputFiles;
  std::optional<std::string> inputDirectory;
  std::optional<std::string> inputFilesRegex;
  bool recursive;
  std::optional<std::string> resultsFile;
  std::size_t statusUpdateEveryTokens;
};

void runParsing(CommandlineFlags const& flags);

extern std::map<std::string, ParseMode> const parsingModes;

extern std::map<std::string,
                std::function<std::unique_ptr<Parser>(std::string const&, ParseMode)>> const
    parsers;

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_PARSER_PARSING_HPP
