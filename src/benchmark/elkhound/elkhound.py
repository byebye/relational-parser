import sys
from collections import defaultdict

productions = defaultdict(lambda: [])
symbols = set()
nonterminals = set()
terminals = set()
start = None


def get_symbols(rp_grammar):
    with open(rp_grammar, 'r') as reader:
        global start
        start = reader.readline().strip()

        for line in [line.rstrip() for line in reader.readlines()]:
            if line == '[terminals]':
                break
            sp = line.split(': ')[1].split(' ->')
            if len(sp) > 1 and sp[1]:
                lhs, rhs = sp
                rhs = rhs.strip().split(' ')
            else:
                lhs = sp[0]
                rhs = []
            productions[lhs.strip()].append(rhs)

        for lhs, rhss in productions.items():
            symbols.add(lhs)
            nonterminals.add(lhs)
            for rhs in rhss:
                symbols.update(rhs)

        global terminals
        terminals = symbols - nonterminals


def map_term(t):
    return max(1, int(t) + 1)


def write_terminals(writer):
    # Terminals
    writer.write('terminals {\n'
                 '  0 : EOF ;\n')  # required by Elkhound
    for t in terminals:
        symbol = map_term(t)
        writer.write('  {} : T{} ;\n'.format(symbol, symbol))
    writer.write('}\n')


def write_recognize(out_dir, grammar_name, count):
    grammar_name = '{}{}'.format(grammar_name, 'Count' if count else 'Recognize')
    with open('{}/{}.gr'.format(out_dir, grammar_name), 'w') as writer:
        writer.write(
            'context_class {} : public UserActions {{}};\n'.format(grammar_name))

        write_terminals(writer)

        # Nonterminals
        def write_nonterm(symbol, production):
            ntype = 'long int' if count else 'bool'
            writer.write('nonterm({}) S{} {{\n'.format(ntype, symbol))
            merge = 'a + b' if count else 'a | b'
            writer.write(
                '  fun dup(n) { return n; }\n'
                '  fun del(n) {}\n'
                '  fun merge(a, b) { return ' + merge + '; }\n\n')
            for rhss in production:
                if not rhss:
                    writer.write(' -> { return 1; }\n')
                    continue
                vars = []
                items = []
                for rhs in rhss:
                    if rhs in terminals:
                        items.append('T{}'.format(map_term(rhs)))
                    else:
                        var = 'n{}'.format(len(vars))
                        vars.append(var)
                        items.append('{}:S{}'.format(var, rhs))
                writer.write(' -> {}'.format(' '.join(items)))
                if vars:
                    writer.write(' {{ return {}; }}\n'.format(' * '.join(vars)))
                else:
                    writer.write(' { return 1; }\n')

            writer.write('}\n')

        write_nonterm(start, productions[start])
        for s, p in productions.items():
            if s != start:
                write_nonterm(s, p)


def write_tree(out_dir, grammar_name, forest):
    grammar_name = '{}{}'.format(grammar_name, 'Forest' if forest else 'Tree')
    with open('{}/{}.gr'.format(out_dir, grammar_name), 'w') as writer:
        writer.write(
            'verbatim {\n'
            '#include "benchmark/elkhound/ElkhoundNode.hpp"\n'
            '}\n'
            'context_class ' + grammar_name + ' : public UserActions {};\n')

        write_terminals(writer)

        # Nonterminals
        def write_nonterm(symbol, production):
            writer.write('nonterm(benchmark::ElkhoundNode*) S{} {{\n'.format(symbol))
            merge = (
                'return new benchmark::ElkhoundNode(lhs, rhs, true);'
                if forest else 'if (rhs) { rhs->delRef(); } return lhs;')
            writer.write(
                '  fun dup(n) { n->newRef(); return n; }\n'
                '  fun del(n) { n->delRef(); }\n'
                '  fun merge(lhs, rhs) { ' + merge + ' }\n\n')
            for rhss in production:
                vars = []
                items = []
                for rhs in rhss:
                    if rhs in terminals:
                        items.append('T{}'.format(map_term(rhs)))
                    else:
                        var = 'n{}'.format(len(vars))
                        vars.append(var)
                        items.append('{}:S{}'.format(var, rhs))
                writer.write(' -> {}'.format(' '.join(items)))

                def make_node(n):
                    if len(n) == 1:
                        return n[0]
                    half = len(n) // 2
                    return 'new benchmark::ElkhoundNode({}, {})'.format(make_node(n[:half]),
                                                                        make_node(n[half:]))

                if vars:
                    writer.write(' {{ return {}; }}\n'.format(make_node(vars)))
                else:
                    writer.write(' { return nullptr; }\n')

            writer.write('}\n')

        write_nonterm(start, productions[start])
        for s, p in productions.items():
            if s != start:
                write_nonterm(s, p)


def write_registration(out_dir, grammar_name, register_name):
    with open('{}/{}Register.cpp'.format(out_dir, grammar_name), 'w') as writer:
        writer.write('''
#include "useract.h"

#include "benchmark/elkhound/ElkhoundParsersRegistry.hpp"
#include "benchmark/elkhound/gen/{0}Recognize.h"
#include "benchmark/elkhound/gen/{0}Count.h"
#include "benchmark/elkhound/gen/{0}Tree.h"
#include "benchmark/elkhound/gen/{0}Forest.h"

namespace benchmark {{
namespace {{

std::unique_ptr<UserActions> makeParser(ParseMode parseMode) {{
    switch (parseMode) {{
        case ParseMode::Recognition:
            return std::make_unique<{0}Recognize>();
        case ParseMode::Count:
            return std::make_unique<{0}Count>();
        case ParseMode::ParseTree:
            return std::make_unique<{0}Tree>();
        case ParseMode::ParseForest:
        case ParseMode::ParseForestCount:
            return std::make_unique<{0}Forest>();
        default:
            return nullptr;
    }}
}}

}}

ElkhoundParsersRegistry::RegisterParserFactory registerParserFactory{0}{{
    "{1}", &makeParser}};

}}  // namespace benchmark
'''.format(grammar_name, register_name))


def main():
    rp_grammar = sys.argv[1]
    grammar_name = sys.argv[2]
    register_name = sys.argv[3]
    out_dir = sys.argv[4]
    get_symbols(rp_grammar)
    write_recognize(out_dir, grammar_name, count=False)
    write_recognize(out_dir, grammar_name, count=True)
    write_tree(out_dir, grammar_name, forest=False)
    write_tree(out_dir, grammar_name, forest=True)
    write_registration(out_dir, grammar_name, register_name)


if __name__ == '__main__':
    main()
