#ifndef RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDNODE_HPP
#define RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDNODE_HPP

#include "benchmark/Settings.hpp"

namespace benchmark {

class ElkhoundNode {
 public:
  ElkhoundNode(ElkhoundNode* lhs, ElkhoundNode* rhs, bool add = false)
      : lhs{lhs}, rhs{rhs}, add{add} {
  }

  ElkhoundNode* lhs;
  ElkhoundNode* rhs;
  bool add;

  void newRef() {
    ++refCount_;
  }

  void delRef() {
    if (--refCount_ == 0) {
      if (lhs) {
        lhs->delRef();
      }
      if (rhs) {
        rhs->delRef();
      }
      delete this;
    }
  }

 private:
  int refCount_{1};
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDNODE_HPP
