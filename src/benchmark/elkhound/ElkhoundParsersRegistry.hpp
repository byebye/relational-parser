#ifndef RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDPARSERSREGISTRY_HPP
#define RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDPARSERSREGISTRY_HPP

#include <absl/strings/str_join.h>
#include <fmt/format.h>
#include <cassert>
#include <functional>
#include <map>
#include <set>

#include "useract.h"

#include "benchmark/Settings.hpp"

namespace benchmark {

class ElkhoundParsersRegistry {
 public:
  ElkhoundParsersRegistry(const ElkhoundParsersRegistry&) = delete;

  struct RegisterParserFactory {
    RegisterParserFactory(std::string name,
                          std::function<std::unique_ptr<UserActions>(ParseMode)> factory) {
      bool inserted = instance().registry_.emplace(std::move(name), std::move(factory)).second;
      assert(inserted);
    }
  };

  static std::set<std::string> registeredNames() {
    std::set<std::string> result;
    for (auto const& element : instance().registry_) {
      result.emplace(element.first);
    }
    return result;
  }

  static bool isRegistered(std::string const& name) {
    return instance().registry_.contains(name);
  }

  static std::unique_ptr<UserActions> create(std::string const& name, ParseMode parseMode) {
    if (auto it = instance().registry_.find(name); it != instance().registry_.end()) {
      return (it->second)(parseMode);
    }
    throw std::invalid_argument(
        fmt::format("Grammar '{}' has not been registered. Must be one of: {}", name,
                    absl::StrJoin(registeredNames(), ", ")));
  }

 private:
  ElkhoundParsersRegistry() = default;

  static ElkhoundParsersRegistry& instance() {
    static ElkhoundParsersRegistry registry;
    return registry;
  }

  std::map<std::string, std::function<std::unique_ptr<UserActions>(ParseMode)>> registry_;
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_BENCHMARK_ELKHOUND_ELKHOUNDPARSERSREGISTRY_HPP
