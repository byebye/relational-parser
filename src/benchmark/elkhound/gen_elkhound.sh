#!/bin/bash

dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
root_dir=${dir}/../../..
res=${root_dir}/resources

mkdir -p ${root_dir}/lib/elkhound/build
cd ${root_dir}/lib/elkhound/build
cmake -Wno-dev ../src/ -DCMAKE_BUILD_TYPE=Release -DEXTRAS=OFF -DOCAML=OFF
cd ${dir}

elkhound_bin=${root_dir}/lib/elkhound/build/elkhound/elkhound
cd $dir
mkdir -p gen/
for grammar in Java8 Java JSON XML arithmetic arithmetic-replica rec-right rec-left ; do
  g=$(echo $grammar | sed -r 's/(^|-)(\w)/\U\2/g')
  echo "Input: $grammar, output: $g"
  register=${grammar,,}
  python elkhound.py ${res}/${grammar}.rp ${g} ${register} gen || exit 1
  cd gen/
  ${elkhound_bin} ${g}Recognize.gr
  ${elkhound_bin} ${g}Count.gr
  ${elkhound_bin} ${g}Tree.gr
  ${elkhound_bin} ${g}Forest.gr
  cd ..
done