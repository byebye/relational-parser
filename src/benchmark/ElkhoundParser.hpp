#ifndef RELATIONAL_PARSER_SRC_BENCHMARK_ElkhoundParser_HPP
#define RELATIONAL_PARSER_SRC_BENCHMARK_ElkhoundParser_HPP

#include "benchmark/Parser.hpp"
#include "benchmark/Settings.hpp"

#include "glr.h"
#include "useract.h"

namespace benchmark {

class ElkhoundParser : public Parser {
 public:
  explicit ElkhoundParser(std::string const& grammarName, ParseMode parseMode);

  std::optional<Count> parse(std::vector<Symbol> const& tokens) override;

 private:
  std::unique_ptr<UserActions> grammar_;
  std::unique_ptr<GLR> parser_;
  ParseMode parseMode_;
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_SRC_BENCHMARK_ElkhoundParser_HPP
