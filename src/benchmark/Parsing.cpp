#include "benchmark/Parsing.hpp"

#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <chrono>
#include <fstream>

#include "Input.hpp"
#include "Statistics.hpp"
#include "Timer.hpp"
#include "benchmark/AntlrParser.hpp"
#include "benchmark/ElkhoundParser.hpp"
#include "benchmark/RelationalParser.hpp"
#include "benchmark/YaepParser.hpp"
#include "util/Printing.hpp"

namespace benchmark {
namespace {

using relparser::getInputFiles;
using relparser::loadRpGrammar;
using relparser::loadTokens;
using relparser::Statistics;
using relparser::Timer;
using stdutil::openFileOrThrow;

void printResult(std::size_t numParseTrees, std::string_view inputFile, std::fstream& resultsStream,
                 CommandlineFlags const& flags) {
  SPDLOG_INFO("Result - number of parse trees: {}", numParseTrees);

  if (!flags.resultsFile) {
    return;
  }
  SPDLOG_INFO("Printing results to file {}", *flags.resultsFile);
  STATS("ParseRunner::printResult");
  spdlog::default_logger()->flush();

  fmt::print(resultsStream, "{}: {}\n", inputFile, stdutil::Printable{numParseTrees});
  resultsStream.flush();
}

}  // namespace

void runParsing(CommandlineFlags const& flags) {
  using namespace std::chrono;

  Timer totalRunTimer{};

  fmt::print("\nUsing grammar: {}\n", flags.grammar);
  Timer preprocessTimer{};
  auto parser = parsers.at(flags.parser)(flags.grammar, parsingModes.at(flags.parsingMode));
  Statistics::time("ParseRunner::preprocessing") = preprocessTimer.stop().ns();
  SPDLOG_INFO("Preprocessing time: {}", preprocessTimer);

  nanoseconds totalParsingTime{};

  std::fstream resultsStream;
  std::size_t numTotalParseTrees{0};
  if (flags.resultsFile) {
    resultsStream = openFileOrThrow(*flags.resultsFile, std::ios_base::out);
  }
  std::size_t numTokens{0};
  std::size_t numNotAccepted{0};
  std::size_t lastUpdated{0};
  auto parseFile = [&](std::string const& inputFile) {
    SPDLOG_DEBUG("Parsing file {}", inputFile);
    std::vector<Symbol> tokens;
    try {
      TIMER("ParseRunner::readTokens");
      tokens = loadTokens(inputFile);
    }
    catch (const std::exception& ex) {
      SPDLOG_ERROR("Error filling tokens from file {}", inputFile);
      std::exit(1);
    }
    numTokens += tokens.size();

    Timer parseTimer{};
    auto numParseTrees = parser->parse(tokens);
    Statistics::time("ParseRunner::parsing") += parseTimer.stop().ns();
    SPDLOG_INFO("{} - parsing time: {}", inputFile, parseTimer);

    if (numTokens / flags.statusUpdateEveryTokens != lastUpdated) {
      lastUpdated = numTokens / flags.statusUpdateEveryTokens;
      fmt::print("\r{}", numTokens);
      std::fflush(stdout);
    }

    Timer resultsTimer{};
    printResult(numParseTrees.value_or(0), inputFile, resultsStream, flags);
    Statistics::time("ParseRunner::printResults") += resultsTimer.stop().ns();
    SPDLOG_INFO("{} - printing results time: {}", inputFile, resultsTimer);

    numTotalParseTrees += numParseTrees.value_or(0);
    numNotAccepted += !numParseTrees.has_value();
  };

  auto const& inputFiles =
      !flags.inputFiles.empty()
          ? flags.inputFiles
          : getInputFiles(*flags.inputDirectory, flags.recursive, flags.inputFilesRegex);
  SPDLOG_INFO("Parsing {} input files with parser: {} using mode: {}", inputFiles.size(),
              flags.parser, flags.parsingMode);
  fmt::print("Parsing {} input files with parser: {} using mode: {}\n", inputFiles.size(),
             flags.parser, flags.parsingMode);
  std::ranges::for_each(inputFiles, parseFile);

  totalRunTimer.stop();

  auto parsingTime = duration_cast<milliseconds>(Statistics::time("ParseRunner::parsing"));
  SPDLOG_INFO("Parsed {} files with {} tokens in {}", inputFiles.size(), numTokens, parsingTime);
  fmt::print("\n\nParsed {} files with {} tokens in {}\n", inputFiles.size(), numTokens,
             parsingTime);

  SPDLOG_INFO("Total parse trees: {}, not accepted: {}", numTotalParseTrees, numNotAccepted);
  SPDLOG_INFO("Total execution time: {}", totalRunTimer.ms());
  SPDLOG_INFO("{}", Statistics::StatsPrinter{});
  fmt::print("Total parse trees: {}, not accepted: {}\n", numTotalParseTrees, numNotAccepted);
  fmt::print("Total execution time: {}\n", totalRunTimer.ms());
  fmt::print("\n{}\n", Statistics::StatsPrinter{});
}

template<typename P>
std::unique_ptr<Parser> makeParser(std::string const& grammar, ParseMode parseMode) {
  return std::make_unique<P>(grammar, parseMode);
}

std::map<std::string, ParseMode> const parsingModes{
    {"recognize", ParseMode::Recognition},
    {"count", ParseMode::Count},
    {"parse", ParseMode::ParseTree},
    {"forest", ParseMode::ParseForest},
    {"forestcount", ParseMode::ParseForestCount},
};

std::map<std::string, std::function<std::unique_ptr<Parser>(std::string const&, ParseMode)>> const
    parsers{
        {"relparser", &makeParser<RelationalParser>},
        {"yaep", &makeParser<YaepParser>},
        {"antlr", &makeParser<AntlrParser>},
        {"elkhound", &makeParser<ElkhoundParser>},
    };

}  // namespace benchmark
