#ifndef RELATIONAL_PARSER_SRC_BENCHMARK_YAEPPARSER_HPP
#define RELATIONAL_PARSER_SRC_BENCHMARK_YAEPPARSER_HPP

#include "benchmark/Parser.hpp"
#include "benchmark/Settings.hpp"
#include "yaep.h"

namespace benchmark {

class YaepParser : public Parser {
 public:
  explicit YaepParser(std::string const& rpGrammarFile, ParseMode parseMode);

  std::optional<Count> parse(std::vector<Symbol> const& tokens) override;

 private:
  yaep parser_;
  std::string description_;
  ParseMode parseMode_;

  static int tokenIndex_;
  static std::vector<Symbol> const* tokens_;

  static int readToken(void** attr);

  static void syntaxError(int err_tok_num, void* err_tok_attr, int start_ignored_tok_num,
                          void* start_ignored_tok_attr, int start_recovered_tok_num,
                          void* start_recovered_tok_attr);
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_SRC_BENCHMARK_YAEPPARSER_HPP
