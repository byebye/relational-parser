#ifndef RELATIONAL_PARSER_SRC_BENCHMARK_RELATIONALPARSER_HPP
#define RELATIONAL_PARSER_SRC_BENCHMARK_RELATIONALPARSER_HPP

#include "benchmark/Parser.hpp"
#include "benchmark/Settings.hpp"

namespace benchmark {

class RelationalParser : public Parser {
 public:
  explicit RelationalParser(std::string const& rpGrammarFile, ParseMode parseMode);

  std::optional<Count> parse(std::vector<Symbol> const& tokens) override;

 private:
  ParseMode parseMode_;
  // Keep relational parser through the interface to avoid templates in this header.
  std::unique_ptr<Parser> relparser_;
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_SRC_BENCHMARK_RELATIONALPARSER_HPP
