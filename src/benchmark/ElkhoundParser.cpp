#include "benchmark/ElkhoundParser.hpp"

#include <spdlog/spdlog.h>

#include "glr.h"
#include "lexerint.h"
#include "useract.h"

#include "Statistics.hpp"
#include "benchmark/elkhound/ElkhoundNode.hpp"
#include "benchmark/elkhound/ElkhoundParsersRegistry.hpp"

namespace benchmark {
namespace {

class Lexer : public LexerInterface {
 public:
  static int tokenIndex_;
  static std::vector<Symbol> const* tokens_;

  explicit Lexer(std::vector<Symbol> const& tokens) {
    tokenIndex_ = 0;
    tokens_ = &tokens;
  }

  static void nextToken(LexerInterface* lexer) {
    if (tokenIndex_ < tokens_->size()) {
      int t = (*tokens_)[tokenIndex_++];
      // Terminals have indices starting from 1 + optional ANTLR's EOF == -1,
      // so they are all mapped cause 0 is reserved for Elkhound's EOF marker.
      lexer->type = std::max(1, t + 1);
    }
    else {
      // Elkhound's EOF marker.
      lexer->type = 0;
    }
  }

  NextTokenFunc getTokenFunc() const override {
    return &Lexer::nextToken;
  }

  string tokenDesc() const override {
    return "";
  }

  string tokenKindDesc(int kind) const override {
    return "";
  }
};

int Lexer::tokenIndex_{0};
std::vector<Symbol> const* Lexer::tokens_{nullptr};

Count countParseTrees(ElkhoundNode const* node,
                      absl::flat_hash_map<ElkhoundNode const*, Count>& cache) {
  if (!node) {
    return 1;
  }
  if (auto it = cache.find(node); it != cache.end()) {
    return it->second;
  }
  Count x = countParseTrees(node->lhs, cache);
  Count y = countParseTrees(node->rhs, cache);
  Count result = node->add ? x + y : x * y;
  cache.emplace(node, result);
  return result;
}

Count countParseTrees(ElkhoundNode const* node) {
  absl::flat_hash_map<ElkhoundNode const*, Count> cache;
  return countParseTrees(node, cache);
}

}  // namespace

using relparser::Statistics;

ElkhoundParser::ElkhoundParser(std::string const& grammarName, ParseMode parseMode)
    : parseMode_{parseMode},
      grammar_{ElkhoundParsersRegistry::create(grammarName, parseMode)},
      parser_{std::make_unique<GLR>(grammar_.get(), grammar_->makeTables())} {
  parser_->noisyFailedParse = false;
}

std::optional<Count> ElkhoundParser::parse(std::vector<Symbol> const& tokens) {
  Lexer lexer{tokens};
  Lexer::nextToken(&lexer);

  {
    TIMER("ElkhoundParser::parse");
    SemanticValue value{0};
    if (!parser_->glrParse(lexer, value)) {
      return std::nullopt;
    }
    switch (parseMode_) {
      case ParseMode::Recognition:
      case ParseMode::ParseTree:
      case ParseMode::ParseForest: return 1;
      case ParseMode::Count: return value;
      case ParseMode::ParseForestCount: {
        auto root = reinterpret_cast<ElkhoundNode*>(value);
        Count result{0};
        {
          TIMER("ElkhoundParser::countParseTrees");
          result = countParseTrees(root);
        }
        {
          TIMER("ElkhoundParser::freeMemory");
          root->delRef();
        }
        return result;
      }
      default: return std::nullopt;
    }
  }
  return std::nullopt;
}

}  // namespace benchmark
