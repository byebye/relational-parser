#ifndef ANTLR_PARSER_SRC_BENCHMARK_ANTLRPARSER_HPP
#define ANTLR_PARSER_SRC_BENCHMARK_ANTLRPARSER_HPP

#include "antlr/AntlrParser.hpp"
#include "benchmark/Parser.hpp"
#include "benchmark/Settings.hpp"

namespace benchmark {

class AntlrParser : public Parser {
 public:
  explicit AntlrParser(std::string const& grammarName, ParseMode parseMode);

  // Define in .cpp file to make it compile with only Antlr forward declarations.
  ~AntlrParser() override;

  std::optional<Count> parse(std::vector<Symbol> const& tokens) override;

 private:
  ParseMode parseMode_;
  // Use unique_ptr to include only Antlr forward declarations.
  std::unique_ptr<relparser::AntlrParser> parser_;
};

}  // namespace benchmark

#endif  // ANTLR_PARSER_SRC_BENCHMARK_ANTLRPARSER_HPP
