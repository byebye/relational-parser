#ifndef RELATIONAL_PARSER_SRC_BENCHMARK_PARSER_HPP
#define RELATIONAL_PARSER_SRC_BENCHMARK_PARSER_HPP

#include <optional>
#include <vector>

#include "benchmark/Settings.hpp"

namespace benchmark {

class Parser {
 public:
  virtual ~Parser() = default;

  virtual std::optional<Count> parse(std::vector<Symbol> const& tokens) = 0;
};

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_SRC_BENCHMARK_PARSER_HPP
