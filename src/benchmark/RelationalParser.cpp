#include "benchmark/RelationalParser.hpp"

#include "Input.hpp"
#include "Statistics.hpp"
#include "benchmark/Settings.hpp"
#include "parser/Engines.hpp"
#include "parser/Output.hpp"
#include "parser/Parser.hpp"

namespace benchmark {
namespace {

using relparser::Statistics;

template<relparser::interface::Parser P>
class Relparser : public Parser {
 public:
  explicit Relparser(P&& relparser, ParseMode parseMode)
      : relparser_{std::move(relparser)}, parseMode_{parseMode} {
  }

  std::optional<Count> parse(std::vector<Symbol> const& tokens) override {
    // RelationEngine result.
    using Relation = typename P::Relation;
    // LabelEngine epsilon label.
    using EpsilonValue = typename P::EpsilonValue;
    using LabelEngine = typename P::AtomicEngine::LabelEngine;

    Relation relation;
    {
      TIMER("RelationalParser::parse");
      relation = relparser_.parse(tokens);
    }
    EpsilonValue epsilon;
    {
      TIMER("RelationalParser::epsilon");
      auto eps = relparser_.epsilon(relation);
      if (!eps) {
        return std::nullopt;
      }
      epsilon = *eps;
    }
    {
      constexpr bool isParseTree = relparser::interface::Relation<EpsilonValue>;
      constexpr bool isParseForest = requires(EpsilonValue v) {
        {std::get<0>(*v)};
      };
      if constexpr (isParseTree) {
        return 1;
      }
      else if constexpr (isParseForest) {
        if (parseMode_ == ParseMode::ParseForestCount) {
          TIMER("RelationalParser::countParseTrees");
          relparser::ParseForest<LabelEngine> parseForest{epsilon};
          return parseForest.numParseTrees();
        }
        return 1;
      }
      else {
        return epsilon;
      }
    }
  }

 private:
  P relparser_;
  ParseMode parseMode_;
};

template<typename RelationsEngineProvider>
std::unique_ptr<Parser> createParser(Grammar&& grammar, ParseMode parseMode) {
  using RelationEngine = typename RelationsEngineProvider::RelationEngine;
  using Parser = relparser::Parser<RelationEngine>;
  RelationsEngineProvider provider;
  return std::make_unique<Relparser<Parser>>(Parser{provider(std::move(grammar)), 0}, parseMode);
}

std::map<ParseMode, std::function<std::unique_ptr<Parser>(Grammar&&, ParseMode)>> const makeParser{
    {ParseMode::Recognition, &createParser<relparser::RecognitionStackEngineOpt>},
    {ParseMode::Count, &createParser<relparser::CountingStackEngineOpt>},
    {ParseMode::ParseTree, &createParser<relparser::ParsingStackEngineOpt>},
    {ParseMode::ParseForest, &createParser<relparser::ForestStackEngineOpt>},
    {ParseMode::ParseForestCount, &createParser<relparser::ForestStackEngineOpt>},
};

}  // namespace

RelationalParser::RelationalParser(std::string const& rpGrammarFile, ParseMode parseMode)
    : parseMode_{parseMode},
      relparser_{makeParser.at(parseMode)(relparser::loadRpGrammar(rpGrammarFile), parseMode)} {
}

std::optional<Count> RelationalParser::parse(std::vector<Symbol> const& tokens) {
  return relparser_->parse(tokens);
}

}  // namespace benchmark
