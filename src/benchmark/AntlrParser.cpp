#include "benchmark/AntlrParser.hpp"

#include "antlr4-runtime.h"

#include "Statistics.hpp"
#include "antlr/AntlrParser.hpp"
#include "antlr/AntlrParsersRegistry.hpp"
#include "benchmark/Settings.hpp"

namespace benchmark {

using relparser::AntlrParsersRegistry;
using relparser::Statistics;
using relparser::Timer;

AntlrParser::AntlrParser(std::string const& grammarName, ParseMode parseMode)
    : parseMode_{parseMode}, parser_{AntlrParsersRegistry::create(grammarName)} {
  if (parseMode != ParseMode::Recognition && parseMode != ParseMode::ParseTree) {
    throw std::invalid_argument(fmt::format("Unsuported parse mode: {}", parseMode));
  }
  parser_->parser()->setBuildParseTree(parseMode == ParseMode::ParseTree);
  parser_->parser()->removeErrorListeners();
  // Terminate immediately on parsing error.
  parser_->parser()->setErrorHandler(std::make_shared<antlr4::BailErrorStrategy>());
}

AntlrParser::~AntlrParser() = default;

std::optional<Count> AntlrParser::parse(std::vector<Symbol> const& tokens) {
  antlr4::CommonTokenFactory tokenFactory;

  // As suggested in ANTLR documentation, try faster SLL first, then only if there's a syntax error
  // fallback to the slower LL.
  for (auto predictionMode : {antlr4::atn::PredictionMode::SLL, antlr4::atn::PredictionMode::LL}) {
    COUNTER("AntlrParser::parseRuns");
    std::vector<std::unique_ptr<antlr4::Token>> antlrTokens;

    Timer prepareTokens{};
    antlrTokens.reserve(tokens.size());
    for (Symbol token : tokens) {
      if (token == -1) {
        antlrTokens.emplace_back(tokenFactory.create(antlr4::Token::EOF, ""));
      }
      else {
        antlrTokens.emplace_back(tokenFactory.create(token, ""));
      }
    }
    antlr4::ListTokenSource tokenSource(std::move(antlrTokens));
    antlr4::UnbufferedTokenStream tokenStream(&tokenSource);
    parser_->parser()->getInterpreter<antlr4::atn::ParserATNSimulator>()->setPredictionMode(
        predictionMode);
    parser_->parser()->setTokenStream(&tokenStream);
    Statistics::time("AntlrParser::prepareTokens") += prepareTokens.stop().ns();
    try {
      TIMER("AntlrParser::parse");
      parser_->parse();
      return 1;
    }
    catch (antlr4::ParseCancellationException&) {
      continue;
    }
  }
  return absl::nullopt;
}

}  // namespace benchmark
