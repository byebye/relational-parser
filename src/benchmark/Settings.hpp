#ifndef RELATIONAL_PARSER_SRC_BENCHMARK_SETTINGS_HPP
#define RELATIONAL_PARSER_SRC_BENCHMARK_SETTINGS_HPP

#include "parser/Grammar.hpp"

namespace benchmark {
using Symbol = int;
using Grammar = relparser::Grammar<benchmark::Symbol>;
using Production = Grammar::Production;
using Count = std::size_t;

enum class ParseMode { Recognition, Count, ParseTree, ParseForest, ParseForestCount };

}  // namespace benchmark

#endif  // RELATIONAL_PARSER_SRC_BENCHMARK_SETTINGS_HPP
