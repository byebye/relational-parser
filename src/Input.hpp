#ifndef RELATIONAL_PARSER_PARSER_PARSER_INPUT_HPP
#define RELATIONAL_PARSER_PARSER_PARSER_INPUT_HPP

#include <fmt/ostream.h>
#include <fstream>

#include "parser/Grammar.hpp"

namespace relparser {

using Symbol = int;

// Reads custom format (.rp) grammar in format:  [<LABEL>]: <LHS> ->[ <RHS> ...], e.g. A: 1 -> 2 3
Grammar<Symbol> loadRpGrammar(std::string const& filename);

std::vector<Symbol> loadTokens(std::string const& filename);

std::vector<std::string> getInputFiles(std::string_view directory, bool recursive,
                                       std::optional<std::string> const& filesRegex);

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_PARSER_INPUT_HPP
