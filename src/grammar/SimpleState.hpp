#ifndef RELATIONAL_PARSER_GRAMMAR_SIMPLESTATE_HPP
#define RELATIONAL_PARSER_GRAMMAR_SIMPLESTATE_HPP

#include <ostream>

namespace relparser {

template<typename TId>
struct SimpleState {
  TId id;

  friend bool operator==(SimpleState lhs, SimpleState rhs) = default;

  friend std::strong_ordering operator<=>(SimpleState lhs, SimpleState rhs) {
    return lhs.id <=> rhs.id;
  }

  friend std::ostream& operator<<(std::ostream& os, SimpleState state) {
    return os << state.id;
  }
};

}  // namespace relparser

namespace std {

template<typename TId>
struct hash<relparser::SimpleState<TId>> {
  std::size_t operator()(relparser::SimpleState<TId> state) const {
    return hash<TId>{}(state.id);
  }
};

}  // namespace std

#endif  // RELATIONAL_PARSER_GRAMMAR_SIMPLESTATE_HPP
