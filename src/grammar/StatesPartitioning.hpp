#ifndef RELATIONAL_PARSER_GRAMMAR_STATESPARTITIONING_HPP
#define RELATIONAL_PARSER_GRAMMAR_STATESPARTITIONING_HPP

#include <vector>

#include "CommonTypes.hpp"

namespace relparser {

template<typename TState>
class StatesPartitioning {
 public:
  using PartitionId = int;

  explicit StatesPartitioning(std::vector<TState> singlePartition)
      : StatesPartitioning{std::vector<std::vector<TState>>{std::move(singlePartition)}} {
  }

  explicit StatesPartitioning(std::vector<std::vector<TState>> partitions)
      : partitions_(std::move(partitions)) {
    std::size_t partitionId = 0;
    for (auto&& equivalentStates : partitions_) {
      for (TState state : equivalentStates) {
        statesPartitionIds_.emplace(state, partitionId);
      }
      ++partitionId;
    }
  }

  PartitionId partitionId(TState state) const {
    if (auto it = statesPartitionIds_.find(state); it != statesPartitionIds_.cend()) {
      return it->second;
    }
    throw std::out_of_range(
        fmt::format("Provided state {} is not present in any partition.", state));
  }

  std::vector<std::vector<TState>> const& partitions() const {
    return partitions_;
  }

  bool operator==(StatesPartitioning const& rhs) const {
    return partitions_.size() == rhs.partitions_.size();
  }

  bool operator!=(StatesPartitioning const& rhs) const {
    return !(rhs == *this);
  }

 private:
  Map<TState, PartitionId> statesPartitionIds_;
  std::vector<std::vector<TState>> partitions_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_STATESPARTITIONING_HPP
