#ifndef RELATIONAL_PARSER_GRAMMAR_TYPES_HPP
#define RELATIONAL_PARSER_GRAMMAR_TYPES_HPP

#include "grammar/RtnGrammar.hpp"
#include "grammar/SimpleState.hpp"

namespace relparser {

using TSymbol = int;
using TState = relparser::SimpleState<int>;
using TRtnGrammar = relparser::RtnGrammar<TState, std::ptrdiff_t>;

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_TYPES_HPP
