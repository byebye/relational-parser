#ifndef RELATIONAL_PARSER_GRAMMAR_OPTIMIZEDGRAMMAR_HPP
#define RELATIONAL_PARSER_GRAMMAR_OPTIMIZEDGRAMMAR_HPP

#include <spdlog/spdlog.h>
#include <array>
#include <vector>

#include "CommonTypes.hpp"
#include "grammar/BackwardGrammarOptimizer.hpp"
#include "grammar/EpsilonEdgesGrammarOptimizer.hpp"
#include "grammar/ForwardGrammarOptimizer.hpp"
#include "util/Container.hpp"

namespace relparser {

template<typename TGrammar>
struct GrammarOptimizer {
  static inline std::array const supportedOptimizers{"epsilon", "backward", "forward", "none",
                                                     "all"};

  static inline Map<std::string, std::function<TGrammar(TGrammar const&)>> const optimizers{
      {{"epsilon", EpsilonEdgesGrammarOptimizer<TGrammar>{}},
       {"backward", BackwardGrammarOptimizer<TGrammar>{}},
       {"forward", ForwardGrammarOptimizer<TGrammar>{}}}};

  // Optimizes number of states of the given grammar using selected optimizers.
  TGrammar operator()(TGrammar grammar, std::vector<std::string> selectedOptimizers) const {
    if (!selectedOptimizers.empty() && stdutil::contains(selectedOptimizers, "none")) {
      return grammar;
    }
    if (selectedOptimizers.empty() || stdutil::contains(selectedOptimizers, "all")) {
      selectedOptimizers = {"epsilon", "backward", "forward"};
    }

    TGrammar optimizedGrammar{std::move(grammar)};
    for (auto const& optname : selectedOptimizers) {
      SPDLOG_INFO("Applying grammar optimizer {}", optname);
      std::size_t oldStatesCount = optimizedGrammar.allStates().size();

      optimizedGrammar = optimizers.at(optname)(optimizedGrammar);

      std::size_t newStatesCount = optimizedGrammar.allStates().size();
      SPDLOG_INFO("Grammar optimizer {} reduced number of states: {} -> {}", optname,
                  oldStatesCount, newStatesCount);
    }
    return optimizedGrammar;
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_OPTIMIZEDGRAMMAR_HPP
