#include <absl/strings/str_join.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <CLI/CLI.hpp>
#include <array>
#include <filesystem>
#include <fstream>
#include <ostream>
#include <regex>

#include "AntlrToRtnGrammarConverter.hpp"
#include "antlr/AntlrAtnPrettyPrinter.hpp"
#include "antlr/AntlrAtnRuleVisitor.hpp"
#include "antlr/AntlrParsersRegistry.hpp"
#include "grammar/Grammar.hpp"
#include "grammar/GrammarOptimizer.hpp"
#include "grammar/GrammarToDot.hpp"
#include "grammar/RtnToParserGrammar.hpp"
#include "grammar/SymbolMapping.hpp"
#include "grammar/Types.hpp"
#include "util/Container.hpp"
#include "util/Printing.hpp"

using namespace std::string_literals;
using stdutil::openFileOrThrow;

struct CommandFlags {
  std::string grammar;
  std::string outputFormat;
  std::optional<std::string> outputFile;
  std::optional<std::string> dotOutputFile;
  std::vector<std::string> grammarOptimizers;
  std::vector<std::string> inputFiles;
  std::optional<std::string> inputDirectory;
  std::optional<std::string> inputFilesRegex;
  bool recursive;
  bool saveTokens;
};

using namespace relparser;

bool loadTokens(AntlrParser& antlrParser, std::string const& inputFile) {
  std::fstream fileStream{openFileOrThrow(inputFile, std::ios_base::in)};
  try {
    antlrParser.fillTokens(fileStream);
    return true;
  }
  catch (const std::exception& ex) {
    SPDLOG_ERROR("Error filling tokens from file {}", inputFile);
    return false;
  }
}

std::vector<std::string> getInputFiles(CommandFlags const& command) {
  if (!command.inputFiles.empty()) {
    return command.inputFiles;
  }

  auto dir = command.inputDirectory.value();
  SPDLOG_INFO("Scanning directory {} {} for input files", dir,
              command.recursive ? "recursively" : "");
  if (command.inputFilesRegex) {
    SPDLOG_INFO("Matching input file names with regex '{}'", *command.inputFilesRegex);
  }
  namespace fs = std::filesystem;

  std::vector<std::string> inputFiles;

  std::regex fileRegex{command.inputFilesRegex.value_or(".*")};
  auto addInputFile = [&](fs::directory_entry const& entry) {
    if (entry.is_regular_file() && std::regex_match(entry.path().filename().c_str(), fileRegex)) {
      inputFiles.emplace_back(entry.path().native());
    }
  };

  if (command.recursive) {
    std::ranges::for_each(fs::recursive_directory_iterator(dir), addInputFile);
  }
  else {
    absl::c_for_each(fs::directory_iterator(dir), addInputFile);
  }

  absl::c_sort(inputFiles);
  SPDLOG_INFO("Found {} input files", inputFiles.size());
  return inputFiles;
}

std::array const supportedOutputFormats{"atn", "dot", "rp"};

void setupCommand(CLI::App& app) {
  auto command = std::make_shared<CommandFlags>();

  app.add_option("-g,--grammar", command->grammar,
                 "Input grammar in the ANTLR format. Supported values: "
                     + absl::StrJoin(AntlrParsersRegistry::registeredNames(), ", "))
      ->required()
      ->check([](const std::string& inGrammar) {
        if (AntlrParsersRegistry::isRegistered(inGrammar))
          return std::string{};
        return "Unsupported grammar: " + inGrammar;
      });

  app.add_option(
         "-f,--format", command->outputFormat,
         "Format of the output. Supported formats: " + absl::StrJoin(supportedOutputFormats, ", "))
      ->default_val(supportedOutputFormats.front())
      ->check([&](const std::string& inFormat) {
        if (stdutil::contains(supportedOutputFormats, inFormat))
          return std::string{};
        return "Unsupported format: " + inFormat;
      });

  using GrammarOptimizer = GrammarOptimizer<TRtnGrammar>;

  app.add_option("-t,--grammaropts", command->grammarOptimizers,
                 "Optimize grammar size applying selected optimizers in the specified order. "
                 "Available optimizers: "
                     + absl::StrJoin(GrammarOptimizer::supportedOptimizers, ", "))
      ->default_val("all")
      ->check([](const std::string& optimizer) {
        if (stdutil::contains(GrammarOptimizer::supportedOptimizers, optimizer))
          return std::string{};
        return "Unsupported optimizer: " + optimizer;
      });

  app.add_option("-o,--outfile", command->outputFile,
                 "Output file. If not provided, will be printed to stdout.");
  app.add_option("-i,--dotfile", command->dotOutputFile, "Output file for dot format output.");

  auto dirOpt = app.add_option("-d,--directory", command->inputDirectory,
                               "Directory with the input files to load.");

  app.add_option(
         "-x,--fileregex", command->inputFilesRegex,
         "Regex matching input files names in case the directory is specified. If not provided, "
         "all files will be matched.")
      ->needs(dirOpt);

  app.add_flag("-r,--recursive", command->recursive,
               "Whether the directory should be scanned recursively for input files.")
      ->needs(dirOpt);

  app.add_flag("--savetokens", command->saveTokens,
               "Whether to output lexed tokens to a file. The output files are created in the same "
               "location as the input files with an extension '.tok'.")
      ->needs(dirOpt)
      ->take_last();

  app.callback([command]() {
    SPDLOG_INFO("Printing rtnGrammar {} to file {} using format {}", command->grammar,
                command->outputFile.value_or("stdout"), command->outputFormat);

    std::fstream fileStream;
    std::ostream* outStream = &std::cout;
    if (command->outputFile) {
      fileStream = openFileOrThrow(*command->outputFile, std::ios_base::out);
      outStream = &fileStream;
    }

    auto antlrParser = AntlrParsersRegistry::create(command->grammar);

    if (command->outputFormat == "atn") {
      antlr4::atn::ATN const& atn = antlrParser->parser()->getATN();
      std::vector<antlr4::atn::RuleStartState*> const& rulesStart = atn.ruleToStartState;
      for (auto* start : rulesStart) {
        AntlrAtnRuleVisitor atnVisitor(start->ruleIndex);
        AntlrAtnPrettyPrinter prettyPrinter(*antlrParser->parser(), 2, *outStream);
        atnVisitor.visit(prettyPrinter, start);
      }
      if (!command->saveTokens) {
        return;
      }
    }

    auto rtnGrammar = convertAntlrParser(*antlrParser->parser());
    rtnGrammar = GrammarOptimizer{}(std::move(rtnGrammar), command->grammarOptimizers);

    if (command->outputFormat == "dot") {
      grammarToDot(rtnGrammar, *outStream);
    }
    if (command->dotOutputFile) {
      std::fstream dotStream = openFileOrThrow(*command->dotOutputFile, std::ios_base::out);
      grammarToDot(rtnGrammar, dotStream);
    }

    SymbolMapping<TRtnGrammar, TSymbol> symbolMapping{rtnGrammar};
    auto grammar = rtnToParserGrammar<Grammar<TSymbol>>(symbolMapping);

    if (command->outputFormat == "rp") {
      fmt::print(fileStream, "{}\n", grammar.start);
      for (auto&& [lhs, rhss] : grammar.derivationRules) {
        int i = 0;
        for (auto&& [ruleId, rhs] : rhss) {
          fmt::print(*outStream, "{}: {} ->", grammar.symbolLabel(lhs), lhs);
          for (auto s : rhs) {
            fmt::print(*outStream, " {}", s);
          }
          fmt::print(*outStream, "\n");
        }
      }
      fmt::print(*outStream, "[terminals]\n");
      for (auto t : rtnGrammar.terminals()) {
        fmt::print(*outStream, "{}: {}\n", symbolMapping.terminalLabel(t), t);
      }
    }

    if (command->saveTokens) {
      SPDLOG_INFO("Saving lexed tokens");
      absl::c_for_each(getInputFiles(*command), [&](std::string const& inputFile) {
        if (!loadTokens(*antlrParser, inputFile)) {
          return;
        }
        auto tokensFile = fmt::format("{}.{}.tok", inputFile, command->grammar);
        SPDLOG_DEBUG("Saving lexed tokens to file: {}", tokensFile);
        std::fstream fileStream{openFileOrThrow(tokensFile, std::ios_base::out)};
        fmt::print(fileStream, "{}", stdutil::Printable{antlrParser->tokens, " ", "", ""});
      });
    }
  });
}

int main(int argc, const char* argv[]) {
  CLI::App app{"Tool to output and convert ANTLR grammar."};

  std::string logFile;
  app.add_option("-l,--logfile", logFile, "Log file location (will be truncated if exists)")
      ->default_val("grammar.log");

  app.parse_complete_callback([&]() {
    std::vector<spdlog::sink_ptr> sinks;
    auto stdoutLogger = std::make_shared<spdlog::sinks::stdout_sink_st>();
    stdoutLogger->set_level(spdlog::level::info);
    sinks.push_back(std::move(stdoutLogger));

    auto fileLogger = std::make_shared<spdlog::sinks::basic_file_sink_st>(logFile, true);
    fileLogger->set_level(spdlog::level::debug);
    sinks.push_back(std::move(fileLogger));

    auto combinedLogger = std::make_shared<spdlog::logger>("combined", begin(sinks), end(sinks));
    spdlog::register_logger(combinedLogger);
    spdlog::set_default_logger(combinedLogger);
    spdlog::set_level(spdlog::level::debug);
  });

  setupCommand(app);

  try {
    app.parse(argc, argv);
  }
  catch (CLI::ParseError const& e) {
    return app.exit(e);
  }
  catch (std::exception const& e) {
    fmt::print(stderr, "Exception: {}", e.what());
    return 1;
  }
  return 0;
}
