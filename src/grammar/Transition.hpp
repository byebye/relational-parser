#ifndef RELATIONAL_PARSER_GRAMMAR_TRANSITION_HPP
#define RELATIONAL_PARSER_GRAMMAR_TRANSITION_HPP

namespace relparser {

template<typename TState>
struct Call {
  TState source;
  TState child;
  TState target;

  bool operator==(Call const& rhs) const {
    return source == rhs.source && child == rhs.child && target == rhs.target;
  }

  bool operator!=(Call const& rhs) const {
    return !(rhs == *this);
  }

  bool operator<(Call const& rhs) const {
    return std::tie(source, child, target) < std::tie(rhs.source, rhs.child, rhs.target);
  }

  template<typename H>
  friend H AbslHashValue(H h, Call const& call) {
    return H::combine(std::move(h), call.source, call.child, call.target);
  }
};

template<typename TState>
struct Reduce {
  TState source;
  // TState label; // TLabel

  bool operator==(Reduce const& rhs) const {
    return source == rhs.source;
  }

  bool operator!=(Reduce const& rhs) const {
    return !(rhs == *this);
  }

  bool operator<(Reduce const& rhs) const {
    return source < rhs.source;
  }

  template<typename H>
  friend H AbslHashValue(H h, Reduce const& reduce) {
    return H::combine(std::move(h), reduce.source);
  }
};

template<typename TState, typename TTerminal>
struct Shift {
  TState source;
  TState target;
  TTerminal symbol;

  bool operator==(Shift const& rhs) const {
    return source == rhs.source && target == rhs.target && symbol == rhs.symbol;
  }

  bool operator!=(Shift const& rhs) const {
    return !(rhs == *this);
  }

  bool operator<(Shift const& rhs) const {
    return std::tie(source, target, symbol) < std::tie(rhs.source, rhs.target, rhs.symbol);
  }

  template<typename H>
  friend H AbslHashValue(H h, Shift const& shift) {
    return H::combine(std::move(h), shift.source, shift.target, shift.symbol);
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_TRANSITION_HPP
