#include "AntlrToRtnGrammarConverter.hpp"

#include <stdexcept>

#include "antlr4-runtime.h"

#include "CommonTypes.hpp"
#include "antlr/AntlrAtnRuleVisitor.hpp"
#include "grammar/Transition.hpp"
#include "util/Finally.hpp"

namespace relparser {

namespace detail {

template<typename TGrammar, typename RtnStateFactory>
class AntlrAtnToGrammarConverter {
 public:
  using Transition = antlr4::atn::Transition;
  using ATNState = antlr4::atn::ATNState;
  using StateType = typename TGrammar::StateType;
  using TerminalType = typename TGrammar::TerminalType;

  static TGrammar convert(antlr4::Parser const& parser, RtnStateFactory& rtnStateFactory) {
    return AntlrAtnToGrammarConverter(parser, rtnStateFactory).grammar_;
  }

  void enter(ATNState* state) {
    currentSource_.push(getOrCreateEquivalentRtnState(state));
    if (state->getStateType() == ATNState::RULE_STOP) {
      grammar_.addReduce(Reduce<StateType>{.source = currentSource_.top()});
    }
  }

  void exit(ATNState* state) {
    currentSource_.pop();
  }

  void enter(Transition* trans, bool isLastChild) {
    switch (trans->getSerializationType()) {
      case Transition::ACTION: {
        // Actions are not supported here, but sometimes they appear and don't do anything
        // meaningful. Check if this is the case and treat it simply as an epsilon transition,
        // otherwise throw an exception.
        auto* const action = static_cast<antlr4::atn::ActionTransition*>(trans);
        if (action->actionIndex != INVALID_INDEX && action->actionIndex != 0xFFFF) {
          throw std::invalid_argument("ACTION transitions are not supported.");
        }
        [[fallthrough]];
      }
      case Transition::PRECEDENCE:
        // Precedence is not actually supported. Assume that all rules have the same priority,
        // treating precedence transitions as epsilon transitions.
      case Transition::EPSILON: {
        StateType target = getOrCreateEquivalentRtnState(trans->target);
        grammar_.addCall(
            Call<StateType>{.source = currentSource_.top(), .child = epsilon_, .target = target});
        break;
      }
      case Transition::ATOM:
      case Transition::RANGE:
      case Transition::SET: {
        StateType target = getOrCreateEquivalentRtnState(trans->target);
        for (std::ptrdiff_t label : trans->label().toList()) {
          grammar_.addShift(Shift<StateType, TerminalType>{
              .source = currentSource_.top(), .target = target, .symbol = label});
        }
        break;
      }
      case Transition::RULE: {
        auto* const ruleTarget = static_cast<antlr4::atn::RuleTransition*>(trans)->followState;
        StateType child = getOrCreateEquivalentRtnState(trans->target);
        StateType target = getOrCreateEquivalentRtnState(ruleTarget);
        grammar_.addCall(
            Call<StateType>{.source = currentSource_.top(), .child = child, .target = target});
        break;
      }
      case Transition::NOT_SET: {
        throw std::invalid_argument("NOT_SET transitions are not supported.");
      }
      case Transition::PREDICATE: {
        throw std::invalid_argument("PREDICATE transitions are not supported.");
      }
      case Transition::WILDCARD: {
        throw std::invalid_argument("WILDCARD transitions are not supported.");
      }
    }
  }

  void exit(Transition* trans) {
  }

 private:
  RtnStateFactory& rtnStateFactory_;
  HashMap<ATNState*, StateType> equivalentStates_;
  std::stack<StateType> currentSource_;
  StateType epsilon_;
  TGrammar grammar_;

  AntlrAtnToGrammarConverter(antlr4::Parser const& parser, RtnStateFactory& rtnStateFactory)
      : rtnStateFactory_(rtnStateFactory), epsilon_(rtnStateFactory()) {
    grammar_.addReduce(Reduce<StateType>{.source = epsilon_});
    grammar_.epsilon = epsilon_;

    auto const rulesStart = parser.getATN().ruleToStartState;
    markStartState(rulesStart.front());
    for (antlr4::atn::RuleStartState* start : rulesStart) {
      AntlrAtnRuleVisitor atnVisitor(start->ruleIndex);
      atnVisitor.visit(*this, start);

      std::string const& label = parser.getRuleNames()[start->ruleIndex];
      grammar_.setStateLabel(getOrCreateEquivalentRtnState(start), label);
    }

    for (auto const& shift : grammar_.shiftTransitions) {
      grammar_.setTerminalLabel(shift.symbol, parser.getVocabulary().getDisplayName(shift.symbol));
    }

    grammar_.stop = rtnStateFactory();
    grammar_.setStateLabel(grammar_.stop, "STOP");
  }

  StateType getOrCreateEquivalentRtnState(ATNState* atnState) {
    auto it = equivalentStates_.find(atnState);
    if (it == equivalentStates_.cend()) {
      it = equivalentStates_.emplace(atnState, rtnStateFactory_()).first;
    }
    return it->second;
  }

  void markStartState(ATNState* state) {
    grammar_.start = getOrCreateEquivalentRtnState(state);
  }
};

}  // namespace detail

TRtnGrammar convertAntlrParser(antlr4::Parser const& parser) {
  struct StateFactory {
    using StateType = TState;

    int stateIndex = 0;
    StateType operator()() {
      return StateType{stateIndex++};
    }
  };
  StateFactory stateFactory{};
  return detail::AntlrAtnToGrammarConverter<TRtnGrammar, StateFactory>::convert(parser,
                                                                                stateFactory);
}

}  // namespace relparser
