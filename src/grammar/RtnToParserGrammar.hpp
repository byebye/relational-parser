#ifndef RELATIONAL_PARSER_GRAMMAR_RTNTOPARSERGRAMMAR_HPP
#define RELATIONAL_PARSER_GRAMMAR_RTNTOPARSERGRAMMAR_HPP

#include <fmt/format.h>
#include <variant>

#include "CommonTypes.hpp"
#include "util/Overloaded.hpp"

namespace relparser {

template<typename TGrammar, typename TSymbolMapping>
TGrammar rtnToParserGrammar(TSymbolMapping const& symbolMapping) {
  using TState = typename TSymbolMapping::StateType;
  using TSymbol = typename TSymbolMapping::SymbolType;

  TGrammar parserGrammar;

  auto setStateLabel = [&](TState s) {
    parserGrammar.setSymbolLabel(symbolMapping.state(s), symbolMapping.stateLabel(s));
  };

  auto const& rtnGrammar = symbolMapping.grammar();

  Set<TState> nonEmptyStates;
  for (auto const& shift : rtnGrammar.shiftTransitions) {
    nonEmptyStates.emplace(shift.source);
  }
  for (auto const& call : rtnGrammar.callTransitions) {
    nonEmptyStates.emplace(call.source);
    nonEmptyStates.emplace(call.child);
  }

  parserGrammar.start = symbolMapping.state(rtnGrammar.start);
  setStateLabel(rtnGrammar.start);

  for (auto const& shift : rtnGrammar.shiftTransitions) {
    setStateLabel(shift.source);
    parserGrammar.setSymbolLabel(symbolMapping.terminal(shift.symbol),
                                 symbolMapping.terminalLabel(shift.symbol));
    std::vector<TSymbol> rhs{symbolMapping.terminal(shift.symbol)};
    if (nonEmptyStates.contains(shift.target)) {
      rhs.emplace_back(symbolMapping.state(shift.target));
      setStateLabel(shift.target);
    }
    parserGrammar.addDerivation(symbolMapping.state(shift.source), std::move(rhs));
  }
  for (auto const& call : rtnGrammar.callTransitions) {
    setStateLabel(call.source);
    setStateLabel(call.child);
    std::vector<TSymbol> rhs{symbolMapping.state(call.child)};
    if (nonEmptyStates.contains(call.target)) {
      rhs.emplace_back(symbolMapping.state(call.target));
      setStateLabel(call.target);
    }
    parserGrammar.addDerivation(symbolMapping.state(call.source), std::move(rhs));
  }
  for (auto const& reduce : rtnGrammar.reduceTransitions) {
    if (nonEmptyStates.contains(reduce.source)) {
      parserGrammar.addDerivation(symbolMapping.state(reduce.source), {});
      setStateLabel(reduce.source);
    }
  }
  return parserGrammar;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_RTNTOPARSERGRAMMAR_HPP
