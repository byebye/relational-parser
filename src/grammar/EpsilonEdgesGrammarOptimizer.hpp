#ifndef RELATIONAL_PARSER_GRAMMAR_EPSILONEDGESGRAMMAROPTIMIZER_HPP
#define RELATIONAL_PARSER_GRAMMAR_EPSILONEDGESGRAMMAROPTIMIZER_HPP

#include <fmt/ostream.h>
#include <queue>

#include "CommonTypes.hpp"
#include "grammar/GrammarConverter.hpp"
#include "grammar/Transition.hpp"
#include "util/Container.hpp"
#include "util/Printing.hpp"

namespace relparser {

namespace detail {
template<typename T>
struct FindUnion {
  Map<T, T> parents;
  Map<T, int> rank;

  T find(T const& p) {
    if (auto it = parents.find(p); it == parents.end()) {
      parents.emplace_hint(it, p, p);
      rank.emplace(p, 1);
      return p;
    }
    if (p != parents[p])
      parents[p] = find(parents[p]);
    return parents[p];
  }

  T makeUnion(T const& a, T const& b) {
    T fa = find(a);
    T fb = find(b);
    if (fa == fb)
      return fa;
    if (rank[fa] > rank[fb])
      std::swap(fa, fb);
    parents[fa] = fb;
    rank[fb] += rank[fa];
    return fb;
  }
};

}  // namespace detail

template<typename TGrammar>
struct EpsilonEdgesGrammarOptimizer {
  using TState = typename TGrammar::StateType;
  using TTerminal = typename TGrammar::TerminalType;

  TGrammar operator()(TGrammar const& grammar) const {
    auto partitioning = optimizeEpsEdges(grammar);
    return remapGrammarStates(grammar,
                              [&](TState s) { return TState{partitioning.partitionId(s)}; });
  }

 private:
  StatesPartitioning<TState> optimizeEpsEdges(TGrammar const& grammar) const {
    Map<TState, int> ingoingCount;
    Map<TState, int> outgoingCount;
    Map<TState, Set<TState>> epsGraph;

    for (Shift<TState, TTerminal> const& shift : grammar.shiftTransitions) {
      ingoingCount[shift.target]++;
      outgoingCount[shift.source]++;
    }

    for (Call<TState> const& call : grammar.callTransitions) {
      ingoingCount[call.target]++;
      ingoingCount[call.child]++;
      outgoingCount[call.source]++;
      if (call.child == grammar.epsilon) {
        epsGraph[call.source].emplace(call.target);
      }
    }

    for (Reduce<TState> const& reduce : grammar.reduceTransitions) {
      outgoingCount[reduce.source]++;
    }

    Set<TState> visited;
    detail::FindUnion<TState> mergeGroups;

    std::function<void(TState)> dfs = [&](TState source) {
      if (!visited.emplace(source).second)
        return;

      if (auto const targets = epsGraph.find(source); targets != epsGraph.cend()) {
        for (TState target : targets->second) {
          dfs(target);
          auto sourceGroup = mergeGroups.find(source);
          auto targetGroup = mergeGroups.find(target);
          if (sourceGroup != targetGroup
              && (outgoingCount[sourceGroup] <= 1 || ingoingCount[targetGroup] <= 1)) {
            auto newGroup = mergeGroups.makeUnion(sourceGroup, targetGroup);
            auto otherGroup = newGroup == sourceGroup ? targetGroup : sourceGroup;
            outgoingCount[newGroup] += outgoingCount[otherGroup] - 1;
            ingoingCount[newGroup] += ingoingCount[otherGroup] - 1;
          }
        }
      }
    };

    for (TState source : grammar.allStates()) {
      dfs(source);
    }

    Map<TState, TState> stateGroups;
    for (TState source : grammar.allStates()) {
      stateGroups.emplace(source, mergeGroups.find(source));
    }

    return StatesPartitioning<TState>{
        std::move(stdutil::values(stdutil::inverseMapping(stateGroups)))};
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_EPSILONEDGESGRAMMAROPTIMIZER_HPP
