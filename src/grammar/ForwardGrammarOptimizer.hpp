#ifndef RELATIONAL_PARSER_GRAMMAR_FORWARDGRAMMAROPTIMIZER_HPP
#define RELATIONAL_PARSER_GRAMMAR_FORWARDGRAMMAROPTIMIZER_HPP

#include "CommonTypes.hpp"
#include "grammar/StatesPartitioning.hpp"
#include "grammar/Transition.hpp"
#include "util/Container.hpp"

namespace relparser::detail {

// Forward equivalence of states specification: two states are equivalent if all their forward paths
// are exactly the same. States with the same spec are equivalent and will end up in the same
// partition.
template<typename TState, typename TTerminal>
struct ForwardEquivalenceSpec {
  using PartitionId = typename StatesPartitioning<TState>::PartitionId;

  // Call transitions (outgoing non-terminal edges).
  OrdSet<std::pair<PartitionId, PartitionId>> callTargets;
  // Shift transitions (outgoing terminal edges).
  OrdSet<std::pair<TTerminal, PartitionId>> shiftTargets;
  // If there is a reduce transition from this state.
  bool isFinalState{false};
  // If the state is a labeled nonterminal in the original grammar.
  std::optional<TState> ruleStart{std::nullopt};

  bool operator==(ForwardEquivalenceSpec const& rhs) const {
    return callTargets == rhs.callTargets && shiftTargets == rhs.shiftTargets
           && isFinalState == rhs.isFinalState && ruleStart == rhs.ruleStart;
  }

  bool operator!=(ForwardEquivalenceSpec const& rhs) const {
    return !(rhs == *this);
  }

#ifdef DETERMINISTIC_GRAMMAR
  bool operator<(ForwardEquivalenceSpec const& rhs) const {
    return std::tie(callTargets, shiftTargets, isFinalState, ruleStart)
           < std::tie(rhs.callTargets, rhs.shiftTargets, rhs.isFinalState, rhs.ruleStart);
  }
#endif

  template<typename H>
  friend H AbslHashValue(H h, ForwardEquivalenceSpec const& spec) {
    return H::combine(std::move(h), spec.callTargets, spec.shiftTargets, spec.isFinalState,
                      spec.ruleStart);
  }
};

}  // namespace relparser::detail

namespace relparser {

template<typename TGrammar>
struct ForwardGrammarOptimizer {
  using TState = typename TGrammar::StateType;
  using TTerminal = typename TGrammar::TerminalType;

  TGrammar operator()(TGrammar const& grammar) const {
    auto partitioning =
        StatesPartitioning<TState>{stdutil::convert<std::vector<TState>>(grammar.allStates())};

    while (true) {
      auto refinedPartitioning = refinePartitioning(grammar, partitioning);
      if (partitioning == refinedPartitioning) {
        return remapGrammarStates(grammar,
                                  [&](TState s) { return TState{partitioning.partitionId(s)}; });
      }
      partitioning = std::move(refinedPartitioning);
    }
  }

 private:
  StatesPartitioning<TState> refinePartitioning(
      TGrammar const& grammar, StatesPartitioning<TState> const& partitioning) const {
    Map<TState, detail::ForwardEquivalenceSpec<TState, TTerminal>> statesSpec;

    statesSpec[grammar.start].ruleStart = grammar.start;
    statesSpec[grammar.stop];

    for (Shift<TState, TTerminal> const& shift : grammar.shiftTransitions) {
      statesSpec[shift.source].shiftTargets.emplace(shift.symbol,
                                                    partitioning.partitionId(shift.target));
      statesSpec[shift.target];
    }

    for (Call<TState> const& call : grammar.callTransitions) {
      statesSpec[call.source].callTargets.emplace(partitioning.partitionId(call.child),
                                                  partitioning.partitionId(call.target));
      statesSpec[call.child].ruleStart = call.child;
      statesSpec[call.target];
    }

    for (Reduce<TState> const& reduce : grammar.reduceTransitions) {
      statesSpec[reduce.source].isFinalState = true;
    }

    return StatesPartitioning<TState>{stdutil::values(stdutil::inverseMapping(statesSpec))};
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_FORWARDGRAMMAROPTIMIZER_HPP
