#ifndef RELATIONAL_PARSER_PARSER_SYMBOLMAPPING_HPP
#define RELATIONAL_PARSER_PARSER_SYMBOLMAPPING_HPP

#include <fmt/ostream.h>
#include <variant>

#include "CommonTypes.hpp"

namespace relparser {

template<typename TRtnGrammar, typename TSymbol>
class SymbolMapping {
 public:
  using TerminalType = typename TRtnGrammar::TerminalType;
  using StateType = typename TRtnGrammar::StateType;
  using GrammarType = TRtnGrammar;
  using SymbolType = TSymbol;

  explicit SymbolMapping(TRtnGrammar const& grammar) : rtnGrammar_{grammar} {
    auto const terminals = rtnGrammar_.terminals();
    auto symbolId = static_cast<SymbolType>(*std::ranges::max_element(terminals));
    for (auto state : rtnGrammar_.allStates()) {
      stateMapping_.emplace(state, ++symbolId);
    }
  }

  TRtnGrammar const& grammar() const {
    return rtnGrammar_;
  }

  std::string stateLabel(StateType s) const {
    return rtnGrammar_.stateLabel(s);
  }

  std::string terminalLabel(TerminalType t) const {
    return rtnGrammar_.terminalLabel(t);
  }

  SymbolType state(StateType s) const {
    return stateMapping_.at(s);
  }

  SymbolType terminal(TerminalType t) const {
    return t;
  }

 private:
  TRtnGrammar const& rtnGrammar_;
  Map<StateType, SymbolType> stateMapping_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_SYMBOLMAPPING_HPP
