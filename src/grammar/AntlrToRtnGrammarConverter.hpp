#ifndef RELATIONAL_PARSER_CLI_ANTLRTORELATIONALPARSERCONVERTER_HPP
#define RELATIONAL_PARSER_CLI_ANTLRTORELATIONALPARSERCONVERTER_HPP

#include "antlr/AntlrFwd.hpp"
#include "grammar/Types.hpp"

namespace relparser {

TRtnGrammar convertAntlrParser(antlr4::Parser const& parser);

}  // namespace relparser

#endif  // RELATIONAL_PARSER_CLI_ANTLRTORELATIONALPARSERCONVERTER_HPP
