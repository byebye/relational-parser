#ifndef RELATIONAL_PARSER_GRAMMAR_GRAMMAR_HPP
#define RELATIONAL_PARSER_GRAMMAR_GRAMMAR_HPP

#include <fmt/ostream.h>
#include <ostream>

#include "CommonTypes.hpp"
#include "util/Container.hpp"

namespace relparser {

template<typename TSymbol>
class Grammar {
 public:
  using SymbolType = TSymbol;

  SymbolType start;
  Map<SymbolType, std::vector<std::pair<int, std::vector<TSymbol>>>> derivationRules;

  void addDerivation(SymbolType lhs, std::vector<TSymbol> rhs) {
    derivationRules[lhs].emplace_back(rulesCount_++, std::move(rhs));
  }

  void setSymbolLabel(SymbolType state, std::string const& label) {
    if (!label.empty()) {
      symbolLabels_.emplace(state, label);
    }
  }

  std::string const& symbolLabel(SymbolType state) const {
    static std::string const emptyLabel_;
    return stdutil::getOr(symbolLabels_, state, emptyLabel_);
  }

  friend std::ostream& operator<<(std::ostream& os, Grammar const& grammar) {
    fmt::print(os, "Grammar(\n");
    for (auto&& [lhs, rhss] : grammar.derivationRules) {
      fmt::print(os, "  {}<{}> -> ", lhs, grammar.symbolLabel(lhs));
      int i = 0;
      for (auto&& [ruleId, rhs] : rhss) {
        if (rhs.empty()) {
          fmt::print(os, "''");
        }
        for (auto s : rhs) {
          fmt::print(os, "{}<{}>", s, grammar.symbolLabel(s));
        }
        fmt::print(os, " [{}]", ruleId);
        if (++i < rhss.size()) {
          fmt::print(os, " | ");
        }
      }
      fmt::print(os, "\n");
    }
    fmt::print(os, ")");
    return os;
  }

  int rulesCount() const {
    return rulesCount_;
  }

 private:
  HashMap<SymbolType, std::string> symbolLabels_;
  int rulesCount_{0};
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_GRAMMAR_HPP
