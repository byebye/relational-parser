#ifndef RELATIONAL_PARSER_GRAMMAR_BACKWARDGRAMMAROPTIMIZER_HPP
#define RELATIONAL_PARSER_GRAMMAR_BACKWARDGRAMMAROPTIMIZER_HPP

#include "CommonTypes.hpp"
#include "grammar/StatesPartitioning.hpp"
#include "grammar/Transition.hpp"
#include "util/Container.hpp"

namespace relparser::detail {

// Backward equivalence of states specification: two states are equivalent if they are reachable
// via exactly the same paths. States with the same spec are equivalent and will end up in the same
// partition.
template<typename TState, typename TTerminal>
struct BackwardEquivalenceSpec {
  using PartitionId = typename StatesPartitioning<TState>::PartitionId;

  // Backward shift transitions (ingoing terminal edges).
  OrdSet<std::pair<TTerminal, PartitionId>> shiftPredecessors;
  // Backward call transitions (ingoing non-terminal edges).
  OrdSet<std::pair<PartitionId, PartitionId>> callPredecessors;
  // Call transitions of which the state is the child (is the left-hand side of a production).
  OrdSet<std::pair<PartitionId, PartitionId>> callParents;
  // If the state is a labeled nonterminal in the original grammar.
  std::optional<TState> ruleStart{std::nullopt};

  bool operator==(BackwardEquivalenceSpec const& rhs) const {
    return shiftPredecessors == rhs.shiftPredecessors && callPredecessors == rhs.callPredecessors
           && callParents == rhs.callParents && ruleStart == rhs.ruleStart;
  }

  bool operator!=(BackwardEquivalenceSpec const& rhs) const {
    return !(rhs == *this);
  }

#ifdef DETERMINISTIC_GRAMMAR
  bool operator<(BackwardEquivalenceSpec const& rhs) const {
    return std::tie(shiftPredecessors, callPredecessors, callParents, ruleStart) < std::tie(
               rhs.shiftPredecessors, rhs.callPredecessors, rhs.callParents, rhs.ruleStart);
  }
#endif

  template<typename H>
  friend H AbslHashValue(H h, BackwardEquivalenceSpec const& spec) {
    return H::combine(std::move(h), spec.shiftPredecessors, spec.callPredecessors, spec.callParents,
                      spec.ruleStart);
  }
};

}  // namespace relparser::detail

namespace relparser {

template<typename TGrammar>
struct BackwardGrammarOptimizer {
  using TState = typename TGrammar::StateType;
  using TTerminal = typename TGrammar::TerminalType;

  TGrammar operator()(TGrammar const& grammar) const {
    auto partitioning =
        StatesPartitioning<TState>{stdutil::convert<std::vector<TState>>(grammar.allStates())};

    while (true) {
      auto refinedPartitioning = refinePartitioning(grammar, partitioning);
      if (partitioning == refinedPartitioning) {
        return remapGrammarStates(grammar,
                                  [&](TState s) { return TState{partitioning.partitionId(s)}; });
      }
      partitioning = std::move(refinedPartitioning);
    }
  }

 private:
  StatesPartitioning<TState> refinePartitioning(
      TGrammar const& grammar, StatesPartitioning<TState> const& partitioning) const {
    Map<TState, detail::BackwardEquivalenceSpec<TState, TTerminal>> statesSpec;

    statesSpec[grammar.start].ruleStart = grammar.start;
    statesSpec[grammar.stop];

    for (Shift<TState, TTerminal> const& shift : grammar.shiftTransitions) {
      statesSpec[shift.target].shiftPredecessors.emplace(shift.symbol,
                                                         partitioning.partitionId(shift.source));
      statesSpec[shift.source];
    }

    for (Call<TState> const& call : grammar.callTransitions) {
      statesSpec[call.target].callPredecessors.emplace(partitioning.partitionId(call.source),
                                                       partitioning.partitionId(call.child));
      statesSpec[call.child].callParents.emplace(partitioning.partitionId(call.source),
                                                 partitioning.partitionId(call.target));
      statesSpec[call.child].ruleStart = call.child;
      statesSpec[call.source];
    }

    for (Reduce<TState> const& reduce : grammar.reduceTransitions) {
      statesSpec[reduce.source];
    }

    return StatesPartitioning<TState>{stdutil::values(stdutil::inverseMapping(statesSpec))};
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_BACKWARDGRAMMAROPTIMIZER_HPP
