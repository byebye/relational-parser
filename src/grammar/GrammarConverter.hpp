#ifndef RELATIONAL_PARSER_GRAMMAR_GRAMMARCONVERTER_HPP
#define RELATIONAL_PARSER_GRAMMAR_GRAMMARCONVERTER_HPP

#include "grammar/Transition.hpp"

namespace relparser {

template<typename TRtnGrammar, typename TStateMapping>
static TRtnGrammar remapGrammarStates(TRtnGrammar const& grammar, TStateMapping stateMapping) {
  using TState = typename TRtnGrammar::StateType;
  using TTerminal = typename TRtnGrammar::TerminalType;

  TRtnGrammar mappedGrammar;

  auto mapState = [&](TState oldState) {
    TState newState{stateMapping(oldState)};
    mappedGrammar.setStateLabel(newState, grammar.stateLabel(oldState));
    return newState;
  };

  mappedGrammar.start = mapState(grammar.start);
  mappedGrammar.stop = mapState(grammar.stop);
  mappedGrammar.epsilon = mapState(grammar.epsilon);

  for (Shift<TState, TTerminal> const& shift : grammar.shiftTransitions) {
    TState newSource{mapState(shift.source)};
    TState newTarget{mapState(shift.target)};
    mappedGrammar.addShift(Shift<TState, TTerminal>{newSource, newTarget, shift.symbol});
    mappedGrammar.setTerminalLabel(shift.symbol, grammar.terminalLabel(shift.symbol));
  }

  for (Call<TState> const& call : grammar.callTransitions) {
    TState newSource{mapState(call.source)};
    TState newChild{mapState(call.child)};
    TState newTarget{mapState(call.target)};
    // Don't add epsilon loops.
    if (newChild != mappedGrammar.epsilon || newSource != newTarget) {
      mappedGrammar.addCall(Call<TState>{newSource, newChild, newTarget});
    }
  }

  for (Reduce<TState> const& reduce : grammar.reduceTransitions) {
    TState newSource{mapState(reduce.source)};
    mappedGrammar.addReduce(Reduce<TState>{newSource});
  }

  return mappedGrammar;
}

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_GRAMMARCONVERTER_HPP
