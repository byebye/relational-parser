#ifndef RELATIONAL_PARSER_GRAMMAR_RTNGRAMMAR_HPP
#define RELATIONAL_PARSER_GRAMMAR_RTNGRAMMAR_HPP

#include "CommonTypes.hpp"
#include "grammar/Transition.hpp"
#include "util/Container.hpp"

namespace relparser {

template<typename TState, typename TTerminal>
class RtnGrammar {
 public:
  using TerminalType = TTerminal;
  using StateType = TState;

  StateType start;
  StateType stop;
  StateType epsilon;

  Set<Shift<StateType, TerminalType>> shiftTransitions;
  Set<Call<StateType>> callTransitions;
  Set<Reduce<StateType>> reduceTransitions;

  bool addShift(Shift<StateType, TerminalType> const& shift) {
    return shiftTransitions.emplace(shift).second;
  }

  bool addCall(Call<StateType> const& call) {
    return callTransitions.emplace(call).second;
  }

  bool addReduce(Reduce<StateType> const& reduce) {
    return reduceTransitions.emplace(reduce).second;
  }

  void setStateLabel(StateType state, std::string const& label) {
    if (!label.empty()) {
      stateLabels_.emplace(state, label);
    }
  }

  void setTerminalLabel(TerminalType terminal, std::string const& label) {
    if (!label.empty()) {
      terminalLabels_[terminal] = label;
    }
  }

  std::string const& stateLabel(StateType state) const {
    return stdutil::getOr(stateLabels_, state, emptyLabel_);
  }

  std::string const& terminalLabel(TerminalType terminal) const {
    return stdutil::getOr(terminalLabels_, terminal, emptyLabel_);
  }

  OrdSet<TerminalType> terminals() const {
    OrdSet<TerminalType> terminals;
    for (auto const& shift : shiftTransitions) {
      terminals.emplace(shift.symbol);
    }
    return terminals;
  }

  Set<StateType> allStates() const {
    Set<StateType> allStates{start, stop};
    for (auto&& shift : shiftTransitions) {
      allStates.emplace(shift.source);
      allStates.emplace(shift.target);
    }
    for (auto&& call : callTransitions) {
      allStates.emplace(call.source);
      allStates.emplace(call.child);
      allStates.emplace(call.target);
    }
    for (auto&& reduce : reduceTransitions) {
      allStates.emplace(reduce.source);
    }
    return allStates;
  }

  Set<StateType> startStates() const {
    Set<StateType> result;
    for (auto const& call : callTransitions) {
      result.emplace(call.child);
    }
    return result;
  }

  Set<StateType> finalStates() const {
    Set<StateType> result;
    for (auto const& reduce : reduceTransitions) {
      result.emplace(reduce.source);
    }
    return result;
  }

 private:
  inline static std::string emptyLabel_{};
  HashMap<StateType, std::string> stateLabels_;
  HashMap<TerminalType, std::string> terminalLabels_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_RTNGRAMMAR_HPP
