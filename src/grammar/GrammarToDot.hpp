#ifndef RELATIONAL_PARSER_GRAMMAR_GRAMMARTODOT_HPP
#define RELATIONAL_PARSER_GRAMMAR_GRAMMARTODOT_HPP

#include <fmt/format.h>
#include <fmt/ostream.h>

#include "CommonTypes.hpp"
#include "util/Container.hpp"
#include "util/DotGraph.hpp"

namespace relparser {

template<typename TGrammar>
void grammarToDot(TGrammar const& grammar, std::ostream& out) {
  using StateType = typename TGrammar::StateType;

  auto const allStates = stdutil::sorted(grammar.allStates());
  auto const startStates = stdutil::sorted(grammar.startStates());
  auto const finalStates = stdutil::convert<Set<StateType>>(grammar.finalStates());

  using NodeOptions = DotGraph::NodeOptions;
  using EdgeOptions = DotGraph::EdgeOptions;

  DotGraph dotGraph{out, DotGraph::Options{DotGraph::Directed, DotGraph::LeftToRight}};

  auto startId = [](StateType s) { return fmt::format("b{}", s); };
  auto id = [](StateType s) { return fmt::format("{}", s); };

  auto startStopNodeOpts = NodeOptions{}.shape(DotGraph::DoubleOctagon);
  dotGraph.node(startId(grammar.start), grammar.stateLabel(grammar.start), startStopNodeOpts);
  dotGraph.node(startId(grammar.stop), grammar.stateLabel(grammar.stop), startStopNodeOpts);

  auto labelEdgeOpts = EdgeOptions{}.arrowHead(DotGraph::None).style(DotGraph::Dashed);
  dotGraph.edge(startId(grammar.start), id(grammar.start), "", labelEdgeOpts);
  dotGraph.edge(startId(grammar.stop), id(grammar.stop), "", labelEdgeOpts);

  auto startNodeOpts = NodeOptions{}.shape(DotGraph::Box);
  for (auto&& state : startStates) {
    dotGraph.node(startId(state), grammar.stateLabel(state), startNodeOpts);
    dotGraph.edge(startId(state), id(state), "", labelEdgeOpts);
  }

  // Rank start nodes top to bottom to put them first in each subgraph.
  if (!startStates.empty()) {
    auto rankOpts =
        EdgeOptions{}.arrowHead(DotGraph::None).style(DotGraph::Invisible).constraint(false);
    auto it = startStates.cbegin();
    while (std::next(it) != startStates.cend()) {
      dotGraph.edge(id(*it), id(*std::next(it)), "", rankOpts);
      ++it;
    }
  }

  auto nodeOpts = NodeOptions{}.fixedSize();
  for (auto&& state : allStates) {
    nodeOpts.shape_ = finalStates.contains(state) ? DotGraph::DoubleCircle : DotGraph::Circle;
    dotGraph.node(id(state), id(state), nodeOpts);
  }

  auto edgeOpts = EdgeOptions{}.fontName("Courier");
  for (auto&& shift : grammar.shiftTransitions) {
    dotGraph.edge(id(shift.source), id(shift.target), grammar.terminalLabel(shift.symbol),
                  edgeOpts);
  }
  for (auto&& call : grammar.callTransitions) {
    dotGraph.edge(id(call.source), id(call.target),
                  fmt::format("<{}>", grammar.stateLabel(call.child)), edgeOpts);
  }
}

}  // namespace relparser

#endif  // RELATIONAL_PARSER_GRAMMAR_GRAMMARTODOT_HPP
