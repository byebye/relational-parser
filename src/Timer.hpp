#ifndef RELATIONAL_PARSER_TIMER_HPP
#define RELATIONAL_PARSER_TIMER_HPP

#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <chrono>
#include <ostream>

#include "CommonTypes.hpp"
#include "util/Finally.hpp"

namespace relparser {

class Timer {
 public:
  explicit Timer() {
    restart();
  }

  void restart() {
    start_ = std::chrono::steady_clock::now();
    time_ = time_.zero();
  }

  Timer& stop() {
    time_ = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now()
                                                                 - start_);
    return *this;
  }

  std::chrono::nanoseconds ns() const {
    return time_;
  }

  std::chrono::milliseconds ms() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_);
  }

  friend std::ostream& operator<<(std::ostream& os, Timer const& timer) {
    if (timer.ms() > std::chrono::nanoseconds::zero())
      fmt::print(os, "{}", timer.ms());
    else
      fmt::print(os, "{}", timer.ns());
    return os;
  }

 private:
  std::chrono::time_point<std::chrono::steady_clock> start_;
  std::chrono::nanoseconds time_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_TIMER_HPP
