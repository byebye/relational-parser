#ifndef RELATIONAL_PARSER_UTIL_PRINTING_HPP
#define RELATIONAL_PARSER_UTIL_PRINTING_HPP

#include <optional>
#include <ostream>
#include <fstream>
#include <utility>
#include <variant>

#include "util/Concepts.hpp"

namespace stdutil {

template<typename T>
concept CharSequence = std::same_as<std::decay_t<T>, char const*> || std::same_as<std::decay_t<T>, char*>;

template<typename T>
concept OstreamPrintable = (!Dereferencable<T> || CharSequence<T>) && requires(T t, std::ostream& out) {
  out << t;
};

static_assert(OstreamPrintable<std::string>);
static_assert(OstreamPrintable<int>);
static_assert(OstreamPrintable<char const*>);
static_assert(OstreamPrintable<char*>);
static_assert(OstreamPrintable<char[]>);
static_assert(!OstreamPrintable<int*>);

template<class T>
struct Printable {
  T const& val;
};

template<Iterable C>
requires (!OstreamPrintable<C>)
struct Printable<C> {
  C const& val;
  std::string_view separator = ", ";
  std::string_view prefix = "[";
  std::string_view suffix = "]";
};

template<class T>
Printable(T v) -> Printable<T>;

template<Iterable C>
Printable(C v, std::string_view, std::string_view, std::string_view) -> Printable<C>;

template<OstreamPrintable T>
std::ostream& operator<<(std::ostream& out, Printable<T> p) {
  if constexpr (std::is_same_v<bool, T>) {
    if (!(out.flags() & std::ios_base::boolalpha))
      return out << std::boolalpha << p.val << std::noboolalpha;
  }
  return out << p.val;
}

template<Iterable C>
requires (!OstreamPrintable<C>)
std::ostream& operator<<(std::ostream& out, Printable<C> p) {
  out << p.prefix;
  if (!p.val.empty()) {
    auto it = std::begin(p.val);
    out << Printable{*it};
    for (++it; it != std::end(p.val); ++it)
      out << p.separator << Printable{*it};
  }
  return out << p.suffix;
}

template<class... Ts>
std::ostream& operator<<(std::ostream& out, Printable<std::variant<Ts...>> p) {
  std::visit([&out](const auto& v) { out << Printable{v}; }, p.val);
  return out;
}

template<class T>
std::ostream& operator<<(std::ostream& out, Printable<std::optional<T>> p) {
  if (p.val) {
    out << Printable{*p.val};
  }
  else {
    out << "nullopt";
  }
  return out;
}

template<Dereferencable T>
requires (!OstreamPrintable<T>)
std::ostream& operator<<(std::ostream& out, Printable<T> p) {
  if (p.val) {
    out << "(" << p.val << ") " << Printable{*p.val};
  }
  else {
    out << "(nullptr)";
  }
  return out;
}

template<typename T, typename U>
std::ostream& operator<<(std::ostream& out, Printable<std::pair<T, U>> p) {
  return out << "{" << Printable{p.val.first} << ", " << Printable{p.val.second} << "}";
}

inline std::fstream openFileOrThrow(std::string const& path, std::ios_base::openmode openmode) {
  std::fstream fileStream(path, openmode);
  if (!fileStream.is_open()) {
    throw std::invalid_argument(fmt::format("Error opening file {}.\n", path));
  }
  return fileStream;
}

inline void debug(std::string const& filename, auto&& outputFunc) {
  std::fstream out = openFileOrThrow(filename, std::ios_base::out);
  outputFunc(out);
}

}  // namespace stdutil

#endif  // RELATIONAL_PARSER_UTIL_PRINTING_HPP
