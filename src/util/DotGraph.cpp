#include "util/DotGraph.hpp"

#include <fmt/ostream.h>
#include <ostream>
#include <string_view>

namespace relparser {

std::string escapeHtml(std::string_view text) {
  std::string escaped;
  for (std::size_t i = 0; i < text.size(); ++i) {
    switch (text[i]) {
      case '&': {
        std::size_t htmlSeqEnd = [i, &text]() {
          for (auto j = i + 1; j < text.size(); ++j) {
            if (text[j] == ';')
              return j;
            if (!std::isalpha(text[j]))
              break;
          }
          return i;
        }();
        if (htmlSeqEnd == i)
          escaped += "&amp;";
        else {
          escaped += text.substr(i, htmlSeqEnd - i + 1);
          i = htmlSeqEnd;
        }
        break;
      }
      case '\"': escaped += "&quot;"; break;
      case '\'': escaped += "&apos;"; break;
      case '<': escaped += "&lt;"; break;
      case '>': escaped += "&gt;"; break;
      default: escaped += text[i]; break;
    }
  }
  return escaped;
}

std::string escapeQuotes(std::string_view text) {
  std::string escaped;
  for (std::size_t i = 0; i < text.size(); ++i) {
    switch (text[i]) {
      case '\"':
      case '\'': escaped += '\\'; [[fallthrough]];
      default: escaped += text[i]; break;
    }
  }
  return escaped;
}

HashMap<DotGraph::Kind, const char*> const DotGraph::kinds{
    {DotGraph::Directed, "digraph"},
    {DotGraph::Undirected, "graph"},
};

HashMap<DotGraph::LayoutDirection, const char*> const DotGraph::layouts{
    {DotGraph::TopToBottom, "TB"},
    {DotGraph::LeftToRight, "LR"},
};

HashMap<DotGraph::NodeShape, const char*> const DotGraph::nodeShapes{
    {DotGraph::Box, "box"},
    {DotGraph::Circle, "circle"},
    {DotGraph::DoubleCircle, "doublecircle"},
    {DotGraph::Oval, "oval"},
    {DotGraph::Octagon, "octagon"},
    {DotGraph::DoubleOctagon, "doubleoctagon"},
    {DotGraph::Diamond, "diamond"},
};

HashMap<DotGraph::Style, const char*> const DotGraph::styles{
    {DotGraph::Filled, "filled"},       {DotGraph::Invisible, "invisible"},
    {DotGraph::Diagonals, "diagonals"}, {DotGraph::Rounded, "rounded"},
    {DotGraph::Dashed, "dashed"},       {DotGraph::Dotted, "dotted"},
    {DotGraph::Solid, "solid"},         {DotGraph::Bold, "bold"},
};

HashMap<DotGraph::ArrowHead, const char*> const DotGraph::arrowHeads{
    {DotGraph::None, "none"},
    {DotGraph::Normal, "normal"},
    {DotGraph::Vee, "vee"},
    {DotGraph::Inversed, "inv"},
    {DotGraph::DiamondHead, "diamond"}};

std::ostream& operator<<(std::ostream& os, DotGraph::Label const& label) {
  if (label.type_ == DotGraph::Label::Text_)
    fmt::print(os, "\"{}\"", escapeQuotes(label.text_));
  else
    fmt::print(os, "<{}>", label.text_);
  return os;
}

std::ostream& operator<<(std::ostream& os, DotGraph::NodeOptions const& options) {
  if (!options.fontName_.empty())
    fmt::print(os, "fontname=\"{}\", ", options.fontName_);
  if (options.fixedSize_)
    fmt::print(os, "fixedsize=true, ");
  if (options.style_ != DotGraph::Solid)
    fmt::print(os, "style={}, ", DotGraph::styles.at(options.style_));
  fmt::print(os, "fontsize={}, shape={}, width={:.2f}", options.fontSize_,
             DotGraph::nodeShapes.at(options.shape_), options.width_);
  return os;
}

std::ostream& operator<<(std::ostream& os, DotGraph::EdgeOptions const& options) {
  if (!options.fontName_.empty())
    fmt::print(os, "fontname=\"{}\", ", options.fontName_);
  if (!options.constraint_)
    fmt::print(os, "constraint=false, ");
  if (options.style_ != DotGraph::Solid)
    fmt::print(os, "style={}, ", DotGraph::styles.at(options.style_));
  if (options.arrowHead_ != DotGraph::Normal)
    fmt::print(os, "arrowhead={}{}, ", (options.arrowHeadFilled_ ? "" : "o"),
               DotGraph::arrowHeads.at(options.arrowHead_));
  if (options.arrowSize_ != 1.0)
    fmt::print(os, "arrowsize={:.2f}, ", options.arrowSize_);
  fmt::print(os, "fontsize={}", options.fontSize_);
  return os;
}

}  // namespace relparser
