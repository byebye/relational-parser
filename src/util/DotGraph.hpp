#ifndef RELATIONAL_PARSER_DOT_DOTGRAPH_HPP
#define RELATIONAL_PARSER_DOT_DOTGRAPH_HPP

#include <fmt/ostream.h>
#include <ostream>
#include <string_view>

#include "CommonTypes.hpp"

namespace relparser {

std::string escapeHtml(std::string_view text);

std::string escapeQuotes(std::string_view text);

class DotGraph {
 public:
  enum Kind { Directed, Undirected };

  enum LayoutDirection { TopToBottom, LeftToRight };

  struct Options {
    Kind kind{Undirected};
    LayoutDirection layout{TopToBottom};
    bool strict{false};
  };

  enum NodeShape { Box, Circle, DoubleCircle, Oval, Octagon, DoubleOctagon, Diamond };

  enum Style { Filled, Invisible, Diagonals, Rounded, Dashed, Dotted, Solid, Bold };

  enum ArrowHead { None, Normal, Vee, Inversed, DiamondHead };

  static HashMap<Kind, const char*> const kinds;
  static HashMap<LayoutDirection, const char*> const layouts;
  static HashMap<NodeShape, const char*> const nodeShapes;
  static HashMap<Style, const char*> const styles;
  static HashMap<ArrowHead, const char*> const arrowHeads;

  struct Label {
    struct Text {
      explicit Text(std::string_view text = "") : text_{text} {
      }

      Text& cat(std::string_view text) {
        text_ += text;
        return *this;
      }

      Text& cat(Text const& text) {
        text_ += text.text_;
        return *this;
      }

      Label label() const& {
        return Label{text_, Text_};
      }

      Label label() && {
        return Label{std::move(text_), Text_};
      }

     private:
      std::string text_;
    };

    struct Html {
      explicit Html(std::string_view text = "") : text_{text} {
      }

      Html& cat(std::string_view text) {
        text_ += escapeHtml(text);
        return *this;
      }

      Html& cat(Html const& text) {
        text_ += text.text_;
        return *this;
      }

      Html& tag(std::string_view tag, std::string_view text) {
        text_ += fmt::format("<{0}>{1}</{0}>", tag, escapeHtml(text));
        return *this;
      }

      Label label() const& {
        return Label{text_, Html_};
      }

      Label label() && {
        return Label{std::move(text_), Html_};
      }

     private:
      std::string text_;
    };

    friend std::ostream& operator<<(std::ostream& os, Label const& label);

   private:
    enum Type { Text_, Html_ };

    std::string text_;
    Type type_;

    Label(std::string_view text, Type type) : text_{text}, type_{type} {
    }
  };

  struct NodeOptions {
    int fontSize_{11};
    std::string fontName_{};
    NodeShape shape_{Circle};
    Style style_{Solid};
    bool fixedSize_{false};
    float width_{0.75};

    NodeOptions& fontSize(int fs) {
      fontSize_ = fs;
      return *this;
    }

    NodeOptions& fontName(std::string_view fn) {
      fontName_ = fn;
      return *this;
    }

    NodeOptions& shape(NodeShape ns) {
      shape_ = ns;
      return *this;
    }

    NodeOptions& style(Style s) {
      style_ = s;
      return *this;
    }

    NodeOptions& fixedSize(bool fs = true) {
      fixedSize_ = fs;
      return *this;
    }

    NodeOptions& width(float w) {
      width_ = w;
      return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, NodeOptions const& options);
  };

  struct EdgeOptions {
    int fontSize_{11};
    std::string fontName_{};
    Style style_{Solid};
    ArrowHead arrowHead_{Normal};
    bool arrowHeadFilled_{true};
    float arrowSize_{1.0};
    bool constraint_{true};

    EdgeOptions& fontSize(int fs) {
      fontSize_ = fs;
      return *this;
    }

    EdgeOptions& fontName(std::string_view fn) {
      fontName_ = fn;
      return *this;
    }

    EdgeOptions& style(Style s) {
      style_ = s;
      return *this;
    }

    EdgeOptions& arrowHead(ArrowHead ah, bool filled = true) {
      arrowHead_ = ah;
      arrowHeadFilled_ = filled;
      return *this;
    }

    EdgeOptions& arrowHeadFilled(bool filled = true) {
      arrowHeadFilled_ = filled;
      return *this;
    }

    EdgeOptions& arrowSize(float as) {
      arrowSize_ = as;
      return *this;
    }

    EdgeOptions& constraint(bool c = true) {
      constraint_ = c;
      return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, EdgeOptions const& options);
  };

  DotGraph(std::ostream& out, Options const& options) : out_{out} {
    if (options.strict) {
      fmt::print(out_, "strict ");
    }
    fmt::print(out_, "{} {{\n", kinds.at(options.kind));
    newLayout(options.layout);
  }

  ~DotGraph() {
    fmt::print(out_, "}}\n");
  }

  void newLayout(LayoutDirection layout) const {
    fmt::print(out_, "rankdir={};\n", layouts.at(layout));
  }

  void node(std::string_view nodeId, std::string_view label, NodeOptions const& options) const {
    assert(!nodeId.empty());
    node(nodeId, Label::Text{label}.label(), options);
  }

  void edge(std::string_view sourceNodeId, std::string_view targetNodeId, std::string_view label,
            EdgeOptions const& options) const {
    assert(!sourceNodeId.empty() && !targetNodeId.empty());
    edge(sourceNodeId, targetNodeId, Label::Text{label}.label(), options);
  }

  void node(std::string_view nodeId, Label const& label, NodeOptions const& options) const {
    assert(!nodeId.empty());
    fmt::print(out_, "{} [label={}, {}];\n", nodeId, label, options);
  }

  void edge(std::string_view sourceNodeId, std::string_view targetNodeId, Label const& label,
            EdgeOptions const& options) const {
    assert(!sourceNodeId.empty() && !targetNodeId.empty());
    fmt::print(out_, "{} -> {} [label={}, {}];\n", sourceNodeId, targetNodeId, label, options);
  }

 private:
  std::ostream& out_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_DOT_DOTGRAPH_HPP
