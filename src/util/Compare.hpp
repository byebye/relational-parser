#ifndef RELATIONAL_PARSER_UTIL_HASH_HPP
#define RELATIONAL_PARSER_UTIL_HASH_HPP

#include <absl/container/flat_hash_set.h>
#include <absl/container/node_hash_set.h>
#include <absl/hash/hash.h>

#include "util/Concepts.hpp"

namespace stdutil {

template<typename T>
bool deep_eq(T const& lhs, T const& rhs) {
  if constexpr (Dereferencable<T>) {
    if (lhs == rhs)
      return true;
    if (!lhs || !rhs)
      return false;
    return *lhs == *rhs;
  }
  else if constexpr (Iterable<T>) {
    if (std::size(lhs) != std::size(rhs))
      return false;
    auto ilhs = std::begin(lhs), irhs = std::begin(rhs);
    while (ilhs != std::end(lhs)) {
      if (!deep_eq(*ilhs++, *irhs++))
        return false;
    }
    return true;
  }
  else {
    return lhs == rhs;
  }
}

template<typename T>
int deep_cmp(T const& lhs, T const& rhs) {
  if constexpr (Dereferencable<T>) {
    if (lhs == rhs)
      return 0;
    if (!lhs)
      return -1;
    if (!rhs)
      return 1;
    return deep_cmp(*lhs, *rhs);
  }
  else if constexpr (Iterable<T>) {
    auto ilhs = std::begin(lhs), irhs = std::begin(rhs);
    while (ilhs != std::end(lhs) && irhs != std::end(rhs)) {
      if (int cmp = deep_cmp(*ilhs++, *irhs++); cmp != 0)
        return cmp;
    }
    if (ilhs == std::end(lhs) && irhs == std::end(rhs))
      return 0;
    if (ilhs == std::end(lhs))
      return -1;
    return 1;
  }
  else {
    if (lhs == rhs)
      return 0;
    return lhs < rhs ? -1 : 1;
  }
}

template<typename T>
int cmp(T const& lhs, T const& rhs) {
  if (lhs == rhs)
    return 0;
  return lhs < rhs ? -1 : 1;
}

template<typename TPtr>
struct dereference_hash {
  std::size_t operator()(TPtr const& ptr) const {
    if (!ptr)
      return absl::Hash<std::nullptr_t>{}(nullptr);
    return absl::Hash<decltype(*ptr)>{}(*ptr);
  }
};

template<typename TPtr>
struct dereference_equal {
  bool operator()(TPtr const& lhs, TPtr const& rhs) const {
    return deep_eq(lhs, rhs);
  }
};

template<typename T>
using dereference_flat_hash_set = absl::flat_hash_set<T, dereference_hash<T>, dereference_equal<T>>;

template<typename T>
using dereference_node_hash_set = absl::node_hash_set<T, dereference_hash<T>, dereference_equal<T>>;

}  // namespace stdutil

#endif  // RELATIONAL_PARSER_UTIL_HASH_HPP
