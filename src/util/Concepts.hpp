#ifndef RELATIONAL_PARSER_UTIL_CONCEPTS_HPP
#define RELATIONAL_PARSER_UTIL_CONCEPTS_HPP

#include <concepts>
#include <functional>

template<typename T>
concept Dereferencable = requires(T a) {
  *a;
};

template<typename T, typename U>
concept SameAsNoCvref = std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>;

template<typename R, typename Elem>
concept RangeOf = std::ranges::range<R> && requires(R const& r) {
  { *std::ranges::begin(r) } -> SameAsNoCvref<Elem>;
};

template<typename T>
concept Iterable = requires(T a) {
  std::begin(a);
  std::end(a);
};

template<typename Elem, typename T>
concept IterableOf = Iterable<T> && requires(T a) {
  std::is_same_v<Elem, decltype(std::begin(a))>;
};

template<typename T>
concept RangeInsertable = requires(T a) {
  a.insert(std::begin(a), std::end(a));
};

template<typename T>
concept RangeIterInsertable = requires(T a) {
  a.insert(std::end(a), std::begin(a), std::end(a));
};

template<typename C, typename E>
concept ElementInsertable = requires(C cont, E elem) {
  cont.insert(elem);
};

template<typename C, typename E>
concept ElementIterInsertable = requires(C cont, E elem) {
  cont.insert(std::begin(cont), elem);
};

template<typename T>
concept Reservable = requires(T a) {
  a.reserve(0);
};

template<typename T>
concept EqualityComparable = requires(T a, T b) {
  { a == b } -> std::same_as<bool>;
};

template<typename T>
concept Hashable = requires(T a) {
  EqualityComparable<T>;
  { std::hash<T>{}(a) } -> std::same_as<std::size_t>;
};

#endif  // RELATIONAL_PARSER_UTIL_CONCEPTS_HPP
