#ifndef RELATIONAL_PARSER_UTIL_CONTAINER_HPP
#define RELATIONAL_PARSER_UTIL_CONTAINER_HPP

#include <absl/algorithm/container.h>
#include <vector>

#include "util/Concepts.hpp"

namespace stdutil {

template<typename F>
class recurse {
  F fun_;

 public:
  template<class T>
  explicit recurse(T&& fun) : fun_(std::forward<T>(fun)) {
  }

  template<class... Args>
  decltype(auto) operator()(Args&&... args) {
    return fun_(std::ref(*this), std::forward<Args>(args)...);
  }
};
template <class F> recurse(F) -> recurse<F>;

template<RangeInsertable C1, typename C2>
requires Iterable<C2> || ElementInsertable<C1, C2>
C1& operator+=(C1& lhs, C2 const& rhs) {
  if constexpr (Iterable<C2>) {
    lhs.insert(std::begin(rhs), std::end(rhs));
  }
  else {
    lhs.insert(rhs);
  }
  return lhs;
}

template<RangeIterInsertable C1, typename C2>
requires Iterable<C2> || ElementIterInsertable<C1, C2>
C1& operator+=(C1& lhs, C2 const& rhs) {
  if constexpr (Iterable<C2>) {
    lhs.insert(std::end(lhs), std::begin(rhs), std::end(rhs));
  }
  else {
    lhs.insert(std::end(lhs), rhs);
  }
  return lhs;
}

template<RangeInsertable C1, typename C2>
requires Iterable<C2> || ElementInsertable<C1, C2>
C1 operator+(C1 lhs, C2 const& rhs) {
  return lhs += rhs;
}

template<RangeIterInsertable C1, typename C2>
requires Iterable<C2> || ElementIterInsertable<C1, C2>
C1 operator+(C1 const& lhs, C2 const& rhs) {
  C1 result;
  if constexpr (Reservable<C1> && Iterable<C2>) {
    result.reserve(std::size(lhs) + std::size(rhs));
  }
  result += lhs;
  return result += rhs;
}

// Target type is first allowing source type to be deduced from the argument.
template<typename CTo, typename CFrom>
CTo convert(CFrom const& from) {
  return CTo(std::begin(from), std::end(from));
}

template<typename C, typename T>
bool contains(C const& cont, T&& elem) {
  auto it = std::find(std::begin(cont), std::end(cont), elem);
  return it != std::end(cont);
}

template<typename T>
std::vector<T> reversed(std::vector<T> vals) {
  absl::c_reverse(vals);
  return vals;
}

template<typename T>
std::vector<T> sorted(std::vector<T> vals) {
  absl::c_sort(vals);
  return vals;
}

template<typename C>
std::vector<typename C::value_type> sorted(C const& vals) {
  std::vector result(std::begin(vals), std::end(vals));
  absl::c_sort(result);
  return result;
}

template<template<class, class, class...> class Map, class K, class V, class... Ts>
std::vector<K> keys(Map<K, V, Ts...> const& map) {
  std::vector<K> result;
  result.reserve(map.size());
  for (auto const& element : map) {
    result.emplace_back(element.first);
  }
  return result;
}

template<template<class, class, class...> class Map, class K, class V, class... Ts>
std::vector<V> values(Map<K, V, Ts...> const& map) {
  std::vector<V> result;
  result.reserve(map.size());
  for (auto const& element : map) {
    result.emplace_back(element.second);
  }
  return result;
}

template<template<class, class, class...> class Map, class K, class V, class... Ts>
std::vector<V> values(Map<K, V, Ts...>&& map) {
  std::vector<V> result;
  result.reserve(map.size());
  for (auto& element : map) {
    result.emplace_back(std::move(element.second));
  }
  return result;
}

template<template<class, class, class...> class Map, class K, class V, class... Ts>
const V& getOr(Map<K, V, Ts...> const& map, K const& key, const V& defaultValue) {
  if (auto it = map.find(key); it != map.cend()) {
    return it->second;
  }
  return defaultValue;
}

template<template<class, class, class...> class Map, class K, class V, class... Ts>
Map<V, std::vector<K>> inverseMapping(Map<K, V, Ts...> const& map) {
  Map<V, std::vector<K>> result;
  for (auto&& [k, v] : map) {
    result[v].emplace_back(k);
  }
  return result;
}

}  // namespace stdutil

#endif  // RELATIONAL_PARSER_UTIL_CONTAINER_HPP
