#ifndef RELATIONAL_PARSER_OVERLOADED_HPP
#define RELATIONAL_PARSER_OVERLOADED_HPP

namespace stdutil {

template<class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};

template<class... Ts>
overloaded(Ts...)->overloaded<Ts...>;

} // namespace stdutil

#endif  // RELATIONAL_PARSER_OVERLOADED_HPP
