#include <absl/container/flat_hash_set.h>
#include <absl/strings/str_join.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>
#include <CLI/CLI.hpp>

#include "parser/Parsing.hpp"
#include "util/Container.hpp"

void setupCommand(CLI::App& app) {
  using namespace relparser;

  auto command = std::make_shared<CommandlineFlags>();

  app.add_option("-g,--grammar", command->grammar,
                 "Input grammar file. The file should contain start symbol on first line then one "
                 "production per line in format: [<LABEL>]: <LHS> ->[ <RHS> ...], e.g. A: 1 -> 2 3")
      ->required();

  auto parsingModeOpt = app.add_option("-m,--mode", command->parsingMode,
                                       "Parsing mode. Can be one of: "
                                           + absl::StrJoin(stdutil::keys(parsingModes), ", "))
                            ->required()
                            ->check([&](const std::string& mode) {
                              if (parsingModes.contains(mode))
                                return std::string{};
                              return "Unsupported parsing mode: " + mode;
                            });

  absl::flat_hash_set<std::string> engines;
  for (auto const& [_, modesMap] : parsingModes) {
    auto modes = stdutil::keys(modesMap);
    engines.insert(modes.begin(), modes.end());
  }
  app.add_option("-e,--engine", command->relationEngine,
                 "Type of relation engine to use. Can be one of: " + absl::StrJoin(engines, ", "))
      ->required()
      ->check([engines, command](const std::string& engine) {
        if (parsingModes.at(command->parsingMode).contains(engine))
          return std::string{};
        return fmt::format(
            "Unsupported relation engine: {} for parsing mode: {}. Can be one of: {}", engine,
            command->parsingMode,
            absl::StrJoin(stdutil::keys(parsingModes.at(command->parsingMode)), ", "));
      });

  auto fileOpt = app.add_option("-f,--file", command->inputFiles, "List of input files to parse.");

  auto dirOpt = app.add_option("-d,--directory", command->inputDirectory,
                               "Directory with the input files to parse.")
                    ->excludes(fileOpt);

  app.add_option(
         "-x,--fileregex", command->inputFilesRegex,
         "Regex matching input files names in case the directory is specified. If not provided, "
         "all files will be matched.")
      ->needs(dirOpt);

  app.add_flag("-r,--recursive", command->recursive,
               "Whether the directory should be scanned recursively for input files.")
      ->needs(dirOpt);

  app.add_option("-u,--updateevery", command->statusUpdateEveryTokens,
                 "Number of tokens processed between each status update. If not specified or "
                 "zero, then status won't be reported.");

  app.add_flag("-p,--parserreset", command->resetParserState,
               "Whether to reset parser state after each input file.")
      ->take_last();

  app.add_option("-o,--output", command->resultsFile,
                 "Parsing results output file in text format.");

  app.add_option("-i,--dot", command->dotResultsFile, "Parsing results output file in dot format.");
  app.add_option("--parsetreelimit", command->parseTreesLimit, "Limit of parse trees to output.");

  app.callback(
      [command]() { parsingModes.at(command->parsingMode).at(command->relationEngine)(*command); });
}

int main(int argc, const char* argv[]) {
  CLI::App app{"Relation Parser."};

  int verbosityLevel{1};
  app.add_option("-v,--verbosity", verbosityLevel,
                 "Logging verbosity level: 0=off, 1=info, 2=debug, 3=trace")
      ->default_str("1")
      ->check(CLI::Range(0, 3));

  std::string logFile;
  app.add_option("-l,--logfile", logFile, "Log file location (will be truncated if exists)")
      ->default_val("relparser.log");

  app.parse_complete_callback([&]() {
    auto const level = [verbosityLevel]() {
      switch (verbosityLevel) {
        case 0: return spdlog::level::off;
        case 1: return spdlog::level::info;
        case 2: return spdlog::level::debug;
        case 3: return spdlog::level::trace;
        default: return spdlog::level::info;
      }
    }();
    SPDLOG_INFO("Setting file logging level to {}", spdlog::level::to_string_view(level));

    std::vector<spdlog::sink_ptr> sinks;
    auto stdoutLogger = std::make_shared<spdlog::sinks::stdout_sink_st>();
    stdoutLogger->set_level(spdlog::level::err);
    sinks.push_back(std::move(stdoutLogger));

    auto fileLogger = std::make_shared<spdlog::sinks::basic_file_sink_st>(logFile, true);
    fileLogger->set_level(level);
    sinks.push_back(std::move(fileLogger));

    auto combinedLogger = std::make_shared<spdlog::logger>("combined", begin(sinks), end(sinks));
    spdlog::register_logger(combinedLogger);
    spdlog::set_default_logger(combinedLogger);
    spdlog::set_level(level);
  });

  setupCommand(app);

  try {
    app.parse(argc, argv);
  }
  catch (CLI::ParseError const& e) {
    return app.exit(e);
  }
  catch (std::exception const& e) {
    fmt::print(stderr, "Exception: {}\n", e.what());
    SPDLOG_ERROR("Exception: {}", e.what());
    return 1;
  }
  return 0;
}
