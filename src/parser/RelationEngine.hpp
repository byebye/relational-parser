#ifndef RELATIONAL_PARSER_PARSER_RELATIONENGINE_HPP
#define RELATIONAL_PARSER_PARSER_RELATIONENGINE_HPP

#include <absl/container/node_hash_set.h>
#include <absl/hash/hash.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <algorithm>
#include <memory>
#include <ostream>
#include <queue>
#include <vector>

#include "CommonTypes.hpp"
#include "Statistics.hpp"
#include "parser/Interfaces.hpp"
#include "util/Compare.hpp"
#include "util/Concepts.hpp"
#include "util/Container.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<interface::AtomicEngine AE>
class RelationEngine {
 public:
  using AtomicEngine = AE;
  using Symbol = typename AE::Symbol;
  using LabelEngine = typename AE::LabelEngine;
  using LabelValue = typename LabelEngine::Value;
  using GrammarAnalysis = typename AE::GrammarAnalysis;
  using Atomic = typename AE::Atomic;
  using EpsilonValue = typename LabelEngine::RelationNode;

  class Node {
   public:
    struct Edge {
      Atomic atomic;
      LabelValue label;
      Node const* target;

      bool operator==(Edge const& rhs) const = default;

      // Parallel compare: parallel edges are considered equal i.e. atomic and target are the same.
      // -1 if this edge is less than rhs, 1 if it's greater.
      std::weak_ordering operator<=>(Edge const& rhs) const {
        if (auto cmp = atomic <=> rhs.atomic; cmp != 0)
          return cmp;
        return target <=> rhs.target;
      }

      template<typename H>
      friend H AbslHashValue(H h, Edge const& edge) {
        return H::combine(std::move(h), edge.atomic, edge.label, edge.target);
      }
    };

    using Edges = std::vector<Edge>;

    Node(Edges&& edges, std::optional<LabelValue>&& epsilon, Node const* dominator)
        : edges_{std::move(edges)}, epsilon_{std::move(epsilon)}, dominator_{dominator} {
      std::ranges::sort(edges_);
    }

    void init() const {
      maxRootDistance_ = absl::c_accumulate(edges_, 0ull, [](std::size_t d, Edge const& edge) {
        return std::max(d, 1 + edge.target->maxRootDistance_);
      });
      if (!dominator_) {
        dominator_ = findDominator();
      }

#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_TRACE
      Set<Node const*> visited;
      std::queue<Node const*> nodesToVisit;
      std::size_t edges{0};
      for (nodesToVisit.push(this); !nodesToVisit.empty(); nodesToVisit.pop()) {
        auto node = nodesToVisit.front();
        edges += node->edges().size();
        for (Edge const& edge : node->edges()) {
          if (visited.emplace(edge.target).second) {
            nodesToVisit.push(edge.target);
          }
        }
      }
      STTRACE(COUNTER_VARKEY(fmt::format("RelationsNode::graphSize[nodes: {:>4d}, edges: {:>4d}]",
                                         visited.size(), edges)));
      STTRACE(
          COUNTER_VARKEY(fmt::format("RelationsNode::maxRootDistance[{:>4d}]", maxRootDistance_)));
#endif
    }

    Edges const& edges() const {
      return edges_;
    }

    std::optional<LabelValue> const& epsilon() const {
      return epsilon_;
    }

    bool isFinal() const {
      return epsilon_.has_value();
    }

    Node const* dominator() const {
      return dominator_;
    }

    bool dominatesItself() const {
      return this == dominator_;
    }

    bool operator==(Node const& rhs) const {
      return edges_ == rhs.edges_ && epsilon_ == rhs.epsilon_;
    }

    // Used for a node cache.
    template<typename H>
    friend H AbslHashValue(H h, Node const& node) {
      return H::combine(std::move(h), node.edges_, node.epsilon_);
    }

   private:
    Edges edges_;
    std::optional<LabelValue> epsilon_;
    // Entirely dependent on the remaining fields, don't take part in comparison.
    mutable Node const* dominator_;
    mutable std::size_t maxRootDistance_{0};

    // Finds the dominator of the current node by computing the LCA of all its children. The LCA has
    // to be located in the DAG before the closest final node, otherwise the current node dominates
    // itself.
    Node const* findDominator() const {
      STDEBUG(STATS("RelationEngine::findDominator"));
      if (epsilon_ || edges_.empty()) {
        return this;
      }
      // In each step go "up" from the node farthest away from the root. If at one point there is
      // just a single node in the queue, it means all paths go through it on the way to the root,
      // so the node is the LCA.
      std::priority_queue<std::pair<std::size_t, Node const*>> toProcess;
      HashSet<Node const*> visited;
      auto insertEdges = [&](Node const* node) {
        for (Edge const& edge : node->edges_) {
          if (visited.emplace(edge.target).second) {
            toProcess.emplace(edge.target->maxRootDistance_, edge.target);
          }
        }
      };
      insertEdges(this);
      while (!toProcess.empty()) {
        auto [_, node] = toProcess.top();
        toProcess.pop();
        if (toProcess.empty()) {
          return node;
        }
        if (node->dominatesItself()) {
          break;
        }
        insertEdges(node);
      }
      return this;
    }
  };

  using Relation = Node const*;
  using Edge = typename Node::Edge;
  using Edges = typename Node::Edges;

  explicit RelationEngine(AtomicEngine atomicEngine) : atomicEngine_{std::move(atomicEngine)} {
    empty_ = makeNode(Edges{}, std::nullopt);
    root_ = makeNode(Edges{}, labelEngine().one());
  }

  RelationEngine(RelationEngine const&) = delete;
  RelationEngine(RelationEngine&&) noexcept = default;

  LabelEngine const& labelEngine() const {
    return atomicEngine_.atomicGraph().labelEngine();
  }

  AtomicEngine const& atomicEngine() const {
    return atomicEngine_;
  }

  std::size_t relationsNodesCount() const {
    return nodeCache_.size();
  }

  Relation init() {
    return prepend(atomicEngine().root(), root());
  }

  Relation root() const {
    return root_;
  }

  bool isRoot(Relation source) const {
    return source == root_;
  }

  Relation empty() const {
    return empty_;
  }

  bool isEmpty(Relation source) const {
    return source == empty_;
  }

  bool isFinal(Relation source) const {
    return source->isFinal();
  }

  Relation prepend(Atomic atomic, Relation target) {
    STDEBUG(TIMER("RelationEngine::prepend"));
    STINFO(COUNTER("RelationEngine::prepend"));
    if (isEmpty(target)) {
      return empty();
    }

    auto const& le = labelEngine();

    Edges edges;
    std::optional<LabelValue> epsilon;

    // Note: atomic has at least either epsilon or derivatives, cannot be empty.
    if (atomicEngine_.containsEpsilon(atomic)) {
      edges.reserve(target->edges().size() + 1);

      auto labelPrefix = atomicEngine_.extractEpsilon(atomic);
      for (Edge const& edge : target->edges()) {
        edges.emplace_back(edge.atomic, le.multiply(labelPrefix, edge.label), edge.target);
      }
      if (target->isFinal()) {
        epsilon = le.multiply(std::move(labelPrefix), *target->epsilon());
      }
    }

    // Optimization: atomic without derivatives is equivalent to not having an edge.
    // Note: there's no need to check for a parallel edge because self-loop cannot exist.
    if (atomicEngine_.hasDerivatives(atomic)) {
      edges.emplace_back(atomic, le.one(), target);
    }

    // It's a final node so it dominates itself.
    if (epsilon) {
      return makeNode(std::move(edges), std::move(epsilon));
    }
    // It's not final and there is only one edge to the target so it's the dominator.
    if (edges.size() == 1 && edges[0].target == target) {
      return makeNode(std::move(edges), std::nullopt, target);
    }
    // There are multiple edges (copied from the parent) and the parent has a dominator, then the
    // dominator is shared.
    if (!target->dominatesItself()) {
      return makeNode(std::move(edges), std::nullopt, target->dominator());
    }
    // Edges from parent added and the parent is final, then there isn't any dominator because other
    // edges lead to the root which is also final.
    return makeNode(std::move(edges), std::nullopt);
  }

  Relation add(Relation lhs, Relation rhs) {
    STDEBUG(TIMER("RelationEngine::add"));
    STINFO(COUNTER("RelationEngine::add"));
    if (isEmpty(lhs)) {
      return rhs;
    }
    if (isEmpty(rhs)) {
      return lhs;
    }
    auto const& le = labelEngine();
    auto sumEdges = [&](Edges const& lhs, Edges const& rhs) {
      Edges result;
      result.reserve(lhs.size() + rhs.size());
      auto lhsIt = lhs.begin();
      auto rhsIt = rhs.begin();
      while (lhsIt != lhs.end() && rhsIt != rhs.end()) {
        auto cmp = *lhsIt <=> *rhsIt;
        if (cmp < 0) {
          result.emplace_back(*lhsIt++);
        }
        else if (cmp > 0) {
          result.emplace_back(*rhsIt++);
        }
        else {
          result.emplace_back(lhsIt->atomic, le.add(lhsIt->label, rhsIt->label), lhsIt->target);
          ++lhsIt, ++rhsIt;
        }
      }
      result.insert(result.end(), lhsIt, lhs.end());
      result.insert(result.end(), rhsIt, rhs.end());
      return result;
    };
    auto epsilon = lhs->epsilon();
    if (rhs->isFinal()) {
      epsilon = lhs->isFinal() ? le.add(*lhs->epsilon(), *rhs->epsilon()) : *rhs->epsilon();
    }
    return makeNode(sumEdges(lhs->edges(), rhs->edges()), std::move(epsilon));
  }

  std::vector<std::pair<Symbol, Relation>> allDerivatives(Relation source, Symbol token) {
    STDEBUG(TIMER("RelationEngine::allDerivatives"));
    STINFO(COUNTER("RelationEngine::allDerivatives"));

    auto const& le = labelEngine();
    auto const& ae = atomicEngine_;

    HashMap<Symbol, std::pair<Edges, std::optional<LabelValue>>> edgesBySymbol;

    for (Edge const& edge : source->edges()) {
      for (auto&& [bySymbol, atomic, label] : ae.allDerivatives(edge.atomic)) {
        // This atomic is prepended later in the phase, no sense adding it if it's empty.
        if (ae.isEmpty(bySymbol, token)) {
          continue;
        }
        // If the atomic didn't have neither any derivatives nor epsilon, then it would be
        // completely empty, which is prevented by omitting empty atomic while calculating
        // derivatives in AtomicEngine.
        assert(ae.hasDerivatives(atomic) || ae.containsEpsilon(atomic));

        auto& [edges, epsilon] = edgesBySymbol[bySymbol];

        // Optimization: atomic without derivatives is equivalent to not having an edge.
        auto labelPrefix = le.flatten(le.multiply(edge.label, label));
        if (ae.hasDerivatives(atomic)) {
          edges.emplace_back(atomic, labelPrefix, edge.target);
        }
        if (!ae.containsEpsilon(atomic)) {
          continue;
        }
        labelPrefix = le.multiply(std::move(labelPrefix), ae.extractEpsilon(atomic));

        for (Edge const& parentEdge : edge.target->edges()) {
          edges.emplace_back(parentEdge.atomic, le.multiply(labelPrefix, parentEdge.label),
                             parentEdge.target);
        }

        if (edge.target->isFinal()) {
          auto targetLabel = le.multiply(std::move(labelPrefix), *edge.target->epsilon());
          epsilon = epsilon ? le.add(std::move(*epsilon), std::move(targetLabel))
                            : std::move(targetLabel);
        }
      }
    }

    // Can't use std::unique here, because when labels are equal they are summed, not skipped.
    auto mergeParallelEdges = [&](Edges& edges) {
      if (edges.empty()) {
        return;
      }
      std::ranges::sort(edges);
      auto last = edges.begin();
      auto current = last;
      int num = 0;
      while (++current != edges.end()) {
        if ((*last <=> *current) == 0) {
          ++num;
          last->label = le.add(std::move(last->label), std::move(current->label));
        }
        else if (++last != current) {
          *last = std::move(*current);
        }
      }
      STTRACE(COUNTER_VARKEY(fmt::format("RelationEngine::addParallelEdges[{:>2d}]", num)));
      STTRACE(COUNTER_INCVAR("RelationEngine::addParallelEdges", num));
      edges.erase(++last, edges.end());
    };

    std::vector<std::pair<Symbol, Relation>> derivatives;
    derivatives.reserve(edgesBySymbol.size());

    for (auto& [derivativeSymbol, edges] : edgesBySymbol) {
      SPDLOG_TRACE("Creating derivative relation by symbol: {}", derivativeSymbol);
      mergeParallelEdges(edges.first);
      derivatives.emplace_back(derivativeSymbol,
                               makeNode(std::move(edges.first), std::move(edges.second)));
    }

    return derivatives;
  }

  Relation computePhase(Relation source, Symbol token) {
    STDEBUG(TIMER("RelationEngine::computePhase"));
    Relation result = empty();
    for (auto&& [symbol, derivative] : allDerivatives(source, token)) {
      Atomic atomic = atomicEngine_.atomic(symbol, token);
      result = add(result, prepend(atomic, derivative));
    }
    return result;
  }

  // The value representing a complete parsing of the whole input - in other words it leaves only
  // the epsilon (nothing) to parse.
  std::optional<EpsilonValue> epsilon(Relation source) const {
    STDEBUG(TIMER("RelationEngine::epsilon"));
    if (!source->isFinal()) {
      return std::nullopt;
    }
    return labelEngine().evaluate(labelEngine().flatten(*source->epsilon()));
  }

  void reset() {
    SPDLOG_TRACE("RelationEngine::reset: number of relations nodes: {}", nodeCache_.size());
    nodeCache_.clear();
  }

 private:
  AtomicEngine atomicEngine_;
  Relation empty_;
  Relation root_;

  // Use node_hash_set for pointer stability.
  absl::node_hash_set<Node> nodeCache_;

 protected:
  Relation makeNode(Edges&& edges, std::optional<LabelValue> epsilon,
                    Relation dominator = nullptr) {
    auto&& [it, inserted] = nodeCache_.emplace(std::move(edges), std::move(epsilon), dominator);
    if (!inserted) {
      STINFO(COUNTER("RelationEngine::makeNode - cached"));
    }
    else {
      STINFO(COUNTER("RelationEngine::makeNode"));
      it->init();
    }
    return &*it;
  }
};

template<interface::AtomicEngine AE>
class CacheableRelationEngine : public RelationEngine<AE> {
  using Base = RelationEngine<AE>;

 public:
  using Relation = typename Base::Relation;
  using AtomicEngine = typename Base::AtomicEngine;
  using Symbol = typename Base::Symbol;
  using LabelEngine = typename Base::LabelEngine;
  using LabelValue = typename Base::LabelValue;
  using GrammarAnalysis = typename Base::GrammarAnalysis;
  using Atomic = typename Base::Atomic;
  using EpsilonValue = typename Base::EpsilonValue;
  using Edge = typename Base::Edge;
  using Edges = typename Base::Edges;

  using Base::Base;

  Relation computePhase(Relation source, Symbol token) {
    STDEBUG(TIMER("CacheableRelationEngine::computePhase"));
    STINFO(COUNTER("CacheableRelationEngine::computePhase"));
    auto it = phaseCache_.find({source, token});
    if (it != phaseCache_.end()) {
      STINFO(COUNTER("CacheableRelationEngine::computePhase - memoized"));
      return it->second;
    }
    Relation result = Base::computePhase(source, token);
    phaseCache_.emplace_hint(it, std::pair{source, token}, result);
    return result;
  }

  void reset() {
    Base::reset();
    phaseCache_.clear();
  }

 private:
  HashMap<std::pair<Relation, Symbol>, Relation> phaseCache_;
};

template<interface::AtomicEngine AE>
class FactorizableRelationEngine : public RelationEngine<AE> {
  using Base = RelationEngine<AE>;

 public:
  using Relation = typename Base::Relation;
  using AtomicEngine = typename Base::AtomicEngine;
  using Symbol = typename Base::Symbol;
  using LabelEngine = typename Base::LabelEngine;
  using LabelValue = typename Base::LabelValue;
  using GrammarAnalysis = typename Base::GrammarAnalysis;
  using Atomic = typename Base::Atomic;
  using EpsilonValue = typename Base::EpsilonValue;
  using Edge = typename Base::Edge;
  using Edges = typename Base::Edges;

  using Base::Base;

  // Replace root of lhs with rhs, effectively appending rhs to each lhs node. All original
  // non-epsilon edges are preserved. Epsilon edges are only preserved if the rhs is final. The only
  // possible new edges are these copied from rhs and only in nodes that had an epsilon path to
  // the root, which is equivalent to them being final.
  Relation concat(Relation lhs, Relation rhs) {
    STDEBUG(TIMER("FactorizableRelationEngine::concat"));
    STINFO(COUNTER("FactorizableRelationEngine::concat"));
    if (Base::isEmpty(lhs) || Base::isRoot(rhs)) {
      return lhs;
    }
    if (Base::isEmpty(rhs) || Base::isRoot(lhs)) {
      return rhs;
    }
    if (auto it = concatCache_.find({lhs, rhs}); it != concatCache_.end()) {
      STINFO(COUNTER("FactorizableRelationEngine::concat - memoized"));
      return it->second;
    }
    STINFO(COUNTER("FactorizableRelationEngine::concat - compute"));

    Edges edges;
    edges.reserve(lhs->edges().size() + (lhs->isFinal() ? rhs->edges().size() : 0));
    std::ranges::for_each(lhs->edges(), [&](Edge const& edge) {
      edges.emplace_back(edge.atomic, edge.label, concat(edge.target, rhs));
    });

    std::optional<LabelValue> epsilon;
    if (lhs->isFinal()) {
      auto const& le = Base::labelEngine();
      std::ranges::for_each(rhs->edges(), [&](Edge const& edge) {
        edges.emplace_back(edge.atomic, le.multiply(*lhs->epsilon(), edge.label), edge.target);
      });
      if (rhs->isFinal()) {
        epsilon = le.multiply(*lhs->epsilon(), *rhs->epsilon());
      }
    }

    // It is final and dominates itself or previously final root is replaced with a non final
    // node, so final nodes in the lhs are no longer final and dominator must be found again.
    // Otherwise, all final nodes are preserved and the new dominator is simply mapped old
    // dominator.
    auto dominator =
        epsilon.has_value() || !rhs->isFinal() ? nullptr : concatCache_.at({lhs->dominator(), rhs});
    Relation result = Base::makeNode(std::move(edges), std::move(epsilon), dominator);
    return concatCache_.emplace(std::pair{lhs, rhs}, result).first->second;
  }

  std::vector<Relation> factorize(Relation source) {
    STDEBUG(TIMER("FactorizableRelationEngine::factorize"));
    STINFO(COUNTER("FactorizableRelationEngine::factorize"));

    if (source->dominatesItself()) {
      return {source};
    }
    auto result = factorize(source->dominator());

    HashMap<Relation, Relation> mapping{{source->dominator(), Base::root()}};
    auto extractFactor = stdutil::recurse{[&](auto extractFactor, Relation node) -> Relation {
      STDEBUG(COUNTER("FactorizableRelationEngine::factorize::factor"));
      Edges edges;
      edges.reserve(node->edges().size());
      for (Edge const& edge : node->edges()) {
        auto it = mapping.find(edge.target);
        if (it == mapping.end()) {
          it = mapping.emplace_hint(it, edge.target, extractFactor(edge.target));
        }
        edges.emplace_back(edge.atomic, edge.label, it->second);
      }
      auto dominator = (node->dominatesItself() ? nullptr : mapping.at(node->dominator()));
      return Base::makeNode(std::move(edges), node->epsilon(), dominator);
    }};

    result.emplace_back(extractFactor(source));
    return result;
  }

  void reset() {
    Base::reset();
    concatCache_.clear();
  }

 private:
  HashMap<std::pair<Relation, Relation>, Relation> concatCache_;
};

template<interface::AtomicEngine AE>
requires interface::LabelSubstitutionEngine<typename AE::LabelEngine>
class SplittableRelationEngine : public RelationEngine<AE> {
  using Base = RelationEngine<AE>;

 public:
  using Relation = typename Base::Relation;
  using AtomicEngine = typename Base::AtomicEngine;
  using Symbol = typename Base::Symbol;
  using LabelEngine = typename Base::LabelEngine;
  using LabelValue = typename Base::LabelValue;
  using GrammarAnalysis = typename Base::GrammarAnalysis;
  using Atomic = typename Base::Atomic;
  using EpsilonValue = typename Base::EpsilonValue;
  using Edge = typename Base::Edge;
  using Edges = typename Base::Edges;
  using SplitResult = std::pair<Relation, std::shared_ptr<std::vector<LabelValue>>>;

  using Base::Base;

  // Splits the node into its shape and values with simultaneous extraction of the first factor.
  //
  // The shape is the first factor of the original graph but independent of labels as each of them
  // is replaced with a variable.
  //
  // The values are a sequence of the original labels in the order of newly created variables,
  // meaning the variables could be substituted by the label restoring the original node.
  //
  // Note: edges on each node are sorted by (atomic, target) allowing to extract shapes
  // deterministically.
  SplitResult split(Relation source) {
    STDEBUG(TIMER("SplittableRelationEngine::split"));
    STINFO(COUNTER("SplittableRelationEngine::split"));
    if (auto it = splitCache_.find(source); it != splitCache_.end()) {
      STINFO(COUNTER("SplittableRelationEngine::split - memoized"));
      return it->second;
    }
    STINFO(COUNTER("SplittableRelationEngine::split - compute"));

    auto values = std::make_shared<std::vector<LabelValue>>();
    if (Base::isEmpty(source) || Base::isRoot(source)) {
      return splitCache_.try_emplace(source, source, std::move(values)).first->second;
    }

    auto variable = [&](LabelValue label) {
      auto index = static_cast<int>(values->size());
      values->emplace_back(std::move(label));
      return Base::labelEngine().variable(index);
    };

    HashMap<Relation, Relation> mapping{
        {source->dominatesItself() ? Base::root() : source->dominator(), Base::root()}};
    auto extractShape = stdutil::recurse{[&](auto extractShape, Relation node) -> Relation {
      STINFO(COUNTER("SplittableRelationEngine::split::shape"));
      // Find the shape for each target.
      std::ranges::for_each(node->edges(), [&](Edge const& edge) {
        if (auto it = mapping.find(edge.target); it == mapping.end()) {
          STINFO(COUNTER("SplittableRelationEngine::split::shape - memoized"));
          mapping.emplace_hint(it, edge.target, extractShape(edge.target));
        }
      });
      // Create variables only after the recursive invocations to make them consecutive.
      Edges edges;
      edges.reserve(node->edges().size());
      std::ranges::for_each(node->edges(), [&](Edge const& edge) {
        edges.emplace_back(edge.atomic, variable(edge.label), mapping.at(edge.target));
      });
      auto epsilon = node->isFinal() ? std::optional{variable(*node->epsilon())} : std::nullopt;
      auto dominator = (node->dominatesItself() ? nullptr : mapping.at(node->dominator()));
      return Base::makeNode(std::move(edges), std::move(epsilon), dominator);
    }};

    auto shape = extractShape(source);
    return splitCache_.try_emplace(source, shape, std::move(values)).first->second;
  }

  SplitResult concat(Relation lhs, Relation rhs) {
    STDEBUG(TIMER("SplittableRelationEngine::concat"));
    STINFO(COUNTER("SplittableRelationEngine::concat"));
    if (Base::isEmpty(lhs) || Base::isRoot(rhs)) {
      STINFO(COUNTER("SplittableRelationEngine::concat - root"));
      return split(lhs);
    }
    if (Base::isEmpty(rhs) || Base::isRoot(lhs)) {
      STINFO(COUNTER("SplittableRelationEngine::concat - root"));
      return split(rhs);
    }
    if (auto it = concatSplitCache_.find({lhs, rhs}); it != concatSplitCache_.end()) {
      STINFO(COUNTER("SplittableRelationEngine::concat - memoized"));
      return it->second;
    }
    STINFO(COUNTER("SplittableRelationEngine::concat - compute"));
    // Replace root of lhs with rhs, effectively appending rhs to each lhs node.
    // All original non-epsilon edges are preserved. Epsilon edges are only preserved if the rhs
    // is final. The only possible new edges are these copied from rhs and only in nodes that had an
    // epsilon path to the root, which is equivalent to them being final.
    auto appendRhs = stdutil::recurse{[this, rhs](auto appendRhs, Relation node) {
      if (Base::isRoot(node)) {
        return rhs;
      }
      STINFO(COUNTER("SplittableRelationEngine::concat::append"));
      if (auto it = concatCache_.find({node, rhs}); it != concatCache_.end()) {
        STINFO(COUNTER("SplittableRelationEngine::concat::append - memoized"));
        return it->second;
      }
      auto const& le = Base::labelEngine();

      Edges edges;
      edges.reserve(node->edges().size() + (node->isFinal() ? rhs->edges().size() : 0));
      for (Edge const& edge : node->edges()) {
        edges.emplace_back(edge.atomic, le.shiftVariablesContext(edge.label),
                           appendRhs(edge.target));
      }

      std::optional<LabelValue> epsilon;
      if (node->isFinal()) {
        LabelValue labelPrefix = le.shiftVariablesContext(*node->epsilon());
        for (Edge const& edge : rhs->edges()) {
          edges.emplace_back(edge.atomic, le.multiply(labelPrefix, edge.label), edge.target);
        }
        if (rhs->isFinal()) {
          epsilon = le.multiply(std::move(labelPrefix), *rhs->epsilon());
        }
      }

      // It is final and dominates itself or previously final root is replaced with a non final
      // node, so final nodes in the lhs are no longer final and dominator must be found again.
      // Otherwise, all final nodes are preserved and the new dominator is simply mapped old
      // dominator.
      auto dominator = epsilon.has_value() || !rhs->isFinal()
                           ? nullptr
                           : concatCache_.at({node->dominator(), rhs});
      Relation result = Base::makeNode(std::move(edges), std::move(epsilon), dominator);
      concatCache_.emplace(std::pair{node, rhs}, result);
      return result;
    }};
    return concatSplitCache_.emplace(std::pair{lhs, rhs}, split(appendRhs(lhs))).first->second;
  }

  std::vector<SplitResult> factorize(Relation source) {
    STDEBUG(TIMER("SplittableRelationEngine::factorize"));
    STINFO(COUNTER("SplittableRelationEngine::factorize"));

    std::vector<SplitResult> result;
    if (!source->dominatesItself()) {
      result = factorize(source->dominator());
    }
    if (!Base::isRoot(source)) {
      result.emplace_back(split(source));
    }
    return result;
  }

  void reset() {
    Base::reset();
    concatCache_.clear();
    splitCache_.clear();
    concatSplitCache_.clear();
  }

 private:
  HashMap<Relation, SplitResult> splitCache_;
  HashMap<std::pair<Relation, Relation>, Relation> concatCache_;
  HashMap<std::pair<Relation, Relation>, SplitResult> concatSplitCache_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_RELATIONENGINE_HPP
