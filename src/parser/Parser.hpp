#ifndef RELATIONAL_PARSER_PARSER_PARSER_HPP
#define RELATIONAL_PARSER_PARSER_PARSER_HPP

#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <vector>

#include "util/Printing.hpp"

namespace relparser {

template<interface::RelationEngine RE>
class Parser {
 public:
  using RelationEngine = RE;
  using Symbol = typename RelationEngine::Symbol;
  using AtomicEngine = typename RelationEngine::AtomicEngine;
  using GrammarAnalysis = typename AtomicEngine::GrammarAnalysis;
  using LabelEngine = typename RelationEngine::LabelEngine;

  using Relation = typename RelationEngine::Relation;
  using EpsilonValue = typename RelationEngine::EpsilonValue;

  explicit Parser(RelationEngine relationEngine, std::size_t statusUpdateEveryTokens = 0)
      : relationEngine_{std::move(relationEngine)},
        statusUpdateEveryTokens_{statusUpdateEveryTokens} {
  }

  Relation parse(std::vector<Symbol> const& input) {
    TIMER("Parser::parse");
    SPDLOG_INFO("Parsing {} terminals", input.size());

    auto relation = relationEngine_.init();

    for (Symbol token : input) {
      SPDLOG_DEBUG("Next symbol: {}", token);
      relation = relationEngine_.computePhase(std::move(relation), token);
      ++tokensCount_;
      if (statusUpdateEveryTokens_ != 0 && tokensCount_ % statusUpdateEveryTokens_ == 0) {
        fmt::print("\r{}", tokensCount_);
        std::fflush(stdout);
      }
      if (relationEngine_.isEmpty(relation)) {
        break;
      }
    }

    return relation;
  }

  std::optional<EpsilonValue> epsilon(Relation relation) const {
    return relationEngine_.epsilon(relation);
  }

  void reset() {
    relationEngine_.reset();
  }

  RelationEngine const& relationEngine() const {
    return relationEngine_;
  }

  std::size_t parsedTokensCount() const {
    return tokensCount_;
  }

 private:
  RelationEngine relationEngine_;
  mutable std::size_t tokensCount_{0};
  std::size_t statusUpdateEveryTokens_{0};
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_PARSER_HPP
