#ifndef RELATIONAL_PARSER_PARSER_GRAMMAR_HPP
#define RELATIONAL_PARSER_PARSER_GRAMMAR_HPP

#include <absl/strings/str_join.h>
#include <span>
#include <vector>

#include "parser/Interfaces.hpp"

namespace relparser {

template<interface::Symbol S>
class Grammar {
 public:
  using Symbol = S;

  class Production {
   public:
    using Symbol = S;

    Production(std::string&& label, Symbol lhs, std::vector<Symbol>&& rhs)
        : label_(std::move(label)), lhs_(lhs), rhs_(std::move(rhs)) {
    }

    std::string const& label() const {
      return label_;
    }

    Symbol lhs() const {
      return lhs_;
    }

    std::vector<Symbol> const& rhs() const {
      return rhs_;
    }

    friend std::ostream& operator<<(std::ostream& out, Production const& p) {
      return out << p.lhs() << "->"
                 << (p.rhs().empty() ? "&epsilon;" : absl::StrJoin(p.rhs(), " "));
    }

   private:
    std::string label_;
    Symbol lhs_;
    std::vector<Symbol> rhs_;
  };
  static_assert(interface::Production<Production>);

  void setStart(Symbol s) {
    start_ = s;
  }

  Symbol start() const {
    return start_;
  }

  void addProduction(std::string label, Symbol lhs, std::vector<Symbol> rhs) {
    productions_.emplace_back(std::move(label), lhs, std::move(rhs));
  }

  std::span<Production const> productions() const {
    return productions_;
  }

  void setTerminalLabel(Symbol terminal, std::string label) {
    terminalLabels_.emplace(terminal, std::move(label));
  }

  std::string const& terminalLabel(Symbol terminal) const {
    static std::string empty_label_;
    if (auto it = terminalLabels_.find(terminal); it != terminalLabels_.end()) {
      return it->second;
    }
    return empty_label_;
  }

 private:
  Symbol start_;
  std::vector<Production> productions_;
  HashMap<Symbol, std::string> terminalLabels_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_GRAMMAR_HPP
