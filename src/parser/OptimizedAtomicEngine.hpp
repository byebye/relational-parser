#ifndef RELATIONAL_PARSER_PARSER_OPTIMIZEDATOMICENGINE_HPP
#define RELATIONAL_PARSER_PARSER_OPTIMIZEDATOMICENGINE_HPP

#include <absl/container/node_hash_map.h>
#include <absl/strings/str_join.h>
#include <spdlog/spdlog.h>
#include <queue>
#include <util/Printing.hpp>
#include <vector>

#include "CommonTypes.hpp"
#include "parser/Interfaces.hpp"
#include "parser/labels/ParsingSemiring.hpp"
#include "util/Container.hpp"

namespace relparser {

namespace detail {

template<interface::Atomic A>
struct Partitioning {
  using Atomic = A;

  template<std::ranges::forward_range R>
  requires std::same_as<std::remove_cvref_t<std::ranges::range_value_t<R>>, Atomic>
  explicit Partitioning(R partition = {}) : partitionCount{1} {
    for (Atomic atomic : partition) {
      partitionIds.emplace(atomic, 1);
    }
  }

  template<std::ranges::forward_range R>
  requires std::same_as<std::remove_cvref_t<std::ranges::range_value_t<R>>, std::vector<Atomic>>
  explicit Partitioning(R partitions) : partitionCount(std::ranges::size(partitions)) {
    std::size_t partitionId = 1;
    for (auto&& partition : partitions) {
      for (Atomic atomic : partition) {
        partitionIds.emplace(atomic, partitionId);
      }
      ++partitionId;
    }
  }

  int partitionId(Atomic atomic) const {
    // 0 - group of atomics without derivatives.
    return stdutil::getOr(partitionIds, atomic, 0);
  }

  bool operator==(Partitioning const& rhs) const {
    return partitionCount == rhs.partitionCount;
  }

  Map<Atomic, int> partitionIds;
  int partitionCount{0};
};

template<interface::AtomicEngine AE>
struct Optimizer {
  using Atomic = typename AE::Atomic;
  using AtomicEdges = typename AE::AtomicEdges;

  template<typename Partitioner>
  Partitioning<Atomic> operator()(AtomicEdges const& edges, Partitioner&& partitioner) const {
    Timer timer{};
    auto partitioning = Partitioning<Atomic>{getAllAtomics(edges)};
    SPDLOG_TRACE("[Optimizer] init: {}", timer.stop().ms());
    while (true) {
      timer.restart();
      auto refinedPartitioning = partitioner(edges, partitioning);
      SPDLOG_TRACE("[Optimizer] partitioner: {} in {}", refinedPartitioning.partitionCount,
                   timer.stop().ms());
      if (partitioning == refinedPartitioning) {
        return partitioning;
      }
      partitioning = std::move(refinedPartitioning);
    }
  }

  static HashSet<Atomic> getAllAtomics(AtomicEdges const& edges) {
    HashSet<Atomic> allAtomics;
    for (auto const& [source, bySymbol, target, value] : edges) {
      allAtomics.emplace(source);
      allAtomics.emplace(target);
    }
    return allAtomics;
  }
};

template<interface::AtomicEngine AE>
struct ForwardPartitioner {
  using Atomic = typename AE::Atomic;
  using Symbol = typename AE::Symbol;
  using LabelValue = typename AE::LabelValue;
  using AtomicEdges = typename AE::AtomicEdges;
  // (epsilon, [outgoing edges])
  using Spec = std::pair<LabelValue, std::vector<std::tuple<Symbol, Atomic, LabelValue>>>;

  Map<Atomic, LabelValue> const& atomicEpsilon_;
  HashMap<Atomic, Spec> specs_;
  HashMap<Spec, std::vector<Atomic>> grouping_;

  explicit ForwardPartitioner(Map<Atomic, LabelValue> const& atomicEpsilon)
      : atomicEpsilon_{atomicEpsilon} {
    SPDLOG_TRACE("[ForwardPartitioner]");
  }

  Partitioning<Atomic> operator()(AtomicEdges const& edges,
                                  Partitioning<Atomic> const& partitioning) {
    specs_.clear();
    grouping_.clear();
    for (auto const& [source, symbol, target, value] : edges) {
      specs_[source].second.emplace_back(symbol, partitioning.partitionId(target), value);
      specs_.try_emplace(target, Spec{stdutil::getOr(atomicEpsilon_, target, LabelValue{}), {}});
    }
    for (auto& [source, spec] : specs_) {
      spec.first = stdutil::getOr(atomicEpsilon_, source, LabelValue{});
      std::ranges::sort(spec.second);
      spec.second.erase(std::ranges::unique(spec.second).end(), spec.second.end());
      grouping_[spec].emplace_back(source);
    }
    return Partitioning<Atomic>{grouping_ | std::ranges::views::values};
  }

  static AtomicEdges mapEdges(AtomicEdges const& edges, Partitioning<Atomic> const& partitioning) {
    AtomicEdges newEdges;
    Map<int, Atomic> representatives;
    for (auto const& [source, symbol, target, value] : edges) {
      auto const sourceId = partitioning.partitionId(source);
      if (representatives.try_emplace(sourceId, source).first->second != source) {
        continue;
      }
      auto const targetId = partitioning.partitionId(target);
      newEdges.emplace_back(sourceId, symbol, targetId, value);
    }
    return newEdges;
  }
};

template<interface::AtomicEngine AE>
struct BackwardPartitioner {
  using Atomic = typename AE::Atomic;
  using Symbol = typename AE::Symbol;
  using LabelValue = typename AE::LabelValue;
  using AtomicEdges = typename AE::AtomicEdges;

  Set<Atomic> const& inputNodes_;

  using Spec = std::pair<int, std::vector<std::tuple<Symbol, Atomic, LabelValue>>>;
  HashMap<Atomic, Spec> specs_;
  HashMap<Spec, std::vector<Atomic>> grouping_;

  explicit BackwardPartitioner(Set<Atomic> const& inputNodes) : inputNodes_{inputNodes} {
    SPDLOG_TRACE("[BackwardPartitioner]");
  }

  Partitioning<Atomic> operator()(AtomicEdges const& edges,
                                  Partitioning<Atomic> const& partitioning) {
    specs_.clear();
    grouping_.clear();
    for (auto const& [source, symbol, target, value] : edges) {
      specs_[target].second.emplace_back(symbol, partitioning.partitionId(source), value);
    }
    for (Atomic atomic : inputNodes_) {
      specs_[atomic].first = atomic;
    }
    for (auto&& [source, spec] : specs_) {
      std::ranges::sort(spec.second);
      spec.second.erase(std::ranges::unique(spec.second).end(), spec.second.end());
      grouping_[spec].emplace_back(source);
    }
    return Partitioning<Atomic>{grouping_ | std::ranges::views::values};
  }

  static AtomicEdges mapEdges(AtomicEdges const& edges, Partitioning<Atomic> const& partitioning) {
    Set<std::tuple<Atomic, Symbol, int>> joinedEdges;
    AtomicEdges newEdges;
    for (auto const& [source, symbol, target, value] : edges) {
      auto const targetId = partitioning.partitionId(target);
      if (bool newEdge = joinedEdges.emplace(source, symbol, targetId).second; !newEdge) {
        continue;
      }
      auto const sourceId = partitioning.partitionId(source);
      newEdges.emplace_back(sourceId, symbol, targetId, value);
    }
    return newEdges;
  }
};

}  // namespace detail

enum class AtomicOptimization { Unreachable, Forward, Backward, All };

template<interface::AtomicGraph AG, AtomicOptimization opt>
class OptimizedAtomicEngine {
 public:
  using AtomicGraph = AG;
  using GrammarAnalysis = typename AG::GrammarAnalysis;
  using Production = typename GrammarAnalysis::Grammar::Production;
  using Symbol = typename AG::Symbol;
  using LabelEngine = typename AG::LabelEngine;
  using LabelValue = typename LabelEngine::Value;
  using Node = typename AtomicGraph::Node;
  using Atomic = int;

  using AtomicEdges = std::vector<std::tuple<Atomic, Symbol, Atomic, LabelValue>>;
  using AtomicDerivatives = Map<Atomic, std::vector<std::tuple<Symbol, Atomic, LabelValue>>>;

  explicit OptimizedAtomicEngine(AtomicGraph atomicGraph) : atomicGraph_{std::move(atomicGraph)} {
    int atomicId = 1;
    Map<std::pair<Node, Node>, int> atomicMap;
    Map<int, std::pair<int, int>> atomicFromId;
    auto getAtomic = [&](std::pair<Node, Node> node) {
      auto it = atomicMap.find(node);
      if (it == atomicMap.end()) {
        atomicFromId.emplace(atomicId, node);
        it = atomicMap.emplace(node, atomicId++).first;
      }
      return it->second;
    };
    auto addEpsilon = [&](std::pair<Node, Node> atomic) {
      if (atomicGraph_.isNodeReachable(atomic.second, atomic.first)) {
        atomicEpsilon_.emplace(getAtomic(atomic),
                               atomicGraph_.computePathsValue(atomic.second, atomic.first));
      }
    };
    Timer timer{};
    AtomicEdges allDerivatives;

    auto start = grammarAnalysis().grammar().start();
    Node startNode = atomicGraph_.symbolId(start);

    // Special value atomic representing (eps, eps).
    auto const& le = atomicGraph_.labelEngine();
    Atomic epsilonAtomic = getAtomic({-1, -1});
    atomicEpsilon_.emplace(epsilonAtomic, le.one());

    // root = nulling({(start, eps)}), so only derivative by start exists (no closure).
    Atomic startAtomic = getAtomic({startNode, 0});
    allDerivatives.emplace_back(startAtomic, start, epsilonAtomic,
                                atomicGraph_.labelEngine().one());
    if (grammarAnalysis().isNullable(start)) {
      atomicEpsilon_.emplace(startAtomic, le.guiding(grammarAnalysis().nullableValue(start)));
    }

    inputAtomic_.try_emplace({start, 0}, startAtomic);
    Map<Atomic, int> incomingCount{{startAtomic, 1}};
    Set<Atomic> inputNodes{startAtomic};

    // Node ids (in atomic graph):
    // - eps: 0
    // - symbol: 1, ..., |S|
    // - production position P(0,0), ..., P(n,|rhs_n|): |S|+1, |S|+2, ...
    for (Symbol s : grammarAnalysis().allSymbols()) {
      for (Symbol t : grammarAnalysis().terminals()) {
        Node source = atomicGraph_.symbolId(s);
        Node target = atomicGraph_.symbolId(t);
        addEpsilon({source, target});
        Atomic atomic = getAtomic({source, target});
        inputAtomic_.try_emplace({s, t}, getAtomic({source, target}));
        inputNodes.emplace(atomic);
        incomingCount[atomic]++;
      }
    }
    Node derivativeByNode = grammarAnalysis().allSymbols().size() + 1;
    for (Production const& production : grammarAnalysis().grammar().productions()) {
      for (Symbol derivativeBy : production.rhs()) {
        for (Symbol s : grammarAnalysis().firstDerivableFromSymbols(production.lhs())) {
          Node source = atomicGraph_.symbolId(s);
          addEpsilon({source, derivativeByNode + 1});
          for (Node target : atomicGraph_.reverseReachableNodes(derivativeByNode)) {
            allDerivatives.emplace_back(getAtomic({source, target}), derivativeBy,
                                        getAtomic({source, derivativeByNode + 1}),
                                        atomicGraph_.computePathsValue(target, derivativeByNode));
            addEpsilon({source, target});
            incomingCount.try_emplace(getAtomic({source, target}), 0);
            incomingCount[getAtomic({source, derivativeByNode + 1})]++;
          }
        }
        ++derivativeByNode;
      }
      ++derivativeByNode;
    }

    auto getAllAtomics = [](AtomicEdges const& edges) {
      Set<Atomic> allAtomics;
      for (auto const& [source, symbol, target, value] : edges) {
        allAtomics.emplace(source);
        allAtomics.emplace(target);
      }
      return allAtomics;
    };

    SPDLOG_INFO("Optimizing atomic engine nodes: {} in {}", getAllAtomics(allDerivatives).size(),
                timer.stop().ms());
    // auto printNfa = [&]() {
    //   fmt::print("Number of states: {}\n", getAllAtomics(allDerivatives).size());
    //   if constexpr (!std::same_as<typename AtomicGraph::Semiring, ParsingSemiring<Production>>) {
    //     auto printNode = [numSymbols = grammarAnalysis().allSymbols().size() + 1,
    //         this](Node node) -> std::string {
    //       if (node == 0)
    //         return "ϵ";
    //       if (node < numSymbols)
    //         return fmt::format("{}", atomicGraph_.nodeSymbol(node));
    //       auto const& [p, pos] = atomicGraph_.nodeProductionPosition(node);
    //       return fmt::format(
    //           "{}->{}", p->lhs(),
    //           (p->rhs().empty() ? "ϵ"
    //                             : absl::StrJoin(std::ranges::take_view{p->rhs(), pos}, "∙") + "⚫"
    //                               + absl::StrJoin(std::ranges::drop_view{p->rhs(), pos}, "∙")));
    //     };
    //
    //     auto printAtomic = [&](Atomic atomic) {
    //       std::pair<int, int> ap = atomicFromId.at(atomic);
    //       return fmt::format("{},{}", printNode(ap.first), printNode(ap.second));
    //     };
    //
    //     for (auto const& [atomic, derivatives] : atomicDerivatives_) {
    //       fmt::print("Atomic derivatives: {}\n", printAtomic(atomic));
    //       for (auto const& [bySymbol, atomic, value] : derivatives) {
    //         fmt::print("  - By symbol: {}\n", bySymbol);
    //         fmt::print("   -- label: {}, atomic: {}, eps: {}\n", value, printAtomic(atomic),
    //                    atomicEpsilon_.at(atomic));
    //       }
    //     }
    //   }
    // };

    using namespace detail;

    auto remap = [&](Partitioning<Atomic> const& partitioning, bool sumEpsilon = false) {
      Map<Atomic, LabelValue> atomicEpsilonTmp;
      for (auto& [atomic, eps] : atomicEpsilon_) {
        if (sumEpsilon) {
          auto [it, inserted] =
              atomicEpsilonTmp.try_emplace(partitioning.partitionId(atomic), std::move(eps));
          if (!inserted) {
            it->second = atomicGraph_.labelEngine().add(std::move(it->second), std::move(eps));
          }
        }
        else {
          atomicEpsilonTmp.try_emplace(partitioning.partitionId(atomic), std::move(eps));
        }
      }
      atomicEpsilon_.swap(atomicEpsilonTmp);
      for (auto& [_, atomic] : inputAtomic_) {
        atomic = partitioning.partitionId(atomic);
      }
      Map<int, std::pair<int, int>> atomicFromIdTmp;
      for (auto const& [id, atomic] : atomicFromId) {
        atomicFromIdTmp.try_emplace(partitioning.partitionId(id), atomic);
      }
      atomicFromId.swap(atomicFromIdTmp);
      Set<Atomic> inputNodesTmp;
      for (Atomic atomic : inputNodes) {
        inputNodesTmp.emplace(partitioning.partitionId(atomic));
      }
      inputNodes.swap(inputNodesTmp);
    };

    // Remove unreachable atomics.
    timer.restart();
    std::queue<Atomic> atomicsToProcess;
    for (auto const& [atomic, count] : incomingCount) {
      if (count == 0) {
        atomicsToProcess.push(atomic);
      }
    }
    std::ranges::sort(allDerivatives,
                      [](auto&& a, auto&& b) { return std::get<0>(a) < std::get<0>(b); });
    Set<Atomic> unreachable;
    for (; !atomicsToProcess.empty(); atomicsToProcess.pop()) {
      Atomic atomic = atomicsToProcess.front();
      unreachable.emplace(atomic);
      auto derivs = std::ranges::equal_range(allDerivatives, atomic, std::ranges::less{},
                                             [](auto&& edge) { return std::get<0>(edge); });
      for (auto const& [_, symbol, target, value] : derivs) {
        if (--incomingCount[target] == 0) {
          atomicsToProcess.push(target);
        }
      }
    }
    allDerivatives.erase(std::remove_if(allDerivatives.begin(), allDerivatives.end(),
                                        [&unreachable](auto&& edge) {
                                          return unreachable.contains(std::get<0>(edge));
                                        }),
                         allDerivatives.end());
    SPDLOG_INFO("Removed unreachable nodes: {} in {}", getAllAtomics(allDerivatives).size(),
                timer.stop().ms());

    auto mergeEdges = [this](AtomicEdges& edges) {
      if (edges.empty()) {
        return;
      }
      std::ranges::sort(edges);
      auto last = edges.begin();
      auto current = last;
      while (++current != edges.end()) {
        if (std::get<0>(*last) == std::get<0>(*current)
            && std::get<1>(*last) == std::get<1>(*current)
            && std::get<2>(*last) == std::get<2>(*current)) {
          std::get<3>(*last) = atomicGraph_.labelEngine().add(std::move(std::get<3>(*last)),
                                                              std::move(std::get<3>(*current)));
        }
        else if (++last != current) {
          *last = std::move(*current);
        }
      }
      edges.erase(++last, edges.end());
    };

    if (opt == AtomicOptimization::Forward || opt == AtomicOptimization::All) {
      timer.restart();
      Partitioning<Atomic> forwardPartition = Optimizer<OptimizedAtomicEngine>{}(
          allDerivatives, ForwardPartitioner<OptimizedAtomicEngine>{atomicEpsilon_});
      allDerivatives =
          ForwardPartitioner<OptimizedAtomicEngine>::mapEdges(allDerivatives, forwardPartition);
      remap(forwardPartition);
      mergeEdges(allDerivatives);
      SPDLOG_INFO("Forward optimization: {} in {}", getAllAtomics(allDerivatives).size(),
                  timer.stop().ms());
    }

    if (opt == AtomicOptimization::Backward || opt == AtomicOptimization::All) {
      timer.restart();
      Partitioning<Atomic> backwardPartition = Optimizer<OptimizedAtomicEngine>{}(
          allDerivatives, BackwardPartitioner<OptimizedAtomicEngine>{inputNodes});
      allDerivatives =
          BackwardPartitioner<OptimizedAtomicEngine>::mapEdges(allDerivatives, backwardPartition);
      remap(backwardPartition, /*sumEpsilon=*/true);
      mergeEdges(allDerivatives);
      SPDLOG_INFO("Backward optimization: {} in {}", getAllAtomics(allDerivatives).size(),
                  timer.stop().ms());
    }

    for (auto const& [source, symbol, target, value] : allDerivatives) {
      atomicDerivatives_[source].emplace_back(symbol, target, value);
    }
  }

  GrammarAnalysis const& grammarAnalysis() const {
    return atomicGraph_.grammarAnalysis();
  }

  AtomicGraph const& atomicGraph() const {
    return atomicGraph_;
  }

  // Atomic that represents {(start, eps)}.
  Atomic root() const {
    return inputAtomic_.at({grammarAnalysis().grammar().start(), 0});
  }

  // Requires: isEmpty(s, t) == false.
  Atomic atomic(Symbol s, Symbol t) const {
    return inputAtomic_.at({s, t});
  }

  // Returns true if the atomic is an empty set.
  // [(s, eps)]^(t) = {}
  bool isEmpty(Symbol s, Symbol t) const {
    return !grammarAnalysis().isFirstDerivable(s, t);
  }

  bool containsEpsilon(Atomic atomic) const {
    return atomicEpsilon_.contains(atomic);
  }

  LabelValue extractEpsilon(Atomic atomic) const {
    return atomicEpsilon_.at(atomic);
  }

  bool hasDerivatives(Atomic atomic) const {
    return atomicDerivatives_.contains(atomic);
  }

  std::vector<std::tuple<Symbol, Atomic, LabelValue>> const& allDerivatives(Atomic atomic) const {
    return atomicDerivatives_.at(atomic);
  }

 private:
  AtomicGraph atomicGraph_;
  AtomicDerivatives atomicDerivatives_;
  Map<std::pair<Symbol, Symbol>, Atomic> inputAtomic_;
  Map<Atomic, LabelValue> atomicEpsilon_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_OPTIMIZEDATOMICENGINE_HPP
