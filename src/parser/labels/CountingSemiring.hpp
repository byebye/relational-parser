#ifndef RELATIONAL_PARSER_PARSER_LABELS_COUNTINGSEMIRING_HPP
#define RELATIONAL_PARSER_PARSER_LABELS_COUNTINGSEMIRING_HPP

#include <spdlog/spdlog.h>
#include <limits>

#include "parser/Interfaces.hpp"

namespace relparser {

template<interface::Production P>
class CountingSemiring {
 public:
  using Value = std::size_t;
  using Production = P;
  constexpr static bool isCommutative = true;

  static std::size_t zero() {
    return 0;
  }

  static std::size_t one() {
    return 1;
  }

  static bool isZero(std::size_t value) {
    return value == 0;
  }

  static bool isOne(std::size_t value) {
    return value == 1;
  }

  static std::size_t add(std::size_t lhs, std::size_t rhs) {
    // if (lhs > std::numeric_limits<std::size_t>::max() - rhs) {
    //   SPDLOG_TRACE("CountingLabels::sum - overflow: {} + {}", lhs, rhs);
    //   return std::numeric_limits<std::size_t>::max();
    // }
    return lhs + rhs;
  }

  static std::size_t multiply(std::size_t lhs, std::size_t rhs) {
    // if (rhs != 0 && lhs > std::numeric_limits<std::size_t>::max() / rhs) {
    //   SPDLOG_TRACE("CountingLabels::multiply - overflow: {} * {}", lhs, rhs);
    //   return std::numeric_limits<std::size_t>::max();
    // }
    return lhs * rhs;
  }

  static std::size_t value(Production const&) {
    return 1;
  }
};

static_assert(interface::Semiring<CountingSemiring<interface::FakeProduction>>);

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_LABELS_COUNTINGSEMIRING_HPP
