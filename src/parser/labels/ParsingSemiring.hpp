#ifndef RELATIONAL_PARSER_PARSER_LABELS_PARSINGSEMIRING_HPP
#define RELATIONAL_PARSER_PARSER_LABELS_PARSINGSEMIRING_HPP

#include <fmt/ostream.h>

#include "util/Container.hpp"
#include "parser/Interfaces.hpp"

namespace relparser {

template<interface::Production P>
class ParsingSemiring {
 public:
  using Production = P;
  constexpr static bool isCommutative = false;

  struct ProductionNode;
  struct AddNode;
  struct MultiplyNode;
  using Node = std::variant<ProductionNode, AddNode, MultiplyNode>;
  using Value = std::shared_ptr<Node const>;

  struct ProductionNode {
    Production const* production;
  };

  struct AddNode {
    std::shared_ptr<Node const> lhs;
    std::shared_ptr<Node const> rhs;
  };

  struct MultiplyNode {
    std::shared_ptr<Node const> lhs;
    std::shared_ptr<Node const> rhs;
  };

  static Value const& zero() {
    static Value const zero_{nullptr};
    return zero_;
  }

  static Value const& one() {
    static Value const one_{std::make_shared<Node const>(ProductionNode{nullptr})};
    return one_;
  }

  static bool isZero(Value const& value) {
    return value == zero();
  }

  static bool isOne(Value const& value) {
    return value == one();
  }

  static Value add(Value const& lhs, Value const& rhs) {
    if (isZero(lhs)) {
      return rhs;
    }
    if (isZero(rhs)) {
      return lhs;
    }
    return std::make_shared<Node const>(AddNode{lhs, rhs});
  }

  static Value multiply(Value const& lhs, Value const& rhs) {
    if (isZero(lhs) || isOne(rhs)) {
      return lhs;
    }
    if (isZero(rhs) || isOne(lhs)) {
      return rhs;
    }
    return std::make_shared<Node const>(MultiplyNode{lhs, rhs});
  }

  static Value value(Production const& p) {
    return std::make_shared<Node const>(ProductionNode{&p});
  }

  friend std::ostream& operator<<(std::ostream& out, Node const& value) {
    auto print = stdutil::recurse{[&out](auto&& print, Node const& value) -> void {
      if (auto node = std::get_if<AddNode>(&value)) {
        fmt::print(out, "-- derivation 1 --\n");
        print(*node->lhs);
        fmt::print(out, "-- derivation 2 --\n");
        print(*node->rhs);
      }
      else if (auto node = std::get_if<MultiplyNode>(&value)) {
        print(*node->lhs);
        print(*node->rhs);
      }
      else if (auto node = std::get_if<ProductionNode>(&value)) {
        if (node->production) {
          fmt::print(out, "{}\n", *node->production);
        }
      }
    }};
    print(value);
    return out;
  }

  friend bool operator==(Node const& lhs, Node const& rhs) {
    auto cmp = stdutil::recurse{[&](auto&& cmp, Node const& lhs, Node const& rhs) -> bool {
      if (lhs.index() != rhs.index())
        return false;
      if (auto node = std::get_if<AddNode>(&lhs)) {
        return cmp(*node->lhs, *std::get<AddNode>(rhs).lhs)
               && cmp(*node->rhs, *std::get<AddNode>(rhs).rhs);
      }
      else if (auto node = std::get_if<MultiplyNode>(&lhs)) {
        return cmp(*node->lhs, *std::get<MultiplyNode>(rhs).lhs)
               && cmp(*node->rhs, *std::get<MultiplyNode>(rhs).rhs);
      }
      else if (auto node = std::get_if<ProductionNode>(&lhs)) {
        return node->production == std::get<ProductionNode>(rhs).production;
      }
      throw std::invalid_argument(
          fmt::format("ParsingSemiring::== - empty or invalid node type: {}", lhs.index()));
    }};
    return cmp(lhs, rhs);
  }
};

static_assert(interface::Semiring<ParsingSemiring<interface::FakeProduction>>);

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_LABELS_PARSINGSEMIRING_HPP
