#ifndef RELATIONAL_PARSER_PARSER_LABELS_LABELSVALUEENGINE_HPP
#define RELATIONAL_PARSER_PARSER_LABELS_LABELSVALUEENGINE_HPP

#include "parser/Interfaces.hpp"

namespace relparser {

template<interface::Semiring S>
requires S::isCommutative
struct LabelValueEngine : public S {
  using Semiring = S;
  using Value = typename Semiring::Value;
  using RelationValue = Value;
  using RelationNode = Value;

  Value flatten(Value value) const {
    return value;
  }

  Value guiding(Value value) const {
    return value;
  }

  Value pending(Value value) const {
    return value;
  }

  Value evaluate(Value value) const {
    return value;
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_LABELS_LABELSVALUEENGINE_HPP
