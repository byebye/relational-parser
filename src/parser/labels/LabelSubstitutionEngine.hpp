#ifndef RELATIONAL_PARSER_PARSER_LABELS_LABELSUBSTITUTIONENGINE_HPP
#define RELATIONAL_PARSER_PARSER_LABELS_LABELSUBSTITUTIONENGINE_HPP

#include <memory>
#include <ostream>
#include <variant>

#include "Statistics.hpp"
#include "parser/Interfaces.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<interface::Relation RN>
struct AdditionRhsIgnorantLabelEvaluator : RN {
  constexpr static bool isAdditionIgnorant = true;

  using RelationOperand = RN;
  using Semiring = typename RN::Semiring;
  using Value = RN;

  static Value add(Value lhs, Value rhs) {
    throw std::logic_error("Addition called on ignorant label evaluator.");
  }

  static Value multiply(Value lhs, Value rhs) {
    // Pending is kept reversed for more efficient value computation, therefore multiplication
    // is reversed as well.
    return {Semiring::multiply(std::move(rhs.pending), std::move(lhs.pending)),
            Semiring::multiply(std::move(lhs.guiding), std::move(rhs.guiding))};
  }

  static Value flatten(Value lhs) {
    return {Semiring::one(), Semiring::multiply(std::move(lhs.pending), std::move(lhs.guiding))};
  }
};

template<interface::Semiring S>
struct ValueLabelEvaluator : public S {
  constexpr static bool isAdditionIgnorant = false;

  using RelationOperand = S;
  using Semiring = S;
  using Value = typename S::Value;

  static Value flatten(Value lhs) {
    return lhs;
  }
};

template<interface::Semiring S>
struct RelationValue {
  using Value = RelationValue;
  using Semiring = S;
  using SemiringValue = typename S::Value;

  SemiringValue pending;
  SemiringValue guiding;

  static RelationValue zero() {
    return {S::zero(), S::zero()};
  }

  static RelationValue one() {
    return {S::one(), S::one()};
  }

  friend std::ostream& operator<<(std::ostream& out, RelationValue const& node) {
    if constexpr (Dereferencable<typename S::Value>) {
      return out << "Relation(" << *node.pending << ", " << *node.guiding << ")";
    }
    else {
      return out << "Relation(" << node.pending << ", " << node.guiding << ")";
    }
  }

  bool operator==(RelationValue const& rhs) const {
    if constexpr (Dereferencable<typename S::Value>) {
      return *this->pending == *rhs.pending && *this->guiding == *rhs.guiding;
    }
    else {
      return this->pending == rhs.pending && this->guiding == rhs.guiding;
    }
  }
};

template<typename... NodeTypes>
struct LabelNodes {
  struct AddNode;
  struct MultiplyNode;
  struct FlattenNode;
  using Value = std::variant<AddNode, MultiplyNode, FlattenNode, NodeTypes...>;
  using Node = std::shared_ptr<Value const>;

  struct AddNode {
    Node lhs;
    Node rhs;
  };

  struct MultiplyNode {
    Node lhs;
    Node rhs;
  };

  struct FlattenNode {
    Node node;
  };

  static Node makeNode(auto&& value) {
    return std::make_shared<Value const>(std::forward<decltype(value)>(value));
  }
};

namespace detail {
template<typename T>
struct SemiringType;
template<interface::Semiring S>
struct SemiringType<S> {
  using Semiring = S;
};
template<interface::Relation R>
struct SemiringType<R> {
  using Semiring = typename R::Semiring;
};
}  // namespace detail

template<typename R, typename... NodeTypes>
requires interface::Semiring<R> || interface::Relation<R>
class LabelRelationEngineBase {
 public:
  using RelationOperand = R;
  using Semiring = typename detail::SemiringType<R>::Semiring;
  using SemiringValue = typename Semiring::Value;
  using RelationValue = std::conditional_t<interface::Relation<R>, R, SemiringValue>;

  using Nodes = LabelNodes<RelationValue, NodeTypes...>;
  using AddNode = typename Nodes::AddNode;
  using MultiplyNode = typename Nodes::MultiplyNode;
  using FlattenNode = typename Nodes::FlattenNode;
  using Value = typename Nodes::Node;

  Value const& zero() const {
    static auto const& zero_{Nodes::makeNode(RelationOperand::zero())};
    return zero_;
  }

  Value const& one() const {
    static auto const& one_{Nodes::makeNode(RelationOperand::one())};
    return one_;
  }

  bool isZero(Value const& value) const {
    return value == zero();
  }

  bool isOne(Value const& value) const {
    return value == one();
  }

  Value pending(SemiringValue value) const {
    if (Semiring::isZero(value)) {
      return zero();
    }
    if (Semiring::isOne(value)) {
      return one();
    }
    if constexpr (interface::Relation<RelationValue>) {
      return Nodes::makeNode(
          RelationValue{.pending = std::move(value), .guiding = Semiring::one()});
    }
    else {
      return Nodes::makeNode(RelationValue{std::move(value)});
    }
  }

  Value guiding(SemiringValue value) const {
    if (Semiring::isZero(value)) {
      return zero();
    }
    if (Semiring::isOne(value)) {
      return one();
    }
    if constexpr (interface::Relation<RelationValue>) {
      return Nodes::makeNode(
          RelationValue{.pending = Semiring::one(), .guiding = std::move(value)});
    }
    else {
      return Nodes::makeNode(RelationValue{std::move(value)});
    }
  }

  Value add(Value const& lhs, Value const& rhs) const {
    if (isZero(lhs)) {
      return rhs;
    }
    if (isZero(rhs)) {
      return lhs;
    }
#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_DEBUG
    if constexpr (interface::Relation<RelationValue>) {
      if (lhs == rhs || *lhs == *rhs) {
        throw std::logic_error("Equal addition elements");
      }
    }
#endif
    return Nodes::makeNode(AddNode{lhs, rhs});
  }

  Value multiply(Value const& lhs, Value const& rhs) const {
    if (isZero(lhs) || isOne(rhs)) {
      return lhs;
    }
    if (isZero(rhs) || isOne(lhs)) {
      return rhs;
    }
    return Nodes::makeNode(MultiplyNode{lhs, rhs});
  }

  Value flatten(Value const& value) const {
    if constexpr (interface::Relation<RelationValue>) {
      if (std::holds_alternative<FlattenNode>(*value) || isZero(value) || isOne(value)) {
        return value;
      }
      return Nodes::makeNode(FlattenNode{value});
    }
    else {
      return value;
    }
  }
};

template<interface::Semiring S, interface::LabelEvaluator LE = ValueLabelEvaluator<S>>
requires std::same_as<S, typename LE::Semiring> && std::same_as<
    typename LE::RelationValue,
    typename LabelRelationEngineBase<typename LE::RelationOperand>::RelationValue>
class LabelDagEngine : public LabelRelationEngineBase<typename LE::RelationOperand> {
 public:
  using Semiring = S;
  using LabelEvaluator = LE;
  using RelationValue = typename LE::Value;

  using Base = LabelRelationEngineBase<typename LE::RelationOperand>;
  using AddNode = typename Base::AddNode;
  using MultiplyNode = typename Base::MultiplyNode;
  using FlattenNode = typename Base::FlattenNode;
  using Value = typename Base::Value;

  using RelationNode = RelationValue;

  RelationNode evaluate(Value const& value) const {
    HashMap<typename Base::Nodes::Value const*, RelationNode> cache;
    auto eval = stdutil::recurse{[&](auto&& eval, Value const& value) -> RelationNode {
      if (auto it = cache.find(value.get()); it != cache.end()) {
        STINFO(COUNTER("evaluate -- cached"));
        return it->second;
      }
      STINFO(COUNTER("evaluate"));
      RelationValue result;
      if (auto node = std::get_if<RelationValue>(value.get())) {
        result = *node;
      }
      else if (auto node = std::get_if<AddNode>(value.get())) {
        if constexpr (LabelEvaluator::isAdditionIgnorant) {
          result = eval(node->lhs);
        }
        else {
          result = LabelEvaluator::add(eval(node->lhs), eval(node->rhs));
        }
      }
      else if (auto node = std::get_if<MultiplyNode>(value.get())) {
        result = LabelEvaluator::multiply(eval(node->lhs), eval(node->rhs));
      }
      else if (auto node = std::get_if<FlattenNode>(value.get())) {
        result = LabelEvaluator::flatten(eval(node->node));
      }
      else {
        throw std::invalid_argument(fmt::format(
            "LabelDagEngine::evaluate - empty or invalid node type: {}", value->index()));
      }
      return cache.emplace(value.get(), std::move(result)).first->second;
    }};
    return eval(value);
  }
};

template<interface::Relation R>
class LabelIdentityDagEngine : public LabelRelationEngineBase<R> {
 public:
  using Base = LabelRelationEngineBase<R>;
  using Value = typename Base::Value;
  using RelationNode = Value;

  Value evaluate(Value const& value) const {
    return value;
  }
};

namespace detail {
struct VariableNode {
  int index;
  bool primary;
};
}  // namespace detail

template<typename R>
requires interface::Semiring<R> || interface::Relation<R>
class LabelSubstitutionEngineBase : public LabelRelationEngineBase<R, detail::VariableNode> {
 public:
  using Base = LabelRelationEngineBase<R, detail::VariableNode>;
  using RelationValue = typename Base::RelationValue;
  using Nodes = typename Base::Nodes;
  using AddNode = typename Base::AddNode;
  using MultiplyNode = typename Base::MultiplyNode;
  using FlattenNode = typename Base::FlattenNode;
  using VariableNode = detail::VariableNode;
  using Value = typename Base::Value;
  using NodeValue = typename Nodes::Value;

  Value const& variable(int index) const {
    STINFO(COUNTER("variables - created"));
    if (index >= variables_.size()) {
      while (index >= variables_.size()) {
        auto varIndex = static_cast<int>(variables_.size());
        variables_.emplace_back(Nodes::makeNode(VariableNode{varIndex, true}),
                                Nodes::makeNode(VariableNode{varIndex, false}));
      }
    }
    STTRACE(COUNTER_VARKEY(fmt::format("LabelSubstitutionEngine::variable[{:>4d}]", index)));
    return variables_[index].first;
  }

  // Changes all variables context from primary to secondary. Node cannot contain any variables
  // with secondary context.
  Value shiftVariablesContext(Value const& value) const {
    STDEBUG(COUNTER("LabelSubstitutionEngine::shiftVariablesContext"));
    if (std::holds_alternative<RelationValue>(*value)) {
      return value;
    }
    if (auto node = std::get_if<AddNode>(value.get())) {
      return Base::add(shiftVariablesContext(node->lhs), shiftVariablesContext(node->rhs));
    }
    if (auto node = std::get_if<MultiplyNode>(value.get())) {
      return Base::multiply(shiftVariablesContext(node->lhs), shiftVariablesContext(node->rhs));
    }
    if (auto node = std::get_if<FlattenNode>(value.get())) {
      return Base::flatten(shiftVariablesContext(node->node));
    }
    if (auto node = std::get_if<VariableNode>(value.get())) {
      if (!node->primary) {
        throw std::invalid_argument("Trying to shift secondary variable");
      }
      return variables_[node->index].second;
    }
    throw std::invalid_argument(fmt::format(
        "LabelSubstitutionEngine::shiftVariablesContext - empty or invalid node type: {}",
        value->index()));
  }

 protected:
  template<interface::SubstitutionContext SC>
  // requires std::same_as<typename SC::LabelValue, Value>
  std::pair<Value, SC const*> substituteVariable(Value const& value, SC const* context) const {
    STDEBUG(COUNTER("LabelSubstitutionEngine::substituteVariable"));
    std::size_t idLen{0}, len{0};
    Value const* variableValue = &value;
    while (auto variable = std::get_if<VariableNode>(variableValue->get())) {
      ++len;
      auto&& [var, parentContext] = context->parentValue(variable->index, variable->primary);
      context = parentContext;
      assert(context);
#if RELPARSER_STATS_ACTIVE_LEVEL < RELPARSER_STATS_LEVEL_TRACE
      variableValue = var;
#else
      if (variable->primary && *var == *variableValue) {
        ++idLen;
      }
      else if (idLen > 0) {
        STTRACE(COUNTER_VARKEY(fmt::format(
            "LabelSubstitutionEngine::substituteVariable::identityLen[{:>4d}]", idLen)));
        idLen = 0;
      }
      variableValue = var;
#endif
    }
    STTRACE(COUNTER_VARKEY(
        fmt::format("LabelSubstitutionEngine::substituteVariable::len[{:>4d}]", len)));
    return {*variableValue, context};
  }

  uint8_t contextMask(Value const value) const {
    if (auto node = std::get_if<RelationValue>(value.get())) {
      return 0;
    }
    if (auto node = std::get_if<VariableNode>(value.get())) {
      return node->primary ? 0b01 : 0b10;
    }
    if (auto it = contextMask_.find(value.get()); it != contextMask_.end()) {
      return it->second;
    }
    uint8_t mask = 0;
    if (auto node = std::get_if<AddNode>(value.get())) {
      mask = contextMask(node->lhs) | contextMask(node->rhs);
    }
    else if (auto node = std::get_if<MultiplyNode>(value.get())) {
      mask = contextMask(node->lhs) | contextMask(node->rhs);
    }
    else if (auto node = std::get_if<FlattenNode>(value.get())) {
      mask = contextMask(node->node);
    }
    else {
      throw std::invalid_argument(
          fmt::format("contextMask - invalid node type: {}", value->index()));
    }
    contextMask_.emplace(value.get(), mask);
    return mask;
  }

 private:
  mutable std::vector<std::pair<Value, Value>> variables_;
  mutable HashMap<NodeValue const*, uint8_t> contextMask_;
};

template<interface::Relation R>
class LabelIdentitySubstitutionEngine : public LabelSubstitutionEngineBase<R> {
 public:
  using Base = LabelSubstitutionEngineBase<R>;
  using RelationValue = typename Base::RelationValue;
  using AddNode = typename Base::AddNode;
  using MultiplyNode = typename Base::MultiplyNode;
  using FlattenNode = typename Base::FlattenNode;
  using VariableNode = detail::VariableNode;
  using Value = typename Base::Value;

  using RelationNode = Value;

  template<interface::SubstitutionContext SC>
  // requires std::same_as<typename SC::LabelValue, Value>
  RelationNode evaluate(Value const& value, SC const* context) const {
    using NodeValue = typename Base::NodeValue;
    using CacheKey = std::tuple<NodeValue const*, SC const*, SC const*>;
    auto cacheKey = [&](uint8_t contextMask, Value v, SC const* context) {
      SC const* p1 = (contextMask & 0b01) ? context->parentPrimary() : nullptr;
      SC const* p2 = (contextMask & 0b10) ? context->parentSecondary() : nullptr;
      return CacheKey{v.get(), p1, p2};
    };
    HashMap<CacheKey, RelationNode> cache;
    auto eval =
        stdutil::recurse{[&](auto&& eval, Value const& value, SC const* context) -> RelationNode {
          auto const contextMask = Base::contextMask(value);
          if (values_.emplace(value.get()).second) {
            STINFO(COUNTER("labels"));
          }
          if (!contextMask) {
            STINFO(COUNTER("## identity -- no variables"));
            return value;
          }
          if (auto it = cache.find(cacheKey(contextMask, value, context)); it != cache.end()) {
            STINFO(COUNTER("## cached"));
            return it->second;
          }
          RelationNode result;
          STINFO(COUNTER("# evaluate"));
          if (auto node = std::get_if<RelationValue>(value.get())) {
            STINFO(COUNTER("## value"));
            result = value;
          }
          else if (auto node = std::get_if<AddNode>(value.get())) {
            STINFO(COUNTER("## add"));
            result = Base::add(eval(node->lhs, context), eval(node->rhs, context));
          }
          else if (auto node = std::get_if<MultiplyNode>(value.get())) {
            STINFO(COUNTER("## multiply"));
            result = Base::multiply(eval(node->lhs, context), eval(node->rhs, context));
          }
          else if (auto node = std::get_if<FlattenNode>(value.get())) {
            STINFO(COUNTER("## flatten"));
            result = Base::flatten(eval(node->node, context));
          }
          else if (auto node = std::get_if<VariableNode>(value.get())) {
            if (!context) {
              throw std::invalid_argument("No context provided for the variable node");
            }
            STINFO(COUNTER("## variable"));
            auto&& [varValue, varContext] = Base::substituteVariable(value, context);
            result = eval(varValue, varContext);
          }
          else {
            throw std::invalid_argument(fmt::format(
                "LabelIdentitySubstitutionEngine::evaluate - empty or invalid node type: {}",
                value->index()));
          }
          auto&& [it, new_value] =
              cache.emplace(cacheKey(contextMask, value, context), std::move(result));
          if (new_value) {
            STINFO(COUNTER("cache values"));
          }
          return it->second;
        }};
    return eval(value, context);
  }

  mutable HashSet<typename Base::NodeValue const*> values_;
};

template<interface::Semiring S, interface::LabelEvaluator LE = ValueLabelEvaluator<S>>
requires std::same_as<S, typename LE::Semiring>
class LabelSubstitutionEngine : public LabelSubstitutionEngineBase<typename LE::RelationOperand> {
 public:
  using Semiring = S;
  using LabelEvaluator = LE;

  using Base = LabelSubstitutionEngineBase<typename LE::RelationOperand>;
  using RelationValue = typename Base::RelationValue;
  using AddNode = typename Base::AddNode;
  using MultiplyNode = typename Base::MultiplyNode;
  using FlattenNode = typename Base::FlattenNode;
  using VariableNode = detail::VariableNode;
  using Value = typename Base::Value;

  using RelationNode = RelationValue;

  template<interface::SubstitutionContext SC>
  // requires std::same_as<typename SC::LabelValue, Value>
  RelationNode evaluate(Value const& value, SC const* context) const {
    using NodeValue = typename Base::NodeValue;
    using CacheKey = std::tuple<NodeValue const*, SC const*, SC const*>;
    HashMap<CacheKey, RelationNode> cache;
    auto cacheKey = [&](uint8_t contextMask, Value v, SC const* context) {
      SC const* p1 = (contextMask & 0b01) ? context->parentPrimary() : nullptr;
      SC const* p2 = (contextMask & 0b10) ? context->parentSecondary() : nullptr;
      return CacheKey{v.get(), p1, p2};
    };
    auto eval = stdutil::recurse{[&](auto&& eval, Value const& value,
                                     SC const* context) -> RelationNode {
      auto const contextMask = Base::contextMask(value);
      if (values_.emplace(value.get()).second) {
        STINFO(COUNTER("labels"));
      }
      if (auto node = std::get_if<RelationValue>(value.get())) {
        STINFO(COUNTER("## value"));
        return *node;
      }
      if (auto it = cache.find(cacheKey(contextMask, value, context)); it != cache.end()) {
        STINFO(COUNTER("## cached"));
        return it->second;
      }
      STINFO(COUNTER("# evaluate"));
      RelationNode result;
      if (auto node = std::get_if<AddNode>(value.get())) {
        STINFO(COUNTER("## add"));
        if constexpr (LabelEvaluator::isAdditionIgnorant) {
          result = eval(node->lhs, context);
        }
        else {
          result = LabelEvaluator::add(eval(node->lhs, context), eval(node->rhs, context));
        }
      }
      else if (auto node = std::get_if<MultiplyNode>(value.get())) {
        STINFO(COUNTER("## multiply"));
        result = LabelEvaluator::multiply(eval(node->lhs, context), eval(node->rhs, context));
      }
      else if (auto node = std::get_if<FlattenNode>(value.get())) {
        STINFO(COUNTER("## flatten"));
        result = LabelEvaluator::flatten(eval(node->node, context));
      }
      else if (auto node = std::get_if<VariableNode>(value.get())) {
        if (!context) {
          throw std::invalid_argument("No context provided for the variable node");
        }
        STINFO(COUNTER("## variable"));
        auto&& [varValue, varContext] = Base::substituteVariable(value, context);
        result = eval(varValue, varContext);
      }
      else {
        throw std::invalid_argument(fmt::format(
            "LabelSubstitutionEngine::evaluate - empty or invalid node type: {}", value->index()));
      }
      auto&& [it, new_value] =
          cache.emplace(cacheKey(contextMask, value, context), std::move(result));
      if (new_value) {
        STINFO(COUNTER("cache values"));
      }
      return it->second;
    }};
    return eval(value, context);
  }
  mutable HashSet<typename Base::NodeValue const*> values_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_LABELS_LABELSUBSTITUTIONENGINE_HPP
