#ifndef RELATIONAL_PARSER_PARSER_LABELS_RECOGNITIONSEMIRING_HPP
#define RELATIONAL_PARSER_PARSER_LABELS_RECOGNITIONSEMIRING_HPP

#include "parser/Interfaces.hpp"

namespace relparser {

template<interface::Production P>
class RecognitionSemiring {
 public:
  using Value = bool;
  using Production = P;
  constexpr static bool isCommutative = true;

  static bool zero() {
    return false;
  }

  static bool one() {
    return true;
  }

  static bool isZero(bool value) {
    return value == 0;
  }

  static bool isOne(bool value) {
    return value == 1;
  }

  static bool add(bool lhs, bool rhs) {
    return lhs || rhs;
  }

  static bool multiply(bool lhs, bool rhs) {
    return lhs && rhs;
  }

  static bool value(Production const&) {
    return true;
  }
};

static_assert(interface::Semiring<RecognitionSemiring<interface::FakeProduction>>);

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_LABELS_RECOGNITIONSEMIRING_HPP
