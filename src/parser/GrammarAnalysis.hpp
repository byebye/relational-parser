#ifndef RELATIONAL_PARSER_PARSER_GRAMMARANALYSIS_HPP
#define RELATIONAL_PARSER_PARSER_GRAMMARANALYSIS_HPP

#include <queue>
#include <vector>

#include "CommonTypes.hpp"
#include "parser/Interfaces.hpp"
#include "parser/labels/ParsingSemiring.hpp"
#include "util/Container.hpp"

namespace relparser {

template<interface::Grammar G, interface::Semiring S>
class GrammarAnalysis {
 public:
  using Grammar = G;
  using Production = typename G::Production;
  using Symbol = typename G::Symbol;
  using Semiring = S;
  using Value = typename Semiring::Value;

  explicit GrammarAnalysis(Grammar grammar)
      : grammar_{std::move(grammar)}, nullableSymbols_{computeNullableSymbols(grammar_)} {
    allSymbols_.emplace(grammar_.start());
    for (Production const& production : grammar_.productions()) {
      allSymbols_.emplace(production.lhs());
      allSymbols_.insert(production.rhs().begin(), production.rhs().end());
      firstSymbols_[production.lhs()].emplace(production.lhs());
      firstFromSymbols_[production.lhs()].emplace(production.lhs());
      nonterminals_.emplace(production.lhs());
    }
    for (Symbol s : allSymbols_) {
      if (!nonterminals_.contains(s)) {
        terminals_.emplace(s);
      }
    }

    // for (auto&& [symbol, value] : nullableSymbols_) {
    //   fmt::print("------ Nullable: {} ------\n{}\n", symbol, stdutil::Printable{value});
    // }

    bool changed = false;
    auto addFirst = [&](Symbol s, Symbol first) {
      changed |= firstSymbols_[s].emplace(first).second;
      firstFromSymbols_[first].emplace(s);
    };
    do {
      changed = false;
      for (Production const& production : grammar_.productions()) {
        for (Symbol rhsSymbol : production.rhs()) {
          addFirst(production.lhs(), rhsSymbol);
          for (Symbol transitiveFirst : firstSymbols_[rhsSymbol]) {
            addFirst(production.lhs(), transitiveFirst);
          }
          if (!isNullable(rhsSymbol)) {
            break;
          }
        }
      }
    } while (changed);
    for (Symbol s : allSymbols_) {
      firstSymbols_[s].emplace(s);
      firstFromSymbols_[s].emplace(s);
    }
  }

  Grammar const& grammar() const {
    return grammar_;
  }

  Set<Symbol> const& allSymbols() const {
    return allSymbols_;
  }

  Set<Symbol> terminals() const {
    return terminals_;
  }

  bool isTerminal(Symbol s) const {
    return terminals_.contains(s);
  }

  bool isNullable(Symbol s) const {
    return nullableSymbols_.contains(s);
  }

  Value const& nullableValue(Symbol s) const {
    return nullableSymbols_.at(s);
  }

  bool isFirstDerivable(Symbol source, Symbol target) const {
    return firstSymbols_.at(source).contains(target);
  }

  Set<Symbol> const& firstDerivableSymbols(Symbol source) const {
    return firstSymbols_.at(source);
  }

  Set<Symbol> const& firstDerivableFromSymbols(Symbol source) const {
    return firstFromSymbols_.at(source);
  }

 private:
  Grammar grammar_;
  Set<Symbol> allSymbols_;
  Set<Symbol> nonterminals_;
  Set<Symbol> terminals_;
  Map<Symbol, Value> nullableSymbols_;
  Map<Symbol, Set<Symbol>> firstSymbols_;
  Map<Symbol, Set<Symbol>> firstFromSymbols_;

  // Computes nullable symbols of the given grammar. Each symbol is mapped to a *reversed* value
  // representing nulling derivations, transformed using the provided label engine.
  // The values are reversed for easier computations because reversing them later is not trivial,
  // besides, in order to obtain the final parsing value they need to be reversed anyway.
  static Map<Symbol, Value> computeNullableSymbols(G const& grammar) {
    Map<Symbol, Map<Production const*, std::size_t>> symbolOccurrences;
    Map<Symbol, Value> nullableSymbolValues;
    Set<Production const*> nullableProductions;
    Map<Production const*, std::size_t> productionNumSymbolsToNull;
    Map<Symbol, Set<Production const*>> lhsToNullableProductions;
    // Total number of lengths of nullable rhs'es of productions where the symbol is the lhs.
    // Used to compute nulling derivations in the topological order.
    Map<Symbol, std::size_t> lhsToRhsSymbolsToNull;
    Set<Symbol> nulledSymbols;
    std::queue<Symbol> nulledSymbolsToProcess;

    // First find all productions that are nullable. Count total number of symbols that need to
    // be nulled before processing a symbol, to guarantee topological order in the later phase
    // calculating nullable value per symbol.

    auto addNullableProduction = [&](Production const& production) {
      lhsToRhsSymbolsToNull[production.lhs()] += production.rhs().size();
      nullableProductions.emplace(&production);
      lhsToNullableProductions[production.lhs()].emplace(&production);
      if (nulledSymbols.emplace(production.lhs()).second) {
        nulledSymbolsToProcess.push(production.lhs());
      }
    };

    for (Production const& production : grammar.productions()) {
      productionNumSymbolsToNull.emplace(&production, production.rhs().size());
      if (production.rhs().empty()) {
        addNullableProduction(production);
      }
      for (Symbol s : production.rhs()) {
        symbolOccurrences[s][&production]++;
      }
    }

    // Find all nullable symbols while calculating number of rhs symbols in all nullable
    // productions.
    while (!nulledSymbolsToProcess.empty()) {
      Symbol s = nulledSymbolsToProcess.front();
      nulledSymbolsToProcess.pop();

      for (auto&& [production, occurrences] : symbolOccurrences[s]) {
        productionNumSymbolsToNull[production] -= occurrences;
        if (productionNumSymbolsToNull[production] == 0) {
          addNullableProduction(*production);
        }
      }
    }

    // Start with symbols having only single direct nulling production (S -> eps), then process the
    // remaining symbols in topological order, in order to compute the label values. If some of the
    // symbols are left unprocessed, then it means that there is a cycle in the nulling graph and
    // grammar is infinitely ambiguous, which is not supported.
    for (auto&& [symbol, symbolsToNull] : lhsToRhsSymbolsToNull) {
      if (symbolsToNull == 0) {
        nulledSymbolsToProcess.push(symbol);
      }
    }

    while (!nulledSymbolsToProcess.empty()) {
      Symbol s = nulledSymbolsToProcess.front();
      nulledSymbolsToProcess.pop();

      Value symbolValue = Semiring::zero();
      for (Production const* production : lhsToNullableProductions[s]) {
        Value productionValue = Semiring::value(*production);
        for (Symbol rhsSymbol : production->rhs()) {
          // Multiplying from left to get value in the reverse order, to avoid reversing the value
          // later, achieving better performance.
          // Computing value in the topological order ensures the value is in the map.
          productionValue =
              Semiring::multiply(nullableSymbolValues.at(rhsSymbol), std::move(productionValue));
        }
        symbolValue = Semiring::add(symbolValue, std::move(productionValue));
      }
      nullableSymbolValues.emplace(s, std::move(symbolValue));

      for (auto&& [production, occurrences] : symbolOccurrences[s]) {
        // Only completely nullable productions are considered.
        if (nullableProductions.contains(production)) {
          lhsToRhsSymbolsToNull[production->lhs()] -= occurrences;
          if (lhsToRhsSymbolsToNull[production->lhs()] == 0) {
            nulledSymbolsToProcess.push(production->lhs());
          }
        }
      }
    }

    for (auto&& [_, symbolsToNull] : lhsToRhsSymbolsToNull) {
      if (symbolsToNull != 0) {
        throw std::invalid_argument("Infinitely ambiguous grammar is not supported");
      }
    }

    return nullableSymbolValues;
  }
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_GRAMMARANALYSIS_HPP
