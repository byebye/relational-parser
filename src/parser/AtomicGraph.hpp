#ifndef RELATIONAL_PARSER_PARSER_ATOMICGRAPH_HPP
#define RELATIONAL_PARSER_PARSER_ATOMICGRAPH_HPP

#include <absl/algorithm/container.h>
#include <absl/strings/str_join.h>
#include <cstdint>
#include <queue>
#include <vector>

#include "CommonTypes.hpp"
#include "util/DotGraph.hpp"
#include "parser/Interfaces.hpp"
#include "parser/labels/LabelSubstitutionEngine.hpp"
#include "parser/labels/ParsingSemiring.hpp"
#include "util/Container.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<interface::GrammarAnalysis GA, interface::LabelEngine LE>
requires std::same_as<typename GA::Semiring, typename LE::Semiring>
class AtomicGraph {
 public:
  using GrammarAnalysis = GA;
  using Symbol = typename GA::Symbol;
  using LabelEngine = LE;
  using Semiring = typename LabelEngine::Semiring;
  using LabelValue = typename LabelEngine::Value;
  using Node = std::int32_t;
  using Production = typename GA::Grammar::Production;

  explicit AtomicGraph(GrammarAnalysis analysis, LabelEngine labelEngine)
      : grammarAnalysis_{std::move(analysis)}, labelEngine_{std::move(labelEngine)} {
    // Node ids:
    // - eps: 0
    // - symbol: 1, ..., |S|
    // - production position P(0,0), ..., P(n,|rhs_n|): |S|+1, |S|+2, ...
    std::size_t const numNodes = 1 + grammarAnalysis_.allSymbols().size()
                                 + absl::c_accumulate(grammarAnalysis_.grammar().productions(), 0,
                                                      [](std::size_t total, auto&& production) {
                                                        return total + production.rhs().size() + 1;
                                                      });
    graph_.resize(numNodes);
    reachableNodes_.resize(numNodes);
    reverseReachableNodes_.resize(numNodes);

    Node nodeId = 1;
    for (Symbol s : grammarAnalysis_.allSymbols()) {
      nodeIdToSymbol_.emplace(nodeId, s);
      symbolIds_.emplace(s, nodeId++);
    }

#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_TRACE
    std::fstream out = stdutil::openFileOrThrow("log/atomic.dot", std::ios_base::out);
    DotGraph dotGraph{out, DotGraph::Options{DotGraph::Directed, DotGraph::LeftToRight, true}};

    auto nodeOpts = DotGraph::NodeOptions{}.shape(DotGraph::Box);
    auto edgeOpts = DotGraph::EdgeOptions{}.fontName("Courier");

    Set<Node> nodes;
    auto dotNodeId = [&](Node s) { return fmt::format("node{}", s); };
    auto dotNode = [&](Node s, auto&& id) {
      if (nodes.emplace(s).second) {
        auto label = DotGraph::Label::Html("&Phi;")
                         .tag("SUB", fmt::format("{} [#{}]", stdutil::Printable{id}, s))
                         .label();
        dotGraph.node(dotNodeId(s), label, nodeOpts);
      }
    };
    auto dotEdge = [&](Node s, auto&& sid, Node t, auto&& tid, auto&& value) {
      dotNode(s, sid);
      dotNode(t, tid);
      if constexpr (std::same_as<typename LabelEngine::Semiring, ParsingSemiring<Production>>) {
        auto printDerivation = stdutil::recurse{
            [](auto&& print, typename Semiring::Value const& value) -> std::string {
              if (Semiring::isZero(value)) {
                return "<>";
              }
              if (Semiring::isOne(value)) {
                return "&epsilon;";
              }
              else if (auto node = std::get_if<typename Semiring::AddNode>(value.get())) {
                return "(" + print(node->lhs) + "|" + print(node->rhs) + ")";
              }
              else if (auto node = std::get_if<typename Semiring::MultiplyNode>(value.get())) {
                return print(node->lhs) + print(node->rhs);
              }
              else if (auto node = std::get_if<typename Semiring::ProductionNode>(value.get())) {
                return fmt::format("[{}]", *node->production);
              }
              else {
                throw std::invalid_argument(fmt::format(
                    "DerivationPrinter::print - invalid node type: {}", value->index()));
              }
            }};
        dotGraph.edge(
            dotNodeId(s), dotNodeId(t),
            fmt::format("({},{})", printDerivation(value.first), printDerivation(value.second)),
            edgeOpts);
      }
      else {
        dotGraph.edge(dotNodeId(s), dotNodeId(t), fmt::format("({},{})", value.first, value.second),
                      edgeOpts);
      }
    };
    auto prodPosLabel = [](Production const& p, int pos) {
      return fmt::format(
          "{}&rarr;{}", p.lhs(),
          (p.rhs().empty() ? "&epsilon;"
                           : absl::StrJoin(std::ranges::take_view{p.rhs(), pos - 1}, "∙") + "⚫"
                                 + absl::StrJoin(std::ranges::drop_view{p.rhs(), pos - 1}, "∙")));
    };
#endif

    for (auto&& production : grammarAnalysis_.grammar().productions()) {
      std::optional<typename Semiring::Value> prefixValue = Semiring::one();
      for (int pos = 0; Symbol rhsSymbol : production.rhs()) {
        nodeToProductionPosition_.emplace(nodeId, std::pair{&production, pos++});
        // v = rhsSymbol, nodeId = u->A*vB
        if (prefixValue.has_value()) {
          // v -- {eps} x E_rev(A) --> u->Av*B
          auto rhsNode = symbolIds_.at(rhsSymbol);
          graph_[rhsNode].emplace_back(nodeId + 1, labelEngine_.guiding(*prefixValue));
          STTRACE(dotEdge(rhsNode, rhsSymbol, nodeId + 1, prodPosLabel(production, pos + 1),
                          std::pair{Semiring::one(), prefixValue}));
        }
        if (grammarAnalysis_.isNullable(rhsSymbol)) {
          // u->A*vB -- E(v) x {eps} --> u->Av*B
          auto symbolValue = grammarAnalysis_.nullableValue(rhsSymbol);
          graph_[nodeId].emplace_back(nodeId + 1, labelEngine_.pending(symbolValue));
          STTRACE(dotEdge(nodeId, prodPosLabel(production, pos), nodeId + 1,
                          prodPosLabel(production, pos + 1),
                          std::pair{symbolValue, Semiring::one()}));
          // Multiplying from left to get value in the reverse order.
          if (prefixValue) {
            prefixValue = Semiring::multiply(std::move(symbolValue), std::move(*prefixValue));
          }
        }
        else {
          prefixValue = std::nullopt;
        }
        ++nodeId;
      }
      nodeToProductionPosition_.emplace(nodeId, std::pair{&production, production.rhs().size()});
      // u->A* -- {(eps, u->A)} --> u
      LabelValue value = labelEngine_.guiding(Semiring::value(production));
      auto lhsNode = symbolIds_.at(production.lhs());
      graph_[nodeId].emplace_back(lhsNode, value);
      STTRACE(dotEdge(nodeId, prodPosLabel(production, production.rhs().size()), lhsNode,
                      production.lhs(), std::pair{Semiring::one(), Semiring::value(production)}));
      ++nodeId;
    }

    // For each node in the graph compute the set of reachable nodes.
    auto computeReachableNodes = [numNodes](auto const& graph, auto& reachableNodes,
                                            auto&& nodeFromEdge) {
      for (auto start = 0; start < numNodes; ++start) {
        HashSet<Node> visited{start};
        std::queue<Node> q;
        for (q.push(start); !q.empty(); q.pop()) {
          auto s = q.front();
          reachableNodes[start].emplace(s);
          for (auto&& e : graph[s]) {
            auto&& t = nodeFromEdge(e);
            if (visited.emplace(t).second) {
              q.push(t);
            }
          }
        }
      }
    };
    computeReachableNodes(graph_, reachableNodes_, [](auto&& p) { return p.first; });

    std::vector<Set<Node>> reversedGraph(numNodes);
    for (auto start = 0; start < numNodes; ++start) {
      for (auto&& [t, _] : graph_[start]) {
        reversedGraph[t].emplace(start);
      }
    }
    computeReachableNodes(reversedGraph, reverseReachableNodes_, [](auto&& p) { return p; });
  }

  GrammarAnalysis const& grammarAnalysis() const {
    return grammarAnalysis_;
  }

  LabelEngine const& labelEngine() const {
    return labelEngine_;
  }

  Node symbolId(Symbol s) const {
    return symbolIds_.at(s);
  }

  Symbol nodeSymbol(Node s) const {
    return nodeIdToSymbol_.at(s);
  }

  Set<Node> const& reachableNodes(Node source) const {
    return reachableNodes_[source];
  }

  Set<Node> const& reverseReachableNodes(Node source) const {
    return reverseReachableNodes_[source];
  }

  bool isNodeReachable(Node source, Node target) const {
    return reachableNodes_[source].contains(target);
  }

  LabelValue computePathsValue(Node source, Node target) const {
    if (!isNodeReachable(source, target)) {
      throw std::logic_error("Unreachable node");
    }

    Map<Node, LabelValue> nodeValues;

    auto visit = stdutil::recurse{[&](auto&& visit, Node node) {
      if (node == target) {
        nodeValues.emplace(node, labelEngine_.one());
        return;
      }
      LabelValue nodeValue = labelEngine_.zero();
      for (auto&& [neighbor, edgeValue] : graph_[node]) {
        // Don't descent into subgraphs from which the target node is not reachable.
        if (!isNodeReachable(neighbor, target)) {
          continue;
        }
        if (!nodeValues.contains(neighbor)) {
          visit(neighbor);
        }
        nodeValue = labelEngine_.add(std::move(nodeValue),
                                     labelEngine_.multiply(edgeValue, nodeValues[neighbor]));
      }
      nodeValues.emplace(node, std::move(nodeValue));
    }};
    visit(source);
    return nodeValues.at(source);
  }

  std::pair<Production const*, int> const& nodeProductionPosition(Node node) const {
    return nodeToProductionPosition_.at(node);
  }

 private:
  GrammarAnalysis grammarAnalysis_;
  LabelEngine labelEngine_;
  std::vector<std::vector<std::pair<Node, LabelValue>>> graph_;
  std::vector<Set<Node>> reachableNodes_;
  std::vector<Set<Node>> reverseReachableNodes_;
  Map<Symbol, Node> symbolIds_;
  Map<Node, Symbol> nodeIdToSymbol_;
  Map<Node, std::pair<Production const*, int>> nodeToProductionPosition_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_ATOMICGRAPH_HPP
