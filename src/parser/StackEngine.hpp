#ifndef RELATIONAL_PARSER_PARSER_STACKENGINE_HPP
#define RELATIONAL_PARSER_PARSER_STACKENGINE_HPP

#include <absl/container/node_hash_map.h>
#include <absl/container/node_hash_set.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <deque>
#include <iterator>
#include <memory>
#include <ostream>
#include <vector>

#include "CommonTypes.hpp"
#include "parser/Interfaces.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<interface::FactorizableRelationEngine RE>
class StackEngine {
 public:
  using BaseEngine = RE;
  using Symbol = typename BaseEngine::Symbol;
  using AtomicEngine = typename BaseEngine::AtomicEngine;
  using GrammarAnalysis = typename BaseEngine::GrammarAnalysis;
  using Atomic = typename BaseEngine::Atomic;

  using LabelEngine = typename BaseEngine::LabelEngine;
  using EpsilonValue = typename LabelEngine::RelationNode;

  using BaseRelation = typename BaseEngine::Relation;

  struct Relation : public std::vector<BaseRelation> {
    BaseRelation pop() {
      auto r = std::vector<BaseRelation>::back();
      std::vector<BaseRelation>::pop_back();
      return r;
    }
    BaseRelation top() const {
      return std::vector<BaseRelation>::back();
    }
    void push(BaseRelation r) {
      std::vector<BaseRelation>::emplace_back(r);
    }
  };

  explicit StackEngine(BaseEngine baseEngine) : baseEngine_{std::move(baseEngine)} {
  }

  StackEngine(StackEngine const&) = delete;
  StackEngine(StackEngine&&) noexcept = default;

  AtomicEngine const& atomicEngine() const {
    return baseEngine_.atomicEngine();
  }

  LabelEngine const& labelEngine() const {
    return atomicEngine().atomicGraph().labelEngine();
  }

  Relation init() {
    Relation r;
    r.push(baseEngine_.init());
    return r;
  }

  bool isEmpty(Relation const& source) const {
    return source.empty();
  }

  bool isFinal(Relation const& source) const {
    return source.size() == 1 && baseEngine_.isFinal(source.top());
  }

  Relation computePhase(Relation source, Symbol token) {
    STDEBUG(TIMER("StackEngine::computePhase"));
    STINFO(COUNTER("StackEngine::computePhase"));
    if (isEmpty(source)) {
      return source;
    }
    auto topNode = source.pop();
    std::pair memoizeKey{topNode, token};
    auto it = factorsCache_.find(memoizeKey);
    if (it == factorsCache_.end()) {
      BaseRelation baseNode = baseEngine_.computePhase(topNode, token);
      it = factorsCache_.emplace_hint(it, memoizeKey, baseEngine_.factorize(baseNode));
    }
    else {
      STINFO(COUNTER("StackEngine::computePhase - memoized"));
    }
    auto const& factors = it->second;
    if (factors.empty()) {
      source.clear();
      return source;
    }
    // Only the first (in reverse) factor can possibly be final.
    bool concat = !source.empty() && baseEngine_.isFinal(factors.front());
    // Push the node at the stack keeping the invariant that only the bottom node can be final.
    source.push(concat ? baseEngine_.concat(factors.front(), source.pop()) : factors.front());
    std::ranges::copy(std::ranges::drop_view{factors, 1}, std::back_inserter(source));
    return source;
  }

  std::optional<EpsilonValue> epsilon(Relation const& source) const {
    STDEBUG(TIMER("StackEngine::epsilon"));
    if (!isFinal(source)) {
      return std::nullopt;
    }
    auto const& le = baseEngine_.labelEngine();
    return le.evaluate(le.flatten(*source.top()->epsilon()));
  }

  void reset() {
    SPDLOG_TRACE("StackEngine::reset");
    factorsCache_.clear();
  }

 private:
  BaseEngine baseEngine_;
  HashMap<std::pair<BaseRelation, Symbol>, std::vector<BaseRelation>> factorsCache_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_STACKENGINE_HPP
