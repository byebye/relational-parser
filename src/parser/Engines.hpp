#ifndef RELATIONAL_PARSER_PARSER_PARSER_ENGINES_HPP
#define RELATIONAL_PARSER_PARSER_PARSER_ENGINES_HPP

#include "parser/AtomicEngine.hpp"
#include "parser/AtomicGraph.hpp"
#include "parser/Grammar.hpp"
#include "parser/GrammarAnalysis.hpp"
#include "parser/Interfaces.hpp"
#include "parser/OptimizedAtomicEngine.hpp"
#include "parser/RelationEngine.hpp"
#include "parser/StackEngine.hpp"
#include "parser/SubstitutionEngine.hpp"
#include "parser/labels/CountingSemiring.hpp"
#include "parser/labels/LabelSubstitutionEngine.hpp"
#include "parser/labels/LabelValueEngine.hpp"
#include "parser/labels/ParsingSemiring.hpp"
#include "parser/labels/RecognitionSemiring.hpp"

namespace relparser {

using Symbol = int;
using Production = typename Grammar<Symbol>::Production;

namespace detail {

template<interface::Semiring S>
using MakeGrammarAnalysis = GrammarAnalysis<Grammar<Symbol>, S>;

template<interface::LabelEngine LE>
using MakeAtomicEngine = AtomicEngine<AtomicGraph<MakeGrammarAnalysis<typename LE::Semiring>, LE>>;

template<interface::LabelEngine LE, AtomicOptimization opt>
using MakeOptAtomicEngine =
    OptimizedAtomicEngine<AtomicGraph<MakeGrammarAnalysis<typename LE::Semiring>, LE>, opt>;

using RS = RecognitionSemiring<Production>;
using RecognitionEngine = MakeAtomicEngine<LabelValueEngine<RS>>;
using RecognitionOptEngine = MakeOptAtomicEngine<LabelValueEngine<RS>, AtomicOptimization::All>;
using RecognitionSubstEngine = MakeAtomicEngine<LabelSubstitutionEngine<RS>>;
using RecognitionSubstOptEngine =
    MakeOptAtomicEngine<LabelSubstitutionEngine<RS>, AtomicOptimization::All>;

using CS = CountingSemiring<Production>;
using CountingEngine = MakeAtomicEngine<LabelValueEngine<CS>>;
using CountingOptEngine = MakeOptAtomicEngine<LabelValueEngine<CS>, AtomicOptimization::Forward>;
using CountingSubstEngine = MakeAtomicEngine<LabelSubstitutionEngine<CS>>;
using CountingSubstOptEngine =
    MakeOptAtomicEngine<LabelSubstitutionEngine<CS>, AtomicOptimization::Forward>;

using PS = ParsingSemiring<Production>;
using PE = AdditionRhsIgnorantLabelEvaluator<RelationValue<PS>>;
using ParsingEngine = MakeAtomicEngine<LabelDagEngine<PS, PE>>;
using ParsingOptEngine = MakeOptAtomicEngine<LabelDagEngine<PS, PE>, AtomicOptimization::Backward>;
using ParsingSubstEngine = MakeAtomicEngine<LabelSubstitutionEngine<PS, PE>>;
using ParsingSubstOptEngine =
    MakeOptAtomicEngine<LabelSubstitutionEngine<PS, PE>, AtomicOptimization::Backward>;

using PF = RelationValue<ParsingSemiring<Production>>;
using ParseForestEngine = MakeAtomicEngine<LabelIdentityDagEngine<PF>>;
using ParseForestOptEngine =
    MakeOptAtomicEngine<LabelIdentityDagEngine<PF>, AtomicOptimization::Backward>;
using ParseForestSubstEngine = MakeAtomicEngine<LabelIdentitySubstitutionEngine<PF>>;
using ParseForestSubstOptEngine =
    MakeOptAtomicEngine<LabelIdentitySubstitutionEngine<PF>, AtomicOptimization::Backward>;

template<interface::AtomicEngine AE>
struct RelationEngineProvider {
  using AtomicEngine = AE;
  using LabelEngine = typename AE::LabelEngine;
  using AtomicGraph = typename AE::AtomicGraph;
  using Semiring = typename LabelEngine::Semiring;
  using RelationEngine = CacheableRelationEngine<AE>;

  CacheableRelationEngine<AE> operator()(Grammar<Symbol> grammar) {
    MakeGrammarAnalysis<Semiring> analysis{std::move(grammar)};
    AtomicGraph atomicGraph{std::move(analysis), LabelEngine{}};
    AtomicEngine atomicEngine{std::move(atomicGraph)};
    return CacheableRelationEngine<AE>{std::move(atomicEngine)};
  }
};

template<interface::AtomicEngine AE>
struct StackEngineProvider {
  using AtomicEngine = AE;
  using LabelEngine = typename AE::LabelEngine;
  using AtomicGraph = typename AE::AtomicGraph;
  using Semiring = typename LabelEngine::Semiring;
  using RelationEngine = StackEngine<FactorizableRelationEngine<AE>>;

  StackEngine<FactorizableRelationEngine<AE>> operator()(Grammar<Symbol> grammar) {
    MakeGrammarAnalysis<Semiring> analysis{std::move(grammar)};
    AtomicGraph atomicGraph{std::move(analysis), LabelEngine{}};
    AtomicEngine atomicEngine{std::move(atomicGraph)};
    return StackEngine<FactorizableRelationEngine<AE>>{
        FactorizableRelationEngine<AE>{std::move(atomicEngine)}};
  }
};

template<interface::AtomicEngine AE>
struct SubstitutionEngineProvider {
  using AtomicEngine = AE;
  using LabelEngine = typename AE::LabelEngine;
  using AtomicGraph = typename AE::AtomicGraph;
  using Semiring = typename LabelEngine::Semiring;
  using SubstitutionContext = SimpleSubstitutionContext<typename LabelEngine::Value>;
  using RelationEngine = SubstitutionEngine<SplittableRelationEngine<AE>, SubstitutionContext>;

  SubstitutionEngine<SplittableRelationEngine<AE>, SubstitutionContext> operator()(
      Grammar<Symbol> grammar) {
    MakeGrammarAnalysis<Semiring> analysis{std::move(grammar)};
    AtomicGraph atomicGraph{std::move(analysis), LabelEngine{}};
    AtomicEngine atomicEngine{std::move(atomicGraph)};
    return SubstitutionEngine<SplittableRelationEngine<AE>, SubstitutionContext>{
        SplittableRelationEngine<AE>{std::move(atomicEngine)}};
  }
};

}  // namespace detail

using RecognitionDagEngine = detail::RelationEngineProvider<detail::RecognitionEngine>;
using RecognitionDagEngineOpt = detail::RelationEngineProvider<detail::RecognitionOptEngine>;
using RecognitionStackEngine = detail::StackEngineProvider<detail::RecognitionEngine>;
using RecognitionStackEngineOpt = detail::StackEngineProvider<detail::RecognitionOptEngine>;
using RecognitionSubstEngine = detail::SubstitutionEngineProvider<detail::RecognitionSubstEngine>;
using RecognitionSubstEngineOpt =
    detail::SubstitutionEngineProvider<detail::RecognitionSubstOptEngine>;

using CountingDagEngine = detail::RelationEngineProvider<detail::CountingEngine>;
using CountingDagEngineOpt = detail::RelationEngineProvider<detail::CountingOptEngine>;
using CountingStackEngine = detail::StackEngineProvider<detail::CountingEngine>;
using CountingStackEngineOpt = detail::StackEngineProvider<detail::CountingOptEngine>;
using CountingSubstEngine = detail::SubstitutionEngineProvider<detail::CountingSubstEngine>;
using CountingSubstEngineOpt = detail::SubstitutionEngineProvider<detail::CountingSubstOptEngine>;

using ParsingDagEngine = detail::RelationEngineProvider<detail::ParsingEngine>;
using ParsingDagEngineOpt = detail::RelationEngineProvider<detail::ParsingOptEngine>;
using ParsingStackEngine = detail::StackEngineProvider<detail::ParsingEngine>;
using ParsingStackEngineOpt = detail::StackEngineProvider<detail::ParsingOptEngine>;
using ParsingSubstEngine = detail::SubstitutionEngineProvider<detail::ParsingSubstEngine>;
using ParsingSubstEngineOpt = detail::SubstitutionEngineProvider<detail::ParsingSubstOptEngine>;

using ForestDagEngine = detail::RelationEngineProvider<detail::ParseForestEngine>;
using ForestDagEngineOpt = detail::RelationEngineProvider<detail::ParseForestOptEngine>;
using ForestStackEngine = detail::StackEngineProvider<detail::ParseForestEngine>;
using ForestStackEngineOpt = detail::StackEngineProvider<detail::ParseForestOptEngine>;
using ForestSubstEngine = detail::SubstitutionEngineProvider<detail::ParseForestSubstEngine>;
using ForestSubstEngineOpt = detail::SubstitutionEngineProvider<detail::ParseForestSubstOptEngine>;

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_PARSER_ENGINES_HPP
