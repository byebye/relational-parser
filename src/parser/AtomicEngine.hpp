#ifndef RELATIONAL_PARSER_PARSER_ATOMICENGINE_HPP
#define RELATIONAL_PARSER_PARSER_ATOMICENGINE_HPP

#include <absl/container/node_hash_map.h>
#include <absl/strings/str_join.h>
#include <spdlog/spdlog.h>
#include <queue>
#include <util/Printing.hpp>
#include <vector>

#include "CommonTypes.hpp"
#include "parser/Interfaces.hpp"
#include "parser/labels/ParsingSemiring.hpp"
#include "util/Container.hpp"

namespace relparser {

template<interface::AtomicGraph AG>
class AtomicEngine {
 public:
  using AtomicGraph = AG;
  using GrammarAnalysis = typename AG::GrammarAnalysis;
  using Symbol = typename AG::Symbol;
  using LabelEngine = typename AG::LabelEngine;
  using LabelValue = typename LabelEngine::Value;
  using Node = typename AtomicGraph::Node;
  using Atomic = std::pair<Node, Node>;
  using Production = typename GrammarAnalysis::Grammar::Production;

  explicit AtomicEngine(AtomicGraph atomicGraph) : atomicGraph_{std::move(atomicGraph)} {
    auto addEpsilon = [this](Atomic atomic) {
      if (atomicGraph_.isNodeReachable(atomic.second, atomic.first)) {
        atomicEpsilon_.emplace(atomic, atomicGraph_.computePathsValue(atomic.second, atomic.first));
      }
    };
    auto start = grammarAnalysis().grammar().start();
    Node startNode = atomicGraph_.symbolId(start);

    // Special value atomic representing (eps, eps).
    auto const& le = atomicGraph_.labelEngine();
    Atomic const epsilonAtomic{-1, -1};
    atomicEpsilon_.emplace(epsilonAtomic, le.one());

    // root = nulling({(start, eps)}), so only derivative by start exists.
    Atomic const startAtomic{startNode, 0};
    atomicDerivatives_[startAtomic].emplace_back(start, epsilonAtomic,
                                                 atomicGraph_.labelEngine().one());
    if (grammarAnalysis().isNullable(start)) {
      atomicEpsilon_.emplace(startAtomic, le.guiding(grammarAnalysis().nullableValue(start)));
    }

    // Node ids (in atomic graph):
    // - eps: 0
    // - symbol: 1, ..., |S|
    // - production position P(0,0), ..., P(n,|rhs_n|): |S|+1, |S|+2, ...
    for (Symbol s : grammarAnalysis().allSymbols()) {
      for (Symbol t : grammarAnalysis().terminals()) {
        Node source = atomicGraph_.symbolId(s);
        Node target = atomicGraph_.symbolId(t);
        addEpsilon({source, target});
      }
    }
    Node derivativeByNode = grammarAnalysis().allSymbols().size() + 1;
    for (Production const& production : grammarAnalysis().grammar().productions()) {
      for (Symbol derivativeBy : production.rhs()) {
        for (Symbol s : grammarAnalysis().firstDerivableFromSymbols(production.lhs())) {
          Node source = atomicGraph_.symbolId(s);
          addEpsilon({source, derivativeByNode + 1});
          for (Node target : atomicGraph_.reverseReachableNodes(derivativeByNode)) {
            atomicDerivatives_[{source, target}].emplace_back(
                derivativeBy, std::pair{source, derivativeByNode + 1},
                atomicGraph_.computePathsValue(target, derivativeByNode));
            addEpsilon({source, target});
          }
        }
        ++derivativeByNode;
      }
      ++derivativeByNode;
    }

    SPDLOG_INFO("Derivatives map size: {}", atomicDerivatives_.size());
    // if constexpr (!std::same_as<typename AtomicGraph::Semiring, ParsingSemiring<Production>>) {
    //   auto printNode = [numSymbols = grammarAnalysis().allSymbols().size() + 1,
    //                     this](Node node) -> std::string {
    //     if (node == 0)
    //       return "ϵ";
    //     if (node < numSymbols)
    //       return fmt::format("{}", atomicGraph_.nodeSymbol(node));
    //     auto const& [p, pos] = atomicGraph_.nodeProductionPosition(node);
    //     return fmt::format(
    //         "{}->{}", p->lhs(),
    //         (p->rhs().empty() ? "ϵ"
    //                           : absl::StrJoin(std::ranges::take_view{p->rhs(), pos}, "∙") + "⚫"
    //                                 + absl::StrJoin(std::ranges::drop_view{p->rhs(), pos},
    //                                 "∙")));
    //   };
    //
    //   auto printAtomic = [&](Atomic atomic) {
    //     return fmt::format("{},{}", printNode(atomic.first), printNode(atomic.second));
    //   };
    //
    //   for (auto const& [atomic, derivatives] : atomicDerivatives_) {
    //     fmt::print("Atomic derivatives: {}\n", printAtomic(atomic));
    //     for (auto const& [bySymbol, symbolDerivatives] : derivatives) {
    //       fmt::print("  - By symbol: {}\n", bySymbol);
    //       for (auto const& [atomic, value] : symbolDerivatives) {
    //         fmt::print("   -- label: {}, atomic: {}\n", value, printAtomic(atomic));
    //       }
    //     }
    //   }
    // }
  }

  GrammarAnalysis const& grammarAnalysis() const {
    return atomicGraph_.grammarAnalysis();
  }

  AtomicGraph const& atomicGraph() const {
    return atomicGraph_;
  }

  // Atomic that represents {(start, eps)}.
  Atomic root() const {
    return {atomicGraph_.symbolId(grammarAnalysis().grammar().start()), 0};
  }

  Atomic atomic(Symbol s, Symbol t) const {
    return {atomicGraph_.symbolId(s), atomicGraph_.symbolId(t)};
  }

  // Returns true if the atomic is an empty set.
  // [(s, eps)]^(t) = {}
  bool isEmpty(Symbol s, Symbol t) const {
    return !grammarAnalysis().isFirstDerivable(s, t);
  }

  bool containsEpsilon(Atomic atomic) const {
    return atomicEpsilon_.contains(atomic);
  }

  LabelValue extractEpsilon(Atomic atomic) const {
    return atomicEpsilon_.at(atomic);
  }

  bool hasDerivatives(Atomic atomic) const {
    return atomicDerivatives_.contains(atomic);
  }

  std::vector<std::tuple<Symbol, Atomic, LabelValue>> const& allDerivatives(Atomic atomic) const {
    return atomicDerivatives_.at(atomic);
  }

 private:
  AtomicGraph atomicGraph_;
  Map<Atomic, std::vector<std::tuple<Symbol, Atomic, LabelValue>>> atomicDerivatives_;
  Map<Atomic, LabelValue> atomicEpsilon_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_ATOMICENGINE_HPP
