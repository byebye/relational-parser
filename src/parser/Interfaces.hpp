#ifndef RELATIONAL_PARSER_PARSER_INTERFACES_HPP
#define RELATIONAL_PARSER_PARSER_INTERFACES_HPP

#include <concepts>
#include <ranges>
#include <span>

#include "CommonTypes.hpp"
#include "util/Concepts.hpp"

namespace relparser::interface {

template<typename S>
concept Symbol = Hashable<S>;

template<typename P>
concept Production = requires(P const& p) {
  requires Symbol<typename P::Symbol>;
  { p.lhs() } -> std::same_as<typename P::Symbol>;
  { p.rhs() } -> RangeOf<typename P::Symbol>;
  { p.label() } -> std::convertible_to<std::string_view>;
};

struct FakeProduction {
  using Symbol = int;
  int lhs() const;
  std::span<int> rhs() const;
  std::string label() const;
};
static_assert(Production<FakeProduction>);

template<typename G>
concept Grammar = requires(G const& g) {
  requires Symbol<typename G::Symbol>;
  requires Production<typename G::Production>;
  { g.start() } -> std::same_as<typename G::Symbol>;
  { g.productions() } -> RangeOf<typename G::Production>;
};

template<typename S>
concept Semiring = requires(S const& s, typename S::Value const& v,
                            typename S::Production const& p) {
  typename S::Value;
  requires Production<typename S::Production>;
  { S::isCommutative } -> std::convertible_to<bool>;
  { S::value(p) } -> SameAsNoCvref<typename S::Value>;
  { S::zero() } -> SameAsNoCvref<typename S::Value>;
  { S::one() } -> SameAsNoCvref<typename S::Value>;
  { S::add(v, v) } -> SameAsNoCvref<typename S::Value>;
  { S::multiply(v, v) } -> SameAsNoCvref<typename S::Value>;
  { S::isZero(v) } -> std::same_as<bool>;
  { S::isOne(v) } -> std::same_as<bool>;
};

template<typename R>
concept Relation = requires(R const& r) {
  typename R::Semiring;
  { r.pending } -> std::same_as<typename R::Semiring::Value const&>;
  { r.guiding } -> std::same_as<typename R::Semiring::Value const&>;
};

template<typename LE>
concept LabelEvaluator = requires(LE const& le, typename LE::Value const& r) {
  typename LE::Value;
  typename LE::Semiring;
  typename LE::RelationOperand;
  { LE::add(r, r) } -> std::same_as<typename LE::Value>;
  { LE::multiply(r, r) } -> std::same_as<typename LE::Value>;
  { LE::flatten(r) } -> std::same_as<typename LE::Value>;
};

template<typename LE>
concept LabelEngine = requires(LE le, typename LE::Value v, typename LE::Semiring::Value s) {
  typename LE::Value;
  requires Semiring<typename LE::Semiring>;
  // requires LE::Semiring::isCommutative || LabelEvaluator<typename LE::LabelEvaluator>;
  typename LE::RelationValue;
  { le.zero() } -> SameAsNoCvref<typename LE::Value>;
  { le.one() } -> SameAsNoCvref<typename LE::Value>;
  { le.add(v, v) } -> SameAsNoCvref<typename LE::Value>;
  { le.multiply(v, v) } -> SameAsNoCvref<typename LE::Value>;
  { le.guiding(s) } -> SameAsNoCvref<typename LE::Value>;
  { le.pending(s) } -> SameAsNoCvref<typename LE::Value>;
  { le.isZero(v) } -> std::same_as<bool>;
  { le.isOne(v) } -> std::same_as<bool>;
};

template<typename LE>
concept LabelDagEngine = LabelEngine<LE> && requires(LE le, typename LE::Value v) {
  { le.evaluate(v) } -> SameAsNoCvref<typename LE::RelationNode>;
};

template<typename SC>
concept SubstitutionContext = requires(SC const& sc, std::size_t index, bool primary) {
  typename SC::LabelValue;
  { sc.variableValue(index) } -> SameAsNoCvref<typename SC::LabelValue const*>;
  { sc.parentValue(index, primary) }
    -> std::same_as<std::pair<typename SC::LabelValue const*, SC const*>>;
  { sc.parentPrimary() } -> std::same_as<SC const*>;
  { sc.parentSecondary() } -> std::same_as<SC const*>;
};

struct FakeSubstitutionContext {
  using LabelValue = int;
  int const* variableValue(std::size_t) const;
  std::pair<int const*, FakeSubstitutionContext const*> parentValue(std::size_t, bool) const;
  FakeSubstitutionContext const* parentPrimary() const;
  FakeSubstitutionContext const* parentSecondary() const;
};
static_assert(SubstitutionContext<FakeSubstitutionContext>);

template<typename LE>
concept LabelSubstitutionEngine = LabelEngine<LE> && requires(LE le, typename LE::Value v, int i,
                                                              FakeSubstitutionContext const sc) {
  { le.variable(i) } -> SameAsNoCvref<typename LE::Value>;
  { le.shiftVariablesContext(v) } -> SameAsNoCvref<typename LE::Value>;
  { le.evaluate(v, &sc) } -> SameAsNoCvref<typename LE::RelationNode>;
};

template<typename GA>
concept GrammarAnalysis = requires(GA const& ga, typename GA::Symbol s) {
  requires Grammar<typename GA::Grammar>;
  requires Symbol<typename GA::Symbol>;
  requires Semiring<typename GA::Semiring>;
  { ga.grammar() } -> SameAsNoCvref<typename GA::Grammar>;
  { ga.allSymbols() } -> RangeOf<typename GA::Symbol>;
  { ga.isNullable(s) } -> std::same_as<bool>;
  { ga.nullableValue(s) } -> SameAsNoCvref<typename GA::Semiring::Value>;
  { ga.isFirstDerivable(s, s) } -> std::same_as<bool>;
  { ga.isTerminal(s) } -> std::same_as<bool>;
};

template<typename AG>
concept AtomicGraph = requires(AG const& ag, typename AG::Node node, typename AG::Symbol s) {
  requires GrammarAnalysis<typename AG::GrammarAnalysis>;
  requires LabelEngine<typename AG::LabelEngine>;
  requires Symbol<typename AG::Symbol>;
  typename AG::Node;

  { ag.reachableNodes(node) } -> RangeOf<typename AG::Node>;
  { ag.reverseReachableNodes(node) } -> RangeOf<typename AG::Node>;
  { ag.isNodeReachable(node, node) } -> std::same_as<bool>;
};

template<typename A>
concept Atomic = true;

template<typename AE>
concept AtomicEngine = requires(AE const& ae, typename AE::Atomic atomic, typename AE::Symbol s) {
  requires GrammarAnalysis<typename AE::GrammarAnalysis>;
  requires LabelEngine<typename AE::LabelEngine>;
  requires AtomicGraph<typename AE::AtomicGraph>;
  requires Symbol<typename AE::Symbol>;
  requires Atomic<typename AE::Atomic>;
  requires std::is_same_v<typename AE::LabelValue, typename AE::LabelEngine::Value>;

  { ae.root() } -> std::same_as<typename AE::Atomic>;
  { ae.atomic(s, s) } -> std::same_as<typename AE::Atomic>;
  { ae.hasDerivatives(atomic) } -> std::same_as<bool>;
  { ae.containsEpsilon(atomic) } -> std::same_as<bool>;
  { ae.isEmpty(s, s) } -> std::same_as<bool>;
  { ae.hasDerivatives(atomic) } -> std::same_as<bool>;
  {
    ae.allDerivatives(atomic)
    } -> RangeOf<std::tuple<typename AE::Symbol, typename AE::Atomic, typename AE::LabelValue>>;
};

template<typename RE>
concept RelationEngine = requires(RE re, typename RE::Relation rel, typename RE::Symbol s) {
  requires GrammarAnalysis<typename RE::GrammarAnalysis>;
  requires LabelEngine<typename RE::LabelEngine>;
  requires AtomicEngine<typename RE::AtomicEngine>;
  requires Symbol<typename RE::Symbol>;
  requires Atomic<typename RE::Atomic>;
  typename RE::Relation;
  typename RE::EpsilonValue;

  { re.init() } -> std::same_as<typename RE::Relation>;
  // { re.root() } -> std::same_as<typename RE::Relation>;
  // { re.isRoot(rel) } -> std::same_as<bool>;
  // { re.empty() } -> std::same_as<typename RE::Relation>;
  { re.isEmpty(rel) } -> std::same_as<bool>;
  { re.isFinal(rel) } -> std::same_as<bool>;
  { re.computePhase(rel, s) } -> std::same_as<typename RE::Relation>;
  { re.epsilon(rel) } -> std::same_as<std::optional<typename RE::EpsilonValue>>;
  { re.atomicEngine() } -> std::same_as<typename RE::AtomicEngine const&>;
  { re.labelEngine() } -> std::same_as<typename RE::LabelEngine const&>;
};

template<typename RE>
concept FactorizableRelationEngine = RelationEngine<RE> && requires(RE re,
                                                                    typename RE::Relation rel) {
  { re.concat(rel, rel) } -> std::same_as<typename RE::Relation>;
  { re.factorize(rel) } -> RangeOf<typename RE::Relation>;
};

template<typename RE>
concept SplittableRelationEngine = RelationEngine<RE> && requires(RE re,
                                                                  typename RE::Relation rel) {
  typename RE::SplitResult;

  { re.split(rel) } -> std::same_as<typename RE::SplitResult>;
  { re.concat(rel, rel) } -> std::same_as<typename RE::SplitResult>;
  { re.factorize(rel) } -> RangeOf<typename RE::SplitResult>;
};

template<typename P>
concept Parser = requires(P p, std::vector<typename P::Symbol> symbols, typename P::Relation r) {
  requires Symbol<typename P::Symbol>;
  requires AtomicEngine<typename P::AtomicEngine>;
  requires RelationEngine<typename P::RelationEngine>;
  requires GrammarAnalysis<typename P::GrammarAnalysis>;
  requires LabelEngine<typename P::LabelEngine>;
  requires std::is_same_v<typename P::Relation, typename P::RelationEngine::Relation>;
  requires std::is_same_v<typename P::EpsilonValue, typename P::LabelEngine::RelationNode>;

  { p.parse(symbols) } -> std::same_as<typename P::Relation>;
  { p.relationEngine() } -> std::same_as<typename P::RelationEngine const&>;
  { p.epsilon(r) } -> std::same_as<std::optional<typename P::EpsilonValue>>;
};

}  // namespace relparser::interface

#endif  // RELATIONAL_PARSER_PARSER_INTERFACES_HPP
