#ifndef RELATIONAL_PARSER_PARSER_SUBSTITUTIONENGINE_HPP
#define RELATIONAL_PARSER_PARSER_SUBSTITUTIONENGINE_HPP

#include <absl/container/node_hash_map.h>
#include <absl/container/node_hash_set.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <deque>
#include <iterator>
#include <memory>
#include <ostream>
#include <vector>

#include "CommonTypes.hpp"
#include "parser/Interfaces.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<typename LV>
class SimpleSubstitutionContext {
 public:
  using LabelValue = LV;
  using Values = std::shared_ptr<std::vector<LabelValue>>;

  SimpleSubstitutionContext(std::shared_ptr<std::vector<LabelValue>> values,
                            SimpleSubstitutionContext const* parentPrimary = nullptr,
                            SimpleSubstitutionContext const* parentSecondary = nullptr) noexcept
      : values_{std::move(values)},
        parentPrimary_{parentPrimary},
        parentSecondary_{parentSecondary} {
  }

  LabelValue const* variableValue(std::size_t index) const {
    return &(*values_)[index];
  }

  std::pair<LabelValue const*, SimpleSubstitutionContext const*> parentValue(std::size_t index,
                                                                             bool primary) const {
    auto parent = (primary ? parentPrimary_ : parentSecondary_);
    return {parent->variableValue(index), parent};
  }

  SimpleSubstitutionContext const* parentPrimary() const {
    return parentPrimary_;
  };

  SimpleSubstitutionContext const* parentSecondary() const {
    return parentSecondary_;
  };

 private:
  SimpleSubstitutionContext const* parentPrimary_;
  SimpleSubstitutionContext const* parentSecondary_;
  std::shared_ptr<std::vector<LabelValue>> values_;
};
static_assert(interface::SubstitutionContext<SimpleSubstitutionContext<int>>);

template<interface::SplittableRelationEngine RE, interface::SubstitutionContext SC>
class SubstitutionEngine {
 public:
  using RelationEngine = RE;
  using SubstitutionContext = SC;
  using Symbol = typename RelationEngine::Symbol;
  using AtomicEngine = typename RelationEngine::AtomicEngine;
  using GrammarAnalysis = typename RelationEngine::GrammarAnalysis;
  using Atomic = typename RelationEngine::Atomic;

  using LabelEngine = typename RelationEngine::LabelEngine;
  using EpsilonValue = typename LabelEngine::RelationNode;

  using BaseRelation = typename RelationEngine::Relation;
  using SubstitutionValues = typename SubstitutionContext::Values;

  struct StackNode {
    BaseRelation shape;
    SubstitutionContext const* context;
    StackNode(BaseRelation shape, SubstitutionContext const* context)
        : shape{shape}, context{context} {
    }
  };

  struct Relation : public std::vector<StackNode> {
    using Base = std::vector<StackNode>;
    StackNode pop() {
      auto r = std::move(Base::back());
      Base::pop_back();
      return r;
    }
    StackNode const& top() const {
      return Base::back();
    }
    void push(BaseRelation r, SubstitutionContext c) {
      contexts.emplace_back(std::move(c));
      Base::emplace_back(std::move(r), &contexts.back());
    }
    SubstitutionContext const* pushContext(SubstitutionContext c) {
      contexts.emplace_back(std::move(c));
      return &contexts.back();
    }
    // Use deque for context pointer stability.
    std::deque<SubstitutionContext> contexts;
  };

  explicit SubstitutionEngine(RelationEngine baseEngine) : baseEngine_{std::move(baseEngine)} {
  }

  SubstitutionEngine(SubstitutionEngine const&) = delete;
  SubstitutionEngine(SubstitutionEngine&&) noexcept = default;

  AtomicEngine const& atomicEngine() const {
    return baseEngine_.atomicEngine();
  }

  LabelEngine const& labelEngine() const {
    return atomicEngine().atomicGraph().labelEngine();
  }

  Relation init() {
    Relation r;
    auto&& [shape, values] = baseEngine_.split(baseEngine_.init());
    r.push(shape, {values});
    return r;
  }

  bool isEmpty(Relation const& source) const {
    return source.empty();
  }

  bool isFinal(Relation const& source) const {
    return source.size() == 1 && baseEngine_.isFinal(source.top().shape);
  }

  Relation computePhase(Relation source, Symbol token) {
    STDEBUG(TIMER("SubstitutionEngine::computePhase"));
    STINFO(COUNTER("SubstitutionEngine::computePhase"));
    if (isEmpty(source)) {
      return source;
    }

    auto [topShape, topContext] = source.pop();
    std::pair memoizeKey{topShape, token};
    auto it = factorsCache_.find(memoizeKey);
    if (it == factorsCache_.end()) {
      BaseRelation baseNode = baseEngine_.computePhase(topShape, token);
      it = factorsCache_.emplace_hint(it, memoizeKey, baseEngine_.factorize(baseNode));
    }
    else {
      STINFO(COUNTER("SubstitutionEngine::computePhase - memoized"));
    }
    auto const& factors = it->second;
    if (factors.empty()) {
      source.clear();
      return source;
    }
    // Only the first (in reverse) factor can possibly be final.
    auto&& [frontShape, frontValues] = factors.front();
    if (!source.empty() && baseEngine_.isFinal(frontShape)) {
      // Push the node at the stack keeping the invariant that only the bottom node can be final.
      auto&& [shape, context] = source.pop();
      auto&& [concatShape, concatValues] = baseEngine_.concat(frontShape, shape);
      // Assign new edge variables, effectively hiding (behind new set of variables) variables with
      // different contexts. Variable context is shifted by concat from primary to secondary in lhs,
      // so the rhs' context goes as the primary context.
      source.push(concatShape,
                  {concatValues, context, source.pushContext({frontValues, topContext})});
    }
    else {
      source.push(frontShape, {frontValues, topContext});
    }
    for (auto&& [shape, values] : std::ranges::drop_view{factors, 1}) {
      source.push(shape, {values, topContext});
    }
    return source;
  }

  std::optional<EpsilonValue> epsilon(Relation const& source) const {
    STDEBUG(TIMER("StackEngine::epsilon"));
    // Only bottom of the stack can be final which means there must be only one node to have a
    // non-zero result.
    if (!isFinal(source)) {
      return std::nullopt;
    }
    auto const& [shape, context] = source.top();
    SubstitutionContext baseContext{nullptr, context};
    LabelEngine const& le = baseEngine_.labelEngine();
    return le.evaluate(le.flatten(*shape->epsilon()), &baseContext);
  }

  void reset() {
    SPDLOG_TRACE("SubstitutionEngine::reset()");
    factorsCache_.clear();
  }

 private:
  RelationEngine baseEngine_;
  HashMap<std::pair<BaseRelation, Symbol>, std::vector<std::pair<BaseRelation, SubstitutionValues>>>
      factorsCache_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_SUBSTITUTIONENGINE_HPP
