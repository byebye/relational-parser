#ifndef RELATIONAL_PARSER_PARSER_OUTPUT_HPP
#define RELATIONAL_PARSER_PARSER_OUTPUT_HPP

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <ostream>
#include <vector>

#include "parser/Interfaces.hpp"
#include "util/Container.hpp"
#include "util/DotGraph.hpp"
#include "util/Printing.hpp"

namespace relparser {

template<interface::Semiring S>
requires requires(typename S::Value const& v) {
  {std::get_if<typename S::AddNode>(v.get())};
  {std::get_if<typename S::MultiplyNode>(v.get())};
  {std::get_if<typename S::ProductionNode>(v.get())};
}
std::vector<typename S::Production const*> getDerivationSteps(typename S::Value const& parseTree) {
  using Semiring = S;
  using Production = typename Semiring::Production;

  std::vector<Production const*> derivationSteps;
  auto getDerivationSteps = stdutil::recurse{
      [&](auto&& getDerivationSteps, typename Semiring::Value const& value) -> void {
        if (Semiring::isZero(value) || Semiring::isOne(value)) {
          return;
        }
        else if (auto node = std::get_if<typename Semiring::AddNode>(value.get())) {
          throw std::invalid_argument(
              fmt::format("getDerivationSteps - add node is not allowed at this point"));
        }
        else if (auto node = std::get_if<typename Semiring::MultiplyNode>(value.get())) {
          // Derivations are reversed so traverse the tree post-order.
          getDerivationSteps(node->rhs);
          getDerivationSteps(node->lhs);
        }
        else if (auto node = std::get_if<typename Semiring::ProductionNode>(value.get())) {
          derivationSteps.emplace_back(node->production);
        }
        else {
          throw std::invalid_argument(
              fmt::format("getDerivationSteps - invalid node type: {}", value->index()));
        }
      }};
  getDerivationSteps(parseTree);
  return derivationSteps;
}

template<interface::Semiring S>
void printDerivationSteps(std::vector<typename S::Production const*> derivationSteps,
                          std::ostream& out) {
  for (auto const* step : derivationSteps) {
    fmt::print(out, "{}: {} ->", step->label(), step->lhs());
    std::ranges::for_each(step->rhs(), [&](auto&& rhs) { fmt::print(out, " {}", rhs); });
    fmt::print(out, "\n");
  }
}

template<interface::Semiring S>
void printParseTree(typename S::Value const& parseTree, std::ostream& out) {
  auto derivationSteps = getDerivationSteps<S>(parseTree);
  printDerivationSteps<S>(derivationSteps, out);
}

template<typename LE>
struct ParseForest {
  using LabelEngine = LE;
  using Node = typename LabelEngine::RelationNode;
  using NodeKey = decltype(std::declval<Node>().get());
  using Semiring = typename LabelEngine::Semiring;
  using RelationValue = typename LabelEngine::RelationValue;
  using Production = typename Semiring::Production;

 public:
  explicit ParseForest(Node const& parseForest) : parseForest_{parseForest} {
    auto countParseTrees =
        stdutil::recurse{[&](auto&& countParseTrees, Node const& value) -> std::size_t {
          if (auto it = numParseSubtrees_.find(value.get()); it != numParseSubtrees_.end()) {
            return it->second;
          }
          std::size_t numTrees{0};
          if (auto node = std::get_if<RelationValue>(value.get())) {
            numTrees = 1;
          }
          else if (auto node = std::get_if<typename LE::AddNode>(value.get())) {
            numTrees = countParseTrees(node->lhs) + countParseTrees(node->rhs);
          }
          else if (auto node = std::get_if<typename LE::MultiplyNode>(value.get())) {
            numTrees = countParseTrees(node->rhs) * countParseTrees(node->lhs);
          }
          else if (auto node = std::get_if<typename LE::FlattenNode>(value.get())) {
            numTrees = countParseTrees(node->node);
          }
          else {
            throw std::invalid_argument(
                fmt::format("getDerivationSteps - invalid node type: {}", value->index()));
          }
          numParseSubtrees_.emplace(value.get(), numTrees);
          return numTrees;
        }};
    numParseTrees_ = countParseTrees(parseForest_);
  }

  std::size_t numParseTrees() const {
    return numParseTrees_;
  }

  typename Semiring::Value parseTree(std::size_t index) const {
    auto getParseTree = stdutil::recurse{[&](auto&& getParseTree, Node const& value,
                                             std::size_t index, int indent) -> RelationValue {
      if (auto node = std::get_if<typename LE::AddNode>(value.get())) {
        auto lhsNumSubtrees = numParseSubtrees_.at(node->lhs.get());
        if (index < lhsNumSubtrees) {
          return getParseTree(node->lhs, index, indent + 2);
        }
        else {
          return getParseTree(node->rhs, index - lhsNumSubtrees, indent + 2);
        }
      }
      else if (auto node = std::get_if<typename LE::MultiplyNode>(value.get())) {
        auto lhsNumSubtrees = numParseSubtrees_.at(node->lhs.get());
        auto lhs = getParseTree(node->lhs, index % lhsNumSubtrees, indent + 2);
        auto rhs = getParseTree(node->rhs, index / lhsNumSubtrees, indent + 2);
        // Pending is kept reversed for more efficient value computation, therefore
        // multiplication is reversed as well.
        return {Semiring::multiply(std::move(rhs.pending), std::move(lhs.pending)),
                Semiring::multiply(std::move(lhs.guiding), std::move(rhs.guiding))};
      }
      else if (auto node = std::get_if<typename LE::FlattenNode>(value.get())) {
        auto lhs = getParseTree(node->node, index, indent + 2);
        return {Semiring::one(),
                Semiring::multiply(std::move(lhs.pending), std::move(lhs.guiding))};
      }
      else if (auto node = std::get_if<RelationValue>(value.get())) {
        return *node;
      }
      else {
        throw std::invalid_argument(
            fmt::format("getDerivationSteps - invalid node type: {}", value->index()));
      }
    }};
    auto const parseTree = getParseTree(parseForest_, index, 0);
    return parseTree.guiding;
  }

 private:
  std::size_t numParseTrees_{0};
  HashMap<NodeKey, std::size_t> numParseSubtrees_;
  Node const& parseForest_;
};

struct ParseTreeNode {
  std::string label;
  bool terminal = false;
  std::vector<std::unique_ptr<ParseTreeNode>> children;
};

template<interface::GrammarAnalysis GA>
std::unique_ptr<ParseTreeNode> getLabeledParseTree(
    std::vector<typename GA::Semiring::Production const*> derivationSteps,
    GA const& grammarAnalysis) {
  using GrammarAnalysis = GA;
  using Production = typename GA::Semiring::Production;
  using Result = std::pair<int, std::vector<std::unique_ptr<ParseTreeNode>>>;

  auto getLabeledTree = stdutil::recurse{[&](auto getLabeledTree, int step) -> Result {
    if (step >= derivationSteps.size()) {
      throw std::invalid_argument(
          "Cannot generate parse tree, the provided derivation is invalid.");
    }

    std::vector<std::unique_ptr<ParseTreeNode>> parentNodes;
    Production const* production = derivationSteps[step];

    bool const hasLabel = !production->label().empty();
    auto node = std::make_unique<ParseTreeNode>(production->label());
    if (hasLabel) {
      node->label = production->label();
    }

    // Index of the child derivation step, assuming it is the leftmost derivation.
    int childStep = step;
    for (auto childSymbol : production->rhs()) {
      if (grammarAnalysis.isTerminal(childSymbol)) {
        auto child =
            std::make_unique<ParseTreeNode>(grammarAnalysis.grammar().terminalLabel(childSymbol),
                                            /*terminal=*/true);
        if (hasLabel) {
          node->children.emplace_back(std::move(child));
        }
        else {
          parentNodes.emplace_back(std::move(child));
        }
      }
      else {
        ++childStep;
        if (childSymbol != derivationSteps[childStep]->lhs()) {
          throw std::invalid_argument(
              "Cannot generate parse tree, the provided derivation is invalid.");
        }
        auto&& [newStep, childNodes] = getLabeledTree(childStep);
        childStep = newStep;
        if (hasLabel) {
          std::ranges::move(childNodes, std::back_inserter(node->children));
        }
        else {
          std::ranges::move(childNodes, std::back_inserter(parentNodes));
        }
      }
    }
    if (hasLabel) {
      parentNodes.emplace_back(std::move(node));
    }
    return {childStep, std::move(parentNodes)};
  }};
  auto&& [step, childNodes] = getLabeledTree(0);
  if (step + 1 != derivationSteps.size()) {
    throw std::invalid_argument(fmt::format(
        "Provided parse tree is invalid, {} out of {} derivation steps have been processed.", step,
        derivationSteps.size()));
  }
  if (childNodes.size() != 1) {
    throw std::invalid_argument(
        "Provided parse tree is invalid, starting nonterminal has empty label.");
  }
  return std::move(childNodes[0]);
}

template<interface::GrammarAnalysis GA>
void printLabeledDerivationSteps(
    std::vector<typename GA::Semiring::Production const*> derivationSteps,
    GA const& grammarAnalysis, std::ostream& out) {
  auto labeledParseTree = getLabeledParseTree(derivationSteps, grammarAnalysis);
  auto printDerivations =
      stdutil::recurse{[&](auto&& printDerivations, const ParseTreeNode& node) -> void {
        fmt::print(out, "{} ->", node.label);
        std::ranges::for_each(node.children,
                              [&](auto&& child) { fmt::print(out, " {}", child->label); });
        fmt::print(out, "\n");
        std::ranges::for_each(
            node.children | std::views::filter([](auto&& child) { return !child->terminal; }),
            [&](auto&& child) { printDerivations(*child); });
      }};
  printDerivations(*labeledParseTree);
}

template<interface::GrammarAnalysis GA>
void printLabeledParseTree(typename GA::Semiring::Value const& parseTree, GA const& grammarAnalysis,
                           std::ostream& out) {
  using Semiring = typename GA::Semiring;
  auto derivationSteps = getDerivationSteps<Semiring>(parseTree);
  printLabeledDerivationSteps(derivationSteps, grammarAnalysis, out);
}

template<interface::GrammarAnalysis GA>
void printLabeledDerivationStepsToDot(
    std::vector<typename GA::Semiring::Production const*> derivationSteps,
    GA const& grammarAnalysis, std::ostream& out) {
  using NodeOptions = DotGraph::NodeOptions;
  using EdgeOptions = DotGraph::EdgeOptions;

  auto labeledParseTree = getLabeledParseTree(derivationSteps, grammarAnalysis);

  DotGraph dotGraph{out, DotGraph::Options{DotGraph::Directed, DotGraph::TopToBottom}};
  int index = 0;
  auto printDot = stdutil::recurse{[&](auto&& printDot, const ParseTreeNode& node) -> int {
    int const id = index++;
    std::string const nodeId = fmt::format("s{}", id);
    auto symbolNodeOpts =
        NodeOptions{}.shape(node.terminal ? DotGraph::Box : DotGraph::Oval).fontName("Courier");
    dotGraph.node(nodeId, node.label, symbolNodeOpts);
    if (!node.terminal) {
      for (auto&& child : node.children) {
        int const childId = printDot(*child);
        dotGraph.edge(nodeId, fmt::format("s{}", childId), "", EdgeOptions{});
      }
    }
    return id;
  }};
  printDot(*labeledParseTree);
}

template<interface::GrammarAnalysis GA>
void printLabeledParseTreeToDot(typename GA::Semiring::Value const& parseTree,
                                GA const& grammarAnalysis, std::ostream& out) {
  using Semiring = typename GA::Semiring;
  auto derivationSteps = getDerivationSteps<Semiring>(parseTree);
  printLabeledDerivationStepsToDot(derivationSteps, grammarAnalysis, out);
}

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_OUTPUT_HPP
