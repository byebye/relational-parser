#ifndef RELATIONAL_PARSER_PARSER_PARSING_HPP
#define RELATIONAL_PARSER_PARSER_PARSING_HPP

#include <functional>
#include <map>
#include <optional>
#include <string>
#include <vector>

namespace relparser {

struct CommandlineFlags {
  std::string parsingMode;
  std::string relationEngine;
  std::string grammar;
  std::vector<std::string> inputFiles;
  std::optional<std::string> inputDirectory;
  std::optional<std::string> inputFilesRegex;
  bool recursive;
  std::optional<std::string> resultsFile;
  std::optional<std::string> dotResultsFile;
  int parseTreesLimit;
  std::size_t statusUpdateEveryTokens;
  bool resetParserState;
};

extern std::map<std::string,
                std::map<std::string, std::function<void(CommandlineFlags const&)>>> const
    parsingModes;

}  // namespace relparser

#endif  // RELATIONAL_PARSER_PARSER_PARSING_HPP
