#include "parser/Parsing.hpp"

#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <chrono>
#include <fstream>

#include "Input.hpp"
#include "Statistics.hpp"
#include "Timer.hpp"
#include "parser/Engines.hpp"
#include "parser/Grammar.hpp"
#include "parser/Interfaces.hpp"
#include "parser/Output.hpp"
#include "parser/Parser.hpp"
#include "util/Printing.hpp"

namespace relparser {

namespace {

using stdutil::openFileOrThrow;

template<interface::Parser Parser>
void printResult(std::optional<typename Parser::EpsilonValue> const& result, Parser const& parser,
                 std::string_view inputFile, std::fstream& resultsStream,
                 CommandlineFlags const& flags) {
  using EpsilonValue = typename Parser::EpsilonValue;
  using Semiring = typename Parser::AtomicEngine::LabelEngine::Semiring;

  if constexpr (Parser::GrammarAnalysis::Semiring::isCommutative) {
    if (result) {
      SPDLOG_INFO("Result: {}", stdutil::Printable{result});
    }
    else {
      SPDLOG_INFO("Result: -");
    }
  }

  if (!flags.resultsFile) {
    return;
  }
  SPDLOG_INFO("Printing results to file {}", *flags.resultsFile);
  STATS("ParseRunner::printResult");
  spdlog::default_logger()->flush();

  constexpr bool isParseTree = interface::Relation<EpsilonValue>;
  constexpr bool isParseForest = requires(EpsilonValue v) {
    {std::get<0>(*v)};
  };

  if (!result) {
    fmt::print(resultsStream, "{}: -\n", inputFile);
    resultsStream.flush();
    return;
  }

  if constexpr (!isParseTree && !isParseForest) {
    fmt::print(resultsStream, "{}: {}\n", inputFile, stdutil::Printable{*result});
  }
  else {
    auto const& grammarAnalysis = parser.relationEngine().atomicEngine().grammarAnalysis();

    auto outputDerivationSteps = [&](auto const& parseTree, auto const& dotFilename) {
      auto steps = getDerivationSteps<Semiring>(parseTree);
      fmt::print(resultsStream, "--- All derivation steps ---\n");
      printDerivationSteps<Semiring>(steps, resultsStream);
      resultsStream.flush();
      fmt::print(resultsStream, "--- Labeled derivation steps ---\n");
      printLabeledDerivationSteps(steps, grammarAnalysis, resultsStream);
      if (dotFilename) {
        std::fstream dotStream = openFileOrThrow(*dotFilename, std::ios_base::out);
        printLabeledDerivationStepsToDot(steps, grammarAnalysis, dotStream);
      }
    };

    if constexpr (isParseTree) {
      fmt::print(resultsStream, "{}:\n", inputFile);
      outputDerivationSteps(result->guiding, flags.dotResultsFile);
    }
    else if constexpr (isParseForest) {
      fmt::print(resultsStream, "{}:\n", inputFile);

      using LabelEngine = typename Parser::AtomicEngine::LabelEngine;
      ParseForest<LabelEngine> parseForest{*result};
      SPDLOG_INFO("Number of parse trees: {}", parseForest.numParseTrees());

      auto dotFilename = [&](int index) -> std::optional<std::string> {
        std::string_view dotBaseFilename;
        if (!flags.dotResultsFile) {
          return std::nullopt;
        }
        dotBaseFilename = *flags.dotResultsFile;
        if (dotBaseFilename.ends_with(".dot")) {
          dotBaseFilename = dotBaseFilename.substr(0, dotBaseFilename.size() - 4);
        }
        return fmt::format("{}_{}.dot", dotBaseFilename, index);
      };
      for (auto i = 0; i < parseForest.numParseTrees() && i < flags.parseTreesLimit; i++) {
        auto parseTree = parseForest.parseTree(i);
        outputDerivationSteps(parseTree, dotFilename(i));
      }
    }
  }
  resultsStream.flush();
}

template<typename RelationsEngineProvider>
void runParsing(CommandlineFlags const& flags) {
  using namespace std::chrono;

  Timer totalRunTimer{};

  SPDLOG_INFO("Loading grammar from file {}", flags.grammar);
  Grammar<Symbol> grammar = loadRpGrammar(flags.grammar);

  Timer preprocessTimer{};
  RelationsEngineProvider provider;
  Parser parser{provider(std::move(grammar)), flags.statusUpdateEveryTokens};
  Statistics::time("ParseRunner::preprocessing") = preprocessTimer.stop().ns();
  SPDLOG_INFO("Preprocessing time: {}", preprocessTimer);

  SPDLOG_INFO("Parser state {} reset after each file",
              flags.resetParserState ? "will be" : "will not be");

  nanoseconds totalParsingTime{};

  std::fstream resultsStream;
  if (flags.resultsFile) {
    resultsStream = openFileOrThrow(*flags.resultsFile, std::ios_base::out);
  }

  auto parseFile = [&](std::string const& inputFile) {
    SPDLOG_DEBUG("Parsing file {}", inputFile);
    std::vector<Symbol> tokens;
    try {
      TIMER("ParseRunner::readTokens");
      tokens = loadTokens(inputFile);
    }
    catch (const std::exception& ex) {
      SPDLOG_ERROR("Error filling tokens from file {}", inputFile);
      std::exit(1);
    }

    Timer parseTimer{};
    auto relation = parser.parse(tokens);
    Statistics::time("ParseRunner::parsing") += parseTimer.stop().ns();
    SPDLOG_INFO("{} - parsing time: {}", inputFile, parseTimer);

    Timer epsilonTimer{};
    auto result = parser.epsilon(relation);
    Statistics::time("ParseRunner::epsilon") += epsilonTimer.stop().ns();
    SPDLOG_INFO("{} - evaluating epsilon time: {}", inputFile, epsilonTimer);

    Timer resultsTimer{};
    printResult(result, parser, inputFile, resultsStream, flags);
    Statistics::time("ParseRunner::printResults") += resultsTimer.stop().ns();
    SPDLOG_INFO("{} - printing results time: {}", inputFile, resultsTimer);

    if (flags.resetParserState) {
      parser.reset();
    }
  };

  auto const& inputFiles =
      !flags.inputFiles.empty()
          ? flags.inputFiles
          : getInputFiles(*flags.inputDirectory, flags.recursive, flags.inputFilesRegex);
  SPDLOG_INFO("Parsing {} input files", inputFiles.size());
  std::ranges::for_each(inputFiles, parseFile);

  auto tokensCount = parser.parsedTokensCount();
  auto epsTime = duration_cast<milliseconds>(Statistics::time("ParseRunner::epsilon"));
  auto parsingTime = duration_cast<milliseconds>(Statistics::time("ParseRunner::parsing"));
  SPDLOG_INFO("Parsed {} files with {} tokens in {} (epsilon time: {})", inputFiles.size(),
              tokensCount, parsingTime, epsTime);

  totalRunTimer.stop();
  SPDLOG_INFO("Total execution time: {}", totalRunTimer.ms());
  SPDLOG_INFO("{}", Statistics::StatsPrinter{});
  fmt::print("\nTotal execution time: {}\n", totalRunTimer.ms());
  fmt::print("{}\n", Statistics::StatsPrinter{});
}

}  // namespace

std::map<std::string, std::map<std::string, std::function<void(CommandlineFlags const&)>>> const
    parsingModes{
        {"recognize",
         {
             {"trivial", &runParsing<RecognitionDagEngine>},
             {"trivialopt", &runParsing<RecognitionDagEngineOpt>},
             {"contextfree", &runParsing<RecognitionStackEngine>},
             {"contextfreeopt", &runParsing<RecognitionStackEngineOpt>},
             {"subst", &runParsing<RecognitionSubstEngine>},
             {"substopt", &runParsing<RecognitionSubstEngineOpt>},
         }},
        {"count",
         {
             {"trivial", &runParsing<CountingDagEngine>},
             {"trivialopt", &runParsing<CountingDagEngineOpt>},
             {"contextfree", &runParsing<CountingStackEngine>},
             {"contextfreeopt", &runParsing<CountingStackEngineOpt>},
             {"subst", &runParsing<CountingSubstEngine>},
             {"substopt", &runParsing<CountingSubstEngineOpt>},
         }},
        {"parse",
         {
             {"trivial", &runParsing<ParsingDagEngine>},
             {"trivialopt", &runParsing<ParsingDagEngineOpt>},
             {"contextfree", &runParsing<ParsingStackEngine>},
             {"contextfreeopt", &runParsing<ParsingStackEngineOpt>},
             {"subst", &runParsing<ParsingSubstEngine>},
             {"substopt", &runParsing<ParsingSubstEngineOpt>},
         }},
        {"forest",
         {
             {"trivial", &runParsing<ForestDagEngine>},
             {"trivialopt", &runParsing<ForestDagEngineOpt>},
             {"contextfree", &runParsing<ForestStackEngine>},
             {"contextfreeopt", &runParsing<ForestStackEngineOpt>},
             {"subst", &runParsing<ForestSubstEngine>},
             {"substopt", &runParsing<ForestSubstEngineOpt>},
         }},
    };

}  // namespace relparser
