#ifndef RELATIONAL_PARSER_STATISTICS_HPP
#define RELATIONAL_PARSER_STATISTICS_HPP

#define RELPARSER_STATS_LEVEL_OFF 0
#define RELPARSER_STATS_LEVEL_INFO 1
#define RELPARSER_STATS_LEVEL_DEBUG 2
#define RELPARSER_STATS_LEVEL_TRACE 3

#ifndef RELPARSER_STATS_ACTIVE_LEVEL
#define RELPARSER_STATS_ACTIVE_LEVEL RELPARSER_STATS_LEVEL_INFO
#endif

#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_TRACE
#define STTRACE(x) x
#else
#define STTRACE(x)
#endif
#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_DEBUG
#define STDEBUG(x) x
#else
#define STDEBUG(x)
#endif
#if RELPARSER_STATS_ACTIVE_LEVEL >= RELPARSER_STATS_LEVEL_INFO
#define STINFO(x) x
#else
#define STINFO(x)
#endif

#define CONCAT_AUX(X, Y) X##Y
#define CONCAT(X, Y) CONCAT_AUX(X, Y)
#define UVAR(x) CONCAT(x, __LINE__) // unique variable name

#define COUNTER_AUX(key, inc, varName)             \
  static auto& varName = Statistics::counter(key); \
  varName += inc;
#define COUNTER(key) COUNTER_AUX(key, 1, UVAR(_statsCounter))
// Increment counter by a variable or number other than 1.
#define COUNTER_INCVAR(key, inc) COUNTER_AUX(key, inc, UVAR(_statsCounter))
// Counter with a variable key for which static variable cannot be used.
// Note: Performance is affected because the map is accessed on every increment.
#define COUNTER_VARKEY(key) Statistics::counter(key)++;

#define TIMER_AUX(key, varName)                 \
  static auto& varName = Statistics::time(key); \
  auto UVAR(_statsTimer) = Statistics::startTimer(varName);
#define TIMER(key) TIMER_AUX(key, UVAR(_statsTime))

#define STATS(key) TIMER(key) COUNTER(key)

#include <chrono>
#include <ostream>

#include "CommonTypes.hpp"
#include "Timer.hpp"
#include "util/Finally.hpp"

namespace relparser {

class Statistics {
 public:
  static std::chrono::nanoseconds& time(std::string const& key) {
    return instance().times_[key];
  }

  static long& counter(std::string const& key) {
    return instance().counters_[key];
  }

  static auto startTimer(std::string key) {
    return finally([key = std::move(key), timer = Timer{}]() mutable {
      Statistics::time(key) += timer.stop().ns();
    });
  }

  static auto startTimer(std::chrono::nanoseconds& time) {
    return finally([&time, timer = Timer{}]() mutable { time += timer.stop().ns(); });
  }

  struct StatsPrinter {
    friend std::ostream& operator<<(std::ostream& os, StatsPrinter const& printer) {
      printer.print(os);
      return os;
    }

   private:
    void print(std::ostream& os) const;
  };

  friend StatsPrinter;

 private:
  Statistics() = default;
  Statistics(Statistics const&) = delete;

  std::map<std::string, std::chrono::nanoseconds> times_;
  std::map<std::string, long> counters_;

  static Statistics& instance() {
    static Statistics stats;
    return stats;
  };
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_STATISTICS_HPP
