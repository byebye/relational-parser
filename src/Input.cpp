#include "Input.hpp"

#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <filesystem>
#include <fstream>
#include <regex>

#include "parser/Grammar.hpp"
#include "util/Printing.hpp"

namespace relparser {

using Symbol = int;
using Production = typename Grammar<Symbol>::Production;

using stdutil::openFileOrThrow;

// Reads .rp grammar in custom format:  [<LABEL>]: <LHS> ->[ <RHS> ...], e.g. A: 1 -> 2 3
Grammar<Symbol> loadRpGrammar(std::string const& filename) {
  std::fstream input{openFileOrThrow(filename, std::ios_base::in)};
  Grammar<Symbol> grammar;
  int lineNumber = 0;
  std::string line;
  auto readLine = [&](auto&& f) {
    ++lineNumber;
    if (!std::getline(input, line)) {
      return false;  // eof
    }
    if (line.empty()) {
      return true;  // ignore empty lines
    }
    if (line == "[terminals]") {
      return false;
    }
    std::istringstream iss(line);
    if (f(iss) && iss.eof()) {
      return true;
    }
    throw std::invalid_argument(
        fmt::format("Invalid grammar input at line {}: '{}'", lineNumber, line));
  };
  readLine([&](std::istream& is) {
    Symbol start;
    if (is >> start) {
      grammar.setStart(start);
      return true;
    }
    return false;
  });
  auto numProductions{0};
  while (readLine([&](std::istream& is) {
    std::string label;
    if (!std::getline(is, label, ':')) {
      return false;
    }
    Symbol lhs;
    if (!(is >> lhs)) {
      return false;
    }
    std::string arrow(3, '\0');
    if (!is.read(&arrow[0], 3) || arrow != " ->") {
      return false;
    }
    std::vector<Symbol> rhs;
    Symbol s;
    while (is >> s) {
      rhs.emplace_back(s);
    }
    grammar.addProduction(std::move(label), lhs, std::move(rhs));
    return true;
  })) {
  }
  // [terminals]
  while (readLine([&](std::istream& is) {
    std::string label;
    if (!std::getline(is, label)) {
      return false;
    }
    auto pos = label.find_last_of(':');
    if (pos == std::string::npos) {
      return false;
    }
    is.seekg(pos + 1);
    Symbol terminal;
    if (!(is >> terminal)) {
      return false;
    }
    label = label.substr(0, pos);
    grammar.setTerminalLabel(terminal, std::move(label));
    return true;
  })) {
  }
  SPDLOG_INFO("Loaded .rp grammar with {} productions", grammar.productions().size());
  return grammar;
}

std::vector<Symbol> loadTokens(std::string const& filename) {
  std::fstream input{openFileOrThrow(filename, std::ios_base::in)};
  std::vector<Symbol> tokens;
  Symbol token;
  while (input >> token) {
    tokens.emplace_back(token);
  }
  SPDLOG_DEBUG("Loaded {} tokens from input", tokens.size());
  return tokens;
}

std::vector<std::string> getInputFiles(std::string_view directory, bool recursive,
                                       std::optional<std::string> const& filesRegex) {
  SPDLOG_INFO("Scanning directory {} {} for input files", directory,
              recursive ? "recursively" : "");
  if (filesRegex) {
    SPDLOG_INFO("Matching input file names with regex '{}'", *filesRegex);
  }
  namespace fs = std::filesystem;

  std::vector<std::string> inputFiles;

  std::regex fileRegex{filesRegex.value_or(".*")};
  auto addInputFile = [&](fs::directory_entry const& entry) {
    if (entry.is_regular_file() && std::regex_match(entry.path().filename().c_str(), fileRegex)) {
      inputFiles.emplace_back(entry.path().native());
    }
  };

  if (recursive) {
    std::ranges::for_each(fs::recursive_directory_iterator(directory), addInputFile);
  }
  else {
    std::ranges::for_each(fs::directory_iterator(directory), addInputFile);
  }

  std::ranges::sort(inputFiles);
  return inputFiles;
}

}  // namespace relparser