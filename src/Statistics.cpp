#include "Statistics.hpp"

#include <fmt/chrono.h>
#include <fmt/ostream.h>

#include "util/Container.hpp"

namespace relparser {

void Statistics::StatsPrinter::print(std::ostream& os) const {
  if (!Statistics::instance().times_.empty()) {
    fmt::print(os, "Time statistics:\n");
    for (auto const& [key, time] : Statistics::instance().times_) {
      auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(time);
      fmt::print(os, "- {}: {}\n", key, millis);
    }
  }
  if (!Statistics::instance().counters_.empty()) {
    fmt::print(os, "Counters statistics:\n");
    for (auto const& [key, counter] : Statistics::instance().counters_) {
      fmt::print(os, "- {}: {}\n", key, counter);
    }
  }
}

}  // namespace relparser
