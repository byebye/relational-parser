#ifndef RELATIONAL_PARSER_COMMONTYPES_HPP
#define RELATIONAL_PARSER_COMMONTYPES_HPP

#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>
#include <map>
#include <set>

namespace relparser {

template<typename K, typename V, typename... Ts>
using OrdMap = std::map<K, V, Ts...>;

template<typename K, typename V, typename... Ts>
using HashMap = absl::flat_hash_map<K, V, Ts...>;

template<typename T, typename... Ts>
using OrdSet = std::set<T, Ts...>;

template<typename T, typename... Ts>
using HashSet = absl::flat_hash_set<T, Ts...>;

#ifdef DETERMINISTIC_GRAMMAR

template<typename K, typename V, typename... Ts>
using Map = OrdMap<K, V, Ts...>;

template<typename T, typename... Ts>
using Set = OrdSet<T, Ts...>;

#else /* NONDETERMINISTIC - FASTER */

template<typename K, typename V, typename... Ts>
using Map = HashMap<K, V, Ts...>;

template<typename T, typename... Ts>
using Set = HashSet<T, Ts...>;

#endif

}  // namespace relparser

#endif  // RELATIONAL_PARSER_COMMONTYPES_HPP
