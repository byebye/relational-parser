#include "antlr/AntlrParser.hpp"

#include <algorithm>

#include "antlr4-runtime.h"

namespace relparser {

void AntlrParser::fillTokens(std::istream& inFileStream) {
  inputStream->load(inFileStream);
  lexer->setInputStream(inputStream.get());
  tokenStream->setTokenSource(lexer.get());
  tokenStream->fill();

  std::vector<antlr4::Token*> lexerTokens = tokenStream->getTokens();
  if (!hasEOF) {
    lexerTokens.pop_back();
  }

  tokens.resize(lexerTokens.size());
  std::transform(lexerTokens.begin(), lexerTokens.end(), tokens.begin(),
                 [](antlr4::Token* token) { return token->getType(); });

  tokensText.clear();
  tokensText.reserve(lexerTokens.size());
  std::transform(lexerTokens.begin(), lexerTokens.end(), std::back_inserter(tokensText),
                 [](antlr4::Token* token) { return token->getText(); });
}

AntlrParser::~AntlrParser() = default;

}  // namespace relparser
