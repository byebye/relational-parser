#ifndef RELATIONAL_PARSER_ANTLR_ANTLRPARSER_HPP
#define RELATIONAL_PARSER_ANTLR_ANTLRPARSER_HPP

#include <istream>
#include <memory>
#include <vector>

// Make unique_ptrs happy by including only Antlr forward declarations.
#include "antlr/AntlrFwd.hpp"

namespace relparser {

class AntlrParser {
 public:
  std::unique_ptr<antlr4::ANTLRInputStream> inputStream;
  std::unique_ptr<antlr4::Lexer> lexer;
  std::unique_ptr<antlr4::CommonTokenStream> tokenStream;
  std::vector<std::ptrdiff_t> tokens;
  std::vector<std::string> tokensText;
  bool hasEOF;

  void fillTokens(std::istream& inFileStream);

  virtual antlr4::Parser* parser() = 0;

  virtual antlr4::ParserRuleContext* parse() = 0;

  virtual ~AntlrParser();
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_ANTLR_ANTLRPARSER_HPP
