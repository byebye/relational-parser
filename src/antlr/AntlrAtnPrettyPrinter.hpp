#ifndef RELATIONAL_PARSER_ANTLR_ANTLRATNPRETTYPRINTER_HPP
#define RELATIONAL_PARSER_ANTLR_ANTLRATNPRETTYPRINTER_HPP

#include <ostream>
#include <stack>

#include "antlr/AntlrFwd.hpp"

namespace relparser {

class AntlrAtnPrettyPrinter {
  using Transition = antlr4::atn::Transition;
  using ATNState = antlr4::atn::ATNState;

 public:
  AntlrAtnPrettyPrinter(antlr4::Parser const& parser, std::size_t indentWidth, std::ostream& out)
      : parser_(parser), indentWidth_(indentWidth), out_(out) {
    indents_.push("");
  }

  void enter(ATNState* state);

  void exit(ATNState* state);

  void enter(Transition* trans, bool isLastChild);

  void exit(Transition* trans);

 private:
  antlr4::Parser const& parser_;
  std::size_t const indentWidth_;
  std::ostream& out_;
  std::stack<std::string> indents_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_ANTLR_ANTLRATNPRETTYPRINTER_HPP
