#ifndef RELATIONAL_PARSER_ANTLR_ANTLRPARSERSREGISTRY_HPP
#define RELATIONAL_PARSER_ANTLR_ANTLRPARSERSREGISTRY_HPP

#include <absl/strings/str_join.h>
#include <fmt/format.h>
#include <cassert>
#include <functional>
#include <map>
#include <set>

#include "antlr/AntlrParser.hpp"

namespace relparser {

class AntlrParsersRegistry {
 public:
  AntlrParsersRegistry(const AntlrParsersRegistry&) = delete;

  struct RegisterParserFactory {
    RegisterParserFactory(std::string name, std::function<std::unique_ptr<AntlrParser>()> factory) {
      bool inserted = instance().registry_.emplace(std::move(name), std::move(factory)).second;
      assert(inserted);
    }
  };

  static std::set<std::string> registeredNames() {
    std::set<std::string> result;
    for (auto const& element : instance().registry_) {
      result.emplace(element.first);
    }
    return result;
  }

  static bool isRegistered(std::string const& name) {
    return instance().registry_.contains(name);
  }

  static std::unique_ptr<AntlrParser> create(std::string const& name) {
    if (auto it = instance().registry_.find(name); it != instance().registry_.end()) {
      return (it->second)();
    }
    throw std::invalid_argument(
        fmt::format("Grammar '{}' has not been registered. Must be one of: {}", name,
                    absl::StrJoin(registeredNames(), ", ")));
  }

 private:
  AntlrParsersRegistry() = default;

  static AntlrParsersRegistry& instance() {
    static AntlrParsersRegistry registry;
    return registry;
  }

  std::map<std::string, std::function<std::unique_ptr<AntlrParser>()>> registry_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_ANTLR_ANTLRPARSERSREGISTRY_HPP
