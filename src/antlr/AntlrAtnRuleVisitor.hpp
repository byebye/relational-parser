#ifndef RELATIONAL_PARSER_ANTLR_ANTLRATNRULEVISITOR_HPP
#define RELATIONAL_PARSER_ANTLR_ANTLRATNRULEVISITOR_HPP

#include "antlr4-runtime.h"

#include "CommonTypes.hpp"
#include "util/Finally.hpp"

namespace relparser {

class AntlrAtnRuleVisitor {
  using Transition = antlr4::atn::Transition;
  using ATNState = antlr4::atn::ATNState;

 public:
  explicit AntlrAtnRuleVisitor(std::size_t const ruleIndex) : ruleToVisit_(ruleIndex) {
  }

  template<typename TVisitor>
  void visit(TVisitor& visitor, ATNState* state) {
    visitor.enter(state);
    auto exit_state = finally([&visitor, &state] { visitor.exit(state); });

    // RULE_STOP has all rule exiting transitions which should be accessed via
    // RuleTransition::followState.
    if (state->getStateType() == ATNState::RULE_STOP)
      return;

    for (std::size_t i = 0; i < state->transitions.size(); ++i) {
      Transition* const trans = state->transitions[i];

      bool const isLastChild = (i + 1 == state->transitions.size());

      visitor.enter(trans, isLastChild);
      auto exit_transition = finally([&visitor, &trans] { visitor.exit(trans); });

      if (trans->getSerializationType() == Transition::RULE) {
        // static_cast is safe here because the type is known.
        auto* const ruleTarget = static_cast<antlr4::atn::RuleTransition*>(trans)->followState;
        visited_.insert(ruleTarget);
        visit(visitor, ruleTarget);
      }
      else {
        ATNState* const target = trans->target;
        if (target->ruleIndex == ruleToVisit_ && !visited_.contains(target)) {
          visited_.insert(target);
          visit(visitor, target);
        }
        else {
          visitor.enter(target);
          visitor.exit(target);
        }
      }
    }
  }

 private:
  std::size_t const ruleToVisit_;
  HashSet<ATNState*> visited_;
};

}  // namespace relparser

#endif  // RELATIONAL_PARSER_SRC_GRAMMAR_ANTLRATNVISITOR_HPP
