#include "antlr/AntlrAtnPrettyPrinter.hpp"

#include <fmt/ostream.h>
#include <ostream>
#include <vector>

#include "antlr4-runtime.h"

namespace relparser {

void AntlrAtnPrettyPrinter::enter(ATNState* state) {
  fmt::print(out_, "{}({}) «{}»\n", parser_.getRuleNames()[state->ruleIndex], state->stateNumber,
             ATNState::serializationNames[state->getStateType()]);
}

void AntlrAtnPrettyPrinter::exit(ATNState* state) {
}

void AntlrAtnPrettyPrinter::enter(Transition* trans, bool isLastChild) {
  std::string const indent = indents_.top();
  indents_.push(indent + (isLastChild ? "  " : "│ "));

  fmt::print(out_, "{}{}⧼", indent, (isLastChild ? "└─" : "├─"),
             Transition::serializationNames[trans->getSerializationType()]);

  if (antlr4::misc::IntervalSet const& label = trans->label(); !label.isEmpty()) {
    antlr4::dfa::Vocabulary const& vocab = parser_.getVocabulary();
    std::vector<std::ptrdiff_t> const& labels = label.toList();

    fmt::print(out_, "[{} {}", labels[0], vocab.getDisplayName(labels[0]));
    for (int i = 1; i < labels.size(); ++i) {
      fmt::print(out_, ", {} {}", labels[i], vocab.getDisplayName(labels[i]));
    }
    fmt::print(out_, "]");
  }
  else if (trans->getSerializationType() == Transition::RULE) {
    fmt::print(out_, "[{}]", parser_.getRuleNames()[trans->target->ruleIndex]);
  }
  else if (trans->getSerializationType() == Transition::ACTION) {
    auto* const action = static_cast<antlr4::atn::ActionTransition*>(trans);
    if (action->actionIndex == INVALID_INDEX || action->actionIndex == 0xFFFF) {
      fmt::print(out_, "[none]");
    }
    else {
      fmt::print(out_, "[{}, is ctx dependent: {}]", action->actionIndex, action->isCtxDependent);
    }
  }
  else if (trans->getSerializationType() == Transition::PRECEDENCE) {
    auto* const prec = static_cast<antlr4::atn::PrecedencePredicateTransition*>(trans);
    fmt::print(out_, "[{}, {}]", prec->getPredicate()->toString(),
               prec->getPredicate()->precedence);
  }
  fmt::print(out_, "⧽──➤ ");
}

void AntlrAtnPrettyPrinter::exit(Transition* trans) {
  indents_.pop();
}

}  // namespace relparser
