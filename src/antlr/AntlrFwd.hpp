#ifndef RELATIONAL_PARSER_ANTLR_ANTLRFWD_HPP
#define RELATIONAL_PARSER_ANTLR_ANTLRFWD_HPP

// forward declarations to avoid including antlr4-runtime.h
namespace antlr4 {

class ANTLRInputStream;
class Lexer;
class CommonTokenStream;
class Parser;
class ParserRuleContext;

namespace atn {

class Transition;
class ATNState;

}  // namespace atn

}  // namespace antlr4

#endif  // RELATIONAL_PARSER_ANTLR_ANTLRFWD_HPP
