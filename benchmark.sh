#!/bin/bash

parser="${1}"
mode="${2}"
grammar="${3}"
optinput="${4}"
statslevel=OFF
logslevel=0

output=${grammar}
case "${grammar}" in
  java8)
    [[ ! ${parser} =~ antlr|elkhound ]] && grammar="resources/Java8.rp"
    input="-d resources/Java8/elasticsearch-6.5.3 -x .*.java8.tok -r"
    ;;
  java)
    [[ ! ${parser} =~ antlr|elkhound ]] && grammar="resources/Java.rp"
    input="-d resources/Java8/elasticsearch-6.5.3 -x .*.java.tok -r"
    ;;
  json)
    [[ ! ${parser} =~ antlr|elkhound  ]] && grammar="resources/JSON.rp"
    input="-d resources/json -x .*.json.tok"
    ;;
  xml)
    [[ ! ${parser} =~ antlr|elkhound  ]] && grammar="resources/XML.rp"
    input="-d resources/xml -x .*.xml.tok"
    ;;
  arithmetic)
    [[ ! ${parser} =~ antlr|elkhound ]] && grammar="resources/arithmetic.rp"
    input="-f resources/arithmetic/expr100.in.arithmetic.tok"
    ;;
  arithmetic-replica)
    [[ "${parser}" == "antlr" ]] && { echo "Non-ANTLR grammar"; exit 1; }
    [[ ${parser} != elkhound ]] && grammar="resources/arithmetic-replica.rp"
    input="-f resources/arithmetic/expr100.in.arithmetic.tok"
    ;;
  rec-left|rec-right)
    [[ "${parser}" == "antlr" ]] && { echo "Non-ANTLR grammar"; exit 1; }
    [[ ${parser} != elkhound ]] && grammar="resources/${grammar}.rp"
    # input="-d resources/rec -x .*.tok"
    input="-f resources/rec/s50k.tok"
    ;;
  *)
    echo "Unsupported grammar: ${grammar}"
    exit 1
    ;;
esac

if [[ -n "${optinput}" ]]; then
  input="${optinput}"
fi

./run.sh benchmark run release ${logslevel} ${mode} ${parser} \
  "${grammar}" "${input}" "${output}" statslevel ${statslevel} updateevery 0 noresult
