# About Relational Parser

Relational Parser is a generalized parser designed by dr Grzegorz Herman, the publication available
at https://doi.org/10.1145/3385412.3386032.

This project is an attempt at creating an efficient implementation of the parser and compare it
against other generalized parsers.

Note: this project is **independent of the implementation presented in the above publication**.

# Getting the code

```sh
https://gitlab.com/byebye/relational-parser.git
git submodule update --init --recursive
```

# Compiling

This project requires compiler supporting `C++20` features. It compiles successfully using `gcc` at
version `11.1`, there are no guarantees for older versions and different compilers.

There is a convenience script to compile each CMake target which can be used as follows:

```sh
./compile.sh <target> <mode> <stats level> <cmake flags>
```

Examples:

```sh
# relparser target in debug mode with statistics set to DEBUG level.
./compile.sh relparser debug DEBUG
# benchmark target in release mode with statistics disabled.
./compile.sh benchmark release OFF
# grammar target in release mode with default statistics level for the mode (INFO).
./compile.sh grammar release
```

This will produce a soft link to the compiled binary `<target>-<mode>`, e.g.
`relparser-debug`, `benchmark-release`, `grammar-release` for the above commands.

What | Possible values
--- | ---
Target | `relparser`, `benchmark`, `grammar`
Mode | `debug`, `relwithdebinfo`, `release`
Statistics levels | `OFF`, `INFO`, `DEBUG`, `TRACE`

The statistics level is passed to CMake via the flag `-DSTATS_LEVEL=RELPARSER_STATS_LEVEL_<level>`.
The `RELPARSER_STATS_LEVEL_<level>` flag is a `#define` clause passed to the binary, see the source
code for usages.

## ANTLR grammars

The `compile.sh` script will compile the default set of ANTLR grammars into the
`grammar` and `benchmark` targets which will be automatically registered and known to the programs.
Additional grammars can be easily added to the script by appending it to
the `-DANTLR_PARSER_HEADER_LIBS` CMake flag using the specification:

```
<grammar name>,<registration name>,<start nonterminal>,<explicit EOF>,<separate parser/lexer>
```

The grammar name must correspond to `.g4` files put in the `resources/` directory. Examples:

Specification | Explanation
--- | ---
`JSON,json,json,NO_EOF,` | `JSON.g4` grammar to be registered with name `json` and start nonterminal `json`, without explicit `EOF` token, in a single file.
`XML,xml,document,NO_EOF,SEPARATE` | Grammar in separate files `XMLLexer.g4` and `XMLParser.g4` to be registered with name `xml` and start nonterminal `document`, without explicit `EOF` token
`Java8,java8,start,,` | `Java8.g4` grammar to be registered with name `java8` and start nonterminal `start`, with explicit `EOF` token, in a single file.

## Elkhound grammars

In order to execute Elkhound parser (available as part of `benchmark`), its grammars code needs to
be generated first. There is a convenience script `src/benchmark/elkhound/gen_elkhound.sh` which
will compile `elkhound` binary (in git submodule `lib/elkhound`) and use it to produce C++ files
automatically included by CMake:

```
cd src/benchmark/elkhound
# Files will be placed in src/benchmark/elkhound/gen/ directory
./gen_elkhound.sh
# Now make the Elkhound parser aware of the grammars.
./compile.sh benchmark release
```

# Executing

Target | Description
--- | ---
`grammar` | Converts ANTLR grammar to the custom `.rp` (_Relational Parser_) format and runs ANTLR lexer on selected files to generate input tokens understood by
the `.rp` grammar. The input tokens, if requested with `--savetokens`, will be saved to files
alongside the original files with additional `.<grammar>.tok` extension.
`relparser` | Relational Parser target allowing to execute parsing with different memoization
options.
`benchmark` | Binary for benchmarking Relational Parser against different parsers.

All targets can be executed with `-h` flag which will print all available options:

```sh
./relparser-release -h  
```

Both `relparser` and `benchmark` require - for original ANTLR grammars - `.rp` grammar and input
tokens to be generated with `grammar`. Example invocations:

```sh
./grammar-release -g json -f rp -o resources/JSON.rp -i resources/JSON.dot --savetokens -d resources/json -x '.*.json' -t all
./relparser-release -v1 -l relparser.log -m count -e substopt -g resources/JSON.rp -d resources/json -x '.*.json.tok' -u 100 -o json.result
./benchmark-release -v1 -l benchmark-relparser.log -m parse -p relparser -g resources/JSON.rp -d resources/json -x '.*.json.tok' -u 100 -o json-relparser.result
./benchmark-release -v1 -l benchmark-antlr.log -m parse -p antlr -g resources/JSON.g4-d resources/json -x '.*.json.tok' -u 100 -o json-antlr.result
```
