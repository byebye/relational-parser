#!/bin/bash

target="$1"
build_type=$(echo "$2" | awk '{print tolower($0)}')
stats_level=${3-}
flags=${4-}
build_dir=cmake-build-${build_type}

echo "Compiling ${build_type}, stats level: ${stats_level}"

mkdir -p "${build_dir}"
cd "${build_dir}"/ || exit 1

antlr_flags=""
if [ "${target}" != relparser ]; then
  antlr_flags="-DANTLR_PARSER_HEADER_LIBS="
  antlr_flags+="Java8,java8,start,,"
  antlr_flags+=";Java,java,compilationUnit,,"
  antlr_flags+=";Antlr4Simple,antlr,grammarSpec,,"
  antlr_flags+=";Ambiguous,ambig,a,NO_EOF,"
  antlr_flags+=";Parens,parens,parens,NO_EOF,"
  antlr_flags+=";JSON,json,json,NO_EOF,"
  antlr_flags+=";XML,xml,document,NO_EOF,SEPARATE"
  antlr_flags+=";arithmetic,arithmetic,file_,,"
fi

stats_flags=""
if [ -n "${stats_level}" ]; then
  stats_flags="-DSTATS_LEVEL=RELPARSER_STATS_LEVEL_${stats_level^^}"
fi

cmake -Wno-dev -DCMAKE_BUILD_TYPE="${build_type}" -DBUILD_LIBS_ONLY=off "${stats_flags}" "${flags}" "${antlr_flags}" \
  .. && make -j $(nproc) "${target}" || exit 1

cd ..

ln -sf "${build_dir}/src/${target}" "${target}-${build_type}"
echo "Produced link to the binary: ${target}-${build_type}"
