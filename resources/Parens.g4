grammar Parens;

parens :
       | LPAREN parens RPAREN parens;

LPAREN : '(' ;
RPAREN : ')' ;
