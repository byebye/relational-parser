#!/bin/bash

target="${1}"
command=${2-run}
build_type="${3-debug}"
log_level="${4-1}"
mode="${5-parsetree}"
engine="${6-dag}"
grammar="${7-antlr}"
input_opts="${8--f resources/ParensSimple.g4}"
test_id="${9-unknown}"

shift 9

compile=false
resetparser=false
outputresult=true
updateevery=10000
reptimes=5
statslevel=
dotfile=false

while (("$#")); do
  case "${1}" in
    compile) compile=true ;;
    noresult) outputresult=false ;;
    resetparser) resetparser=true ;;
    reptimes)
      shift
      reptimes="${1}"
      ;;
    updateevery)
      shift
      updateevery="${1}"
      ;;
    statslevel)
      shift
      statslevel="${1}"
      ;;
    dotfile)
      shift
      dotfile=true
      ;;
    *)
      echo "Unsupported option: ${1}"
      exit 1
      ;;
  esac
  shift
done

function setup() {
  if [[ "${compile}" == true ]]; then
    ./compile.sh "${target}" "${build_type}" "${statslevel}" || exit 1
  fi

  output_prefix="${1}"

  log_file="${output_prefix}.log"
  stdout_file="${output_prefix}.stdout"

  echo "Log file: ${log_file}"
  echo "stdout file: ${stdout_file}"

  command_args=(
    "./${target}-${build_type}"
    -v"${log_level}" -l "${log_file}"
  )


  if [[ "${target}" == "benchmark" ]]; then
    command_args+=(
      -m "${mode}" -p "${engine}"
      -g "${grammar}" ${input_opts}
      -u ${updateevery}
    )
    if [[ "${outputresult}" == true ]]; then
      echo "results file: ${output_prefix}-result.txt"
      command_args+=(
        -o "${output_prefix}-result.txt"
      )
    fi
  else
    command_args+=(
      -m "${mode}" -e "${engine}"
      -g "${grammar}" ${input_opts}
      -u ${updateevery}
    )

   if [[ "${outputresult}" == true ]]; then
      echo "results file: ${output_prefix}-result.txt"
      command_args+=(
        -o "${output_prefix}-result.txt"
      )
      if [[ "${dotfile}" == true ]]; then
        echo "dot file: ${output_prefix}-result.dot"
        command_args+=(
          --dot "${output_prefix}-result.dot"
          --parsetreelimit 16
        )
      fi
    fi

    if [[ "${resetparser}" == true ]]; then
      command_args+=(-p)
    fi

  fi

  echo "Running command: ${command_args[*]}"
}

timestamp=$(date '+%Y%m%d_%H%M%S_%8N')
machine_name=$(uname -n)
commit_timestamp=$(git log -1 --pretty=format:'%cd' --date=format:'%Y%m%d_%H%M%S')
commit_id=$(git describe --tags --abbrev=8)
code_version="${commit_timestamp}-${commit_id}"
parser_dir="${test_id}-${mode}-${engine}"
execution_dir="${machine_name}-${code_version}"

case ${command} in
  run)
    results_dir="log-${target}/${parser_dir}/${execution_dir}"
    mkdir -p ${results_dir}

    setup "${results_dir}/${timestamp}"
    "${command_args[@]}" | stdbuf -o 0 tee "${stdout_file}"
    ;;
  run)
    results_dir="log/${parser_dir}/${execution_dir}"
    mkdir -p ${results_dir}

    setup "${results_dir}/${timestamp}"
    "${command_args[@]}" | stdbuf -o 0 tee "${stdout_file}"
    ;;
  benchmark)
    results_dir="benchmark/${parser_dir}/${execution_dir}/${timestamp}"
    mkdir -p "${results_dir}"

    setup "${results_dir}/log"

    bench_output="${results_dir}/benchmark-result.md"
    echo "Benchmarking output: ${bench_output}"

    hyperfine -r ${reptimes} --export-markdown "${bench_output}" \
      --show-output "${command_args[*]}" | tee "${stdout_file}"

    run_desc=$(git log -1 --pretty=format:'[%h] %s%d')
    echo "${run_desc}" >> "${bench_output}"
    ;;
  perf)
    results_dir="perf/${parser_dir}/${execution_dir}/${timestamp}"
    mkdir -p "${results_dir}"

    setup "${results_dir}/log"

    perf_output="${results_dir}/result.dat"
    echo "Perf output: ${perf_output}"

    perf record -o "${perf_output}" --call-graph dwarf "${command_args[@]}" | tee "${stdout_file}"
    ;;
  *)
    echo "Unknown command: ${command}. Must be one of: run, perf, benchmark."
    exit 1
    ;;
esac
