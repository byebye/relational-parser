import sys
import csv
from collections import defaultdict

def nested_dict(n):
    if n == 1:
        return defaultdict()
    else:
        return defaultdict(lambda: nested_dict(n - 1))


counters_map = nested_dict(4)


def add_count(key, count):
    k1, stat = key.split('_::')
    grammar, mode, opt = k1.split('_')
    counters_map[grammar][mode][opt][stat] = count


name_mapping = {'arithmetic-replica': 'arith.-replica'}
modes = ['recognize', 'count', 'forest']
opts = ['trivialopt', 'contextfreeopt', 'substopt']
suffs = ['computePhase', 'computePhase-cached']
fieldnames = ['Grammar'] + ['{}-{}-{}'.format(m, o, s) for m in modes for o in opts for s in suffs]
# print(fieldnames)

stats_file = sys.argv[1]
file_prefix = '' if len(sys.argv) < 3 else sys.argv[2]
with open(stats_file, newline='') as input_csv:
    reader = csv.reader(input_csv, delimiter=';')
    for row in reader:
        add_count(*row)

    with open('{}relparser_counters.csv'.format(file_prefix), 'w', newline='') as output_file:
        for grammar, m1 in counters_map.items():
            row = {'Grammar': name_mapping.get(grammar, grammar)}
            line = ''
            for mode in ['recognize', 'count', 'forest', 'forest']:
                vals = []
                for opt in ['trivialopt', 'contextfreeopt', 'substopt']:
                    # print(grammar, mode, opt, m1[mode][opt])
                    cached = int(m1[mode][opt].get('computePhase-cached', '0'))
                    total = int(m1[mode][opt].get('computePhase', '1'))
                    percent = 100 * cached / total
                    if percent == 0:
                        vals.append('${:.0f}$'.format(100 * cached / total))
                    elif percent < 1 or percent > 99.99:
                        vals.append('${:.4f}$'.format(100 * cached / total))
                    else:
                        vals.append('${:.2f}$'.format(100 * cached / total))
                line += ' & & ' + ' & '.join(vals)
            output_file.write('{}: {}\n'.format(grammar, line))
