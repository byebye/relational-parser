#! /usr/bin/env Rscript
d<-scan("stdin", quiet=TRUE)
cat(min(d)/1000, max(d)/1000, mean(d)/1000, sep=";")
cat("\n")
