#!/bin/bash

parser="${1}"
mode="${2}"
grammar="${3}"

timestamp=$(date '+%Y%m%d_%H%M%S_%8N')

dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
hyperdir=${dir}/../log-benchmark/hyperfine
mkdir -p ${hyperdir}
hyperfile="${hyperdir}/${grammar}-${mode}-${parser}_${timestamp}.json"
timeout 12m hyperfine -w 1 -r 5 --export-json "${hyperfile}" "./benchmark.sh ${parser} ${mode} ${grammar}"
