#!/bin/bash

dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
cd ${dir}/..

for grammar in java8 java json xml arithmetic arithmetic-replica rec-right rec-left ; do
  for parser in relparser yaep antlr elkhound ; do
    for mode in recognize count parse forest forestcount ; do
      # ParseRunner::readTokens
      # ::parse ::countParseTrees
      # RelationalParser::epsilon
      # YaepParser::parseGrammar YaepParser::freeMemory
      # AntlrParser::prepareTokens
      # ParseRunner::preprocessing ParseRunner::parsing
      # for stat in ::countParseTrees ; do
      for stat in ParseRunner::preprocessing ParseRunner::parsing ; do
        stats=$(fd -Ip -e stdout "log-benchmark/${grammar}-${mode}-${parser}" \
                -x rg -I "${stat}" --with-filename \
                | sort | tail -n +3 \
                | rg -o '(\d+)ms' -r '$1' \
                | ${dir}/stats.r 2> /dev/null \
                | rg -v 'NaN')
        if [[ -n "$stats" ]]; then
          echo "${grammar}_${mode}_${parser}_${stat};${stats}"
        fi
      done
    done
  done
done
