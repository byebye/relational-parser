#!/bin/bash

#./compile.sh benchmark release OFF || exit 1
#./compile.sh relparser release INFO || exit 1

mkdir -p log-memory

#for grammar in java8 java json xml arithmetic arithmetic-replica rec-right rec-left ; do
#  for mode in recognize count parse forest forestcount ; do
#    /usr/bin/time -f "%E %M" -ao log-memory/relparser-${grammar}-${mode} sh -c "./benchmark.sh relparser ${mode} ${grammar}"
#  done
#done
#
## no xml
#for grammar in java8 java json arithmetic arithmetic-replica rec-right rec-left ; do
#  for mode in recognize ; do
#    /usr/bin/time -f "%E %M" -o log-memory/yaep-${grammar}-${mode} sh -c "./benchmark.sh yaep ${mode} ${grammar}"
#  done
#done
#
## no xml json
#for grammar in java8 java arithmetic arithmetic-replica rec-right rec-left ; do
#  for mode in parse ; do
#    /usr/bin/time -f "%E %M" -o log-memory/yaep-${grammar}-${mode} sh -c "./benchmark.sh yaep ${mode} ${grammar}"
#  done
#done
#
## no xml json arithmetic-replica
#for grammar in java8 java arithmetic rec-right rec-left ; do
#  for mode in forest forestcount ; do
#    /usr/bin/time -f "%E %M" -o log-memory/yaep-${grammar}-${mode} sh -c "./benchmark.sh yaep ${mode} ${grammar}"
#  done
#done
#
#for grammar in java json xml ; do
#  for mode in recognize parse ; do
#    /usr/bin/time -f "%E %M" -o log-memory/antlr-${grammar}-${mode} sh -c "./benchmark.sh antlr ${mode} ${grammar}"
#  done
#done

for grammar in java8 json rec-right rec-left ; do
  for mode in recognize parse forest forestcount ; do
    /usr/bin/time -f "%E %M" -o log-memory/elkhound-${grammar}-${mode} sh -c "./benchmark.sh elkhound ${mode} ${grammar}"
  done
done
