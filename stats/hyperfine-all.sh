#!/bin/bash

for grammar in java8 java json xml arithmetic arithmetic-replica rec-right rec-left ; do
  for parser in relparser yaep ; do
    for mode in recognize count parse forest ; do
      ./hyperfine.sh ${parser} ${mode} ${grammar}
    done
  done
  if [[ "$grammar" != rec* && "$grammar" != arithmetic-replica ]]; then
    for mode in recognize parse ; do
      ./hyperfine.sh antlr ${mode} ${grammar}
    done
  fi
done
