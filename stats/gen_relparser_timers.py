import sys
import csv
from collections import defaultdict

def nested_dict(n):
    if n == 1:
        return defaultdict()
    else:
        return defaultdict(lambda: nested_dict(n - 1))


counters_map = nested_dict(4)


def add_time(key, min_time, max_time, avg_time):
    k1, stat = key.split('_::')
    grammar, mode, opt = k1.split('_')
    counters_map[grammar][mode][opt][stat] = float(avg_time)


name_mapping = {'arithmetic-replica': 'arith.-replica'}
modes = ['recognize', 'count', 'parse', 'forest']
opts = ['trivialopt', 'contextfreeopt', 'substopt']
suffs = ['computePhase', 'computePhase-cached']
fieldnames = ['Grammar'] + ['{}-{}-{}'.format(m, o, s) for m in modes for o in opts for s in suffs]
defs = {'preprocessing': 'P [s]', 'parsing': 'E [s]', 'epsilon': '$\\epsilon$ [s]'}

stats_file = sys.argv[1]
file_prefix = '' if len(sys.argv) < 3 else sys.argv[2]
with open(stats_file, newline='') as input_csv:
    reader = csv.reader(input_csv, delimiter=';')
    for row in reader:
        add_time(*row)

    with open('{}relparser_timers.csv'.format(file_prefix), 'w', newline='') as output_file:
        for grammar, m1 in counters_map.items():
            output_file.write('{}\n'.format(grammar))
            for stat in ['preprocessing', 'parsing', 'epsilon']:
                line = '& {} & '.format(defs[stat])
                first = True
                for mode in ['recognize', 'count', 'parse', 'forest']:
                    vals = []
                    for opt in ['trivialopt', 'contextfreeopt', 'substopt']:
                        # print(grammar, mode, opt, m1[mode][opt])
                        etime = float(m1[mode][opt].get(stat, 'NaN'))
                        if etime < 0.01:
                            vals.append('${:.0f}$'.format(etime))
                        # elif etime < 0.01:
                        #     vals.append('${:.2f}$'.format(etime))
                        else:
                            vals.append('${:.2f}$'.format(etime))
                    if first:
                        first = False
                        line += ' & '.join(vals)
                    else:
                        line += ' & & ' + ' & '.join(vals)
                output_file.write('{} \\\\\n'.format(line))
