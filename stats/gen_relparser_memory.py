import sys
import csv
from collections import defaultdict


def nested_dict(n):
    if n == 1:
        return defaultdict()
    else:
        return defaultdict(lambda: nested_dict(n - 1))


mem_map = nested_dict(3)


def add_memory(key, mem):
    parser, grammar, mode = key.split('_')
    mem_map[grammar][mode][parser] = float(mem) / 1024


grams = ['rec-left', 'rec-right', 'xml', 'json', 'arithmetic', 'arithmetic-replica', 'java',
         'java8']
grammars = {'java': 'Java', 'java8': 'Java8', 'xml': 'XML', 'json': 'JSON', 'rec-left': 'Rec-left',
            'rec-right': 'Rec-right', 'arithmetic': 'Arithmetic',
            'arithmetic-replica': 'Arith. replica'}
modes = ['recognize', 'count', 'parse', 'forest']
defs = {'recognize': 'Recognition', 'count': 'Counting', 'parse': 'Tree', 'forest': 'Forest',
        'forestcount': 'Forest (counting)'}
parsers = ['relparser', 'yaep', 'elkhound', 'antlr']

stats_file = sys.argv[1]
file_prefix = '' if len(sys.argv) < 3 else sys.argv[2]
with open(stats_file, newline='') as input_csv:
    reader = csv.reader(input_csv, delimiter=';')
    for row in reader:
        add_memory(*row)
    # print(mem_map)
    with open('{}relparser_memory.txt'.format(file_prefix), 'w', newline='') as output_file:
        for grammar in grams:
            output_file.write('\\multirow{{4}}{{*}}{{{}}} & '.format(grammars[grammar]))
            gfirst = True
            for mode in modes:
                line = '{} & '.format(defs[mode])
                if not gfirst:
                    line = '& ' + line
                gfirst = False

                vals = []
                first = True
                for parser in parsers:
                    # print(grammar, mode, opt, m1[mode][opt])
                    mem = mem_map[grammar][mode].get(parser, 0)
                    if mem < 0.001:
                        vals.append(' ')
                    else:
                        vals.append('${:.2f}$'.format(mem))
                    if first:
                        first = False
                line += ' & '.join(vals)
                output_file.write('{} & \\\\\n'.format(line))
            output_file.write('\\midrule\n')
