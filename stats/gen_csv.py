import sys
import csv
from collections import defaultdict
import operator


def accumulate(L, i, op):
    def iter(result, rest, out):
        if rest == []:
            return out
        else:
            r = op(result, rest[0][i - 1])
            return iter(r, rest[1:], out + [r])

    return iter(L[0][i - 1], L[1:], [])


def nested_dict(n):
    if n == 1:
        return defaultdict()
    else:
        return defaultdict(lambda: nested_dict(n - 1))


times_map = nested_dict(4)


def add_time(key, min_time, max_time, avg_time):
    k1, stat = key.split('_::')
    grammar, mode, parser = k1.split('_')
    times_map[mode][grammar][parser][stat] = (float(min_time), float(max_time), float(avg_time))


name_mapping = {'arithmetic': 'arith.', 'arithmetic-replica': 'arith.-replica'}
missing_grammars = {'antlr': ['rec-left', 'rec-right', 'arithmetic-replica'],
                    'relparser': [],
                    'yaep': [],
                    'elkhound': [], }
parsers = missing_grammars.keys()
suffs = ['min', 'max', 'avg']
fieldnames = ['Grammar'] + ['{}-{}'.format(p, s) for p in parsers for s in suffs]
# print(fieldnames)

stats_file = sys.argv[1]
file_prefix = '' if len(sys.argv) < 3 else sys.argv[2]
with open(stats_file, newline='') as input_csv:
    reader = csv.reader(input_csv, delimiter=';')
    for row in reader:
        add_time(*row)

    for mode, m1 in times_map.items():
        print('{}:'.format(mode))
        for grammar, m2 in m1.items():
            print('  {}:'.format(grammar))
            for parser, m3 in m2.items():
                for stat, times in m3.items():
                    print('    {} {}: {}'.format(parser, stat, times))
                total = tuple(map(sum, zip(*m3.values())))
                print('    {} total: {}'.format(parser, total))

    for mode, m1 in times_map.items():
        with open('{}{}.csv'.format(file_prefix, mode), 'w', newline='') as output_csv:
            writer = csv.DictWriter(output_csv, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            for grammar, m2 in m1.items():
                row = {'Grammar': name_mapping.get(grammar, grammar)}
                for parser, m3 in m2.items():
                    total = tuple(map(sum, zip(*m3.values())))
                    def fnum(num):
                        return '{:.4f}'.format(num) if num < 1 else '{:.2f}'.format(num)
                    row = row | {'{}-{}'.format(parser, s): fnum(t) for t, s in zip(total, suffs)}
                for p in parsers:
                    for s in suffs:
                        key = '{}-{}'.format(p, s)
                        if key.format(p) not in row:
                            row[key] = 'nan' if (grammar in missing_grammars[p]) else 0
                writer.writerow(row)
